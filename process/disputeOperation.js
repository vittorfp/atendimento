
function capitalize(name) {
    return name.toLowerCase().replace(/(?:^|\s)\S/g, function (a) { return a.toUpperCase(); });
}

function maskedCep(cep) {
    return cep.slice(0, 2) + "." + cep.slice(2, 5) + "-" + cep.slice(5, 8);
}

var body = null;

if (_object.cpf) {
    body = { "cpf": _object.cpf };
} else if (_object.cnpj) {
    body = { "cnpj": _object.cnpj };
}

if (body && _object.attendanceData.signature) {

    var disputeResponses = [];
    _object.confirmContestedOperations.map((operation) => {

        
        var cpResponse = JSON.parse(_object.cadastroPositivoResponse);
        
        var protocolReport = cpResponse.protocolo;
        cpResponse.operacoes.map(operationReport => {
            var payload = {
                "empresa": {
                    "nomeEmpresa": "TESTE DO POSITIVO",
                    "nomeFantasia": "TESTE DO TESTE POSITIVO",
                    "numeroCnpj": "00.000.010/0010-18",
                    "numeroCnpjConglomerado": operationReport.raizCnpjConglomerado, //"10"
                },
                "justificativa": operationReport,
                "modalidade": operationReport.modalidade,
                "operacao": {
                    "cnpjConglomerado": operationReport.cnpjConglomerado, //"00000010000127",
                    "cnpjContratante": "00.000.010/0010-18",
                    "numeroContrato": operationReport.numeroDoContrato, //"47559",
                    "numeroUnico": operationReport.numeroDaOperacao, //"7072204247956398661249414",
                    "prefixoAgencia": operationReport.numeroDaAgencia //"4886"
                },
                "motivoRevisao": {
                    "id": "5d7bacffd2d3e9000156d3ba",
                    "descricaoMotivoRevisao": "Valor do débito",
                    "status": true,
                    "tipoModalidade": "P"
                },
                "protocoloRelatorioSydle": protocolReport.numeroProtocolo, //1568636742959,
                "documentoCliente": _object.cpf //"39793938870"
            };
            var response = serasa.cadastroPositivoIntegration.disputeOperation(payload);
            disputeResponses.push(response);
        });
    });

    // var response = {success: true};
    if (response && response.success) {
        var rg = "";
        _object.attendanceData.terms = [];
        var month_mapper = {
            1: 'Janeiro',
            2: 'Fevereiro',
            3: 'Março',
            4: 'Abril',
            5: 'Maio',
            6: 'Junho',
            7: 'Julho',
            8: 'Agosto',
            9: 'Setembro',
            10: 'Outubro',
            11: 'Novembro',
            12: 'Dezembro'
        };

        if (body.cpf) {
            var template = serasa.cadastroPositivoTemplates._get({ _id: "5d72a9c0dd498345ac09d4ee" }).template;

            template = template.replace(/#NOME#/g, capitalize(_object.customer.name));
            template = template.replace(/#CPF#/g, _object.customer.maskedCpfCnpj);
            template = template.replace('#TIPO_DOCUMENTO#', 'RG');
            template = template.replace('#DOCUMENTO#', rg);

            template = template.replace('#EMAIL#', _object.customer.emails[0].address);
            template = template.replace('#TEL#', _object.customer.phones[0].number);

        } else if (body.cnpj) {
            var template = serasa.cadastroPositivoTemplates._get({ _id: "5d72aa3bdd498345ac09d800" }).template;

            template = template.replace(/#NOME#/g, capitalize(_object.responsible.name));
            template = template.replace(/#CPF#/g, _object.responsible.maskedCpfCnpj);
            template = template.replace('#NOME_EMPRESA#', _object.customer.companyName);
            template = template.replace('#CNPJ#', _object.customer.maskedCpfCnpj);
            template = template.replace('#TIPO_DOCUMENTO#', 'RG');
            template = template.replace('#DOCUMENTO#', rg);

            template = template.replace('#EMAIL#', _object.responsible.emails[0].address);
            template = template.replace('#TEL#', _object.responsible.phones[0].number);

        }

        var table = "";
        _object.confirmContestedOperations.forEach(operation => {
            var line = `
            <tr>
                <th><font face="Calibri">#DATA#</font></th>
                <th><font face="Calibri">#NUM_REL#</font></th>
                <th><font face="Calibri">#CNPJ#</font></th>
                <th><font face="Calibri">#NUM_OP#</font></th>
                <th><font face="Calibri">#MOTIVO#</font></th>
            </tr>`;

            line.replace('#DATA#', operation.cnpj);
            line.replace('#NUM_REL#', operation.operationNumber);
            line.replace('#CNPJ#', operation.cnpj);
            line.replace('#NUM_OP#', operation.operationNumber);
            line.replace('#MOTIVO#', operation.maskedCpfCnpj);
            table += line;
        });
        template = template.replace('#OPERACOES#', table);

        template = template.replace('#LOCAL#', "São Paulo");
        template = template.replace('#DIA#', _object._creationDate.getDate());
        template = template.replace('#MES#', month_mapper[_object._creationDate.getMonth() + 1]);
        template = template.replace('#ANO#', _object._creationDate.getFullYear());
        template = template.replace('#ASSINATURA#', _object.attendanceData.signature);

        let arqHTML = _util._fileHelper.create({
            content: template,
            orientation: "landscape",
            targetFileName: "Termo de contestação de operações.html",
            targetContentType: "text/html"
        });

        // conversão do html para o pdf
        _object.attendanceData.terms.push(_util._fileHelper.convert({
            file: arqHTML,
            targetFileName: "Termo de contestação de operações.pdf"
        }));

        _output.attendanceData = _object.attendanceData;
        _utils.addInfoMessage("O pedido de contestação de operação foi inserido na fila e será executado dentro de 48h.");
    } else {
        throw ('Erro Dispute.');
    }

} else if (!_object.attendanceData.signature) {
    throw ('Consumidor não assinou o documento');
}