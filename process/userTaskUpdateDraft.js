/***
 *
 * Os updateDrafts foram separados por etapa do atendimento.
 *
 * Cada estado deve ter um script principal, que sera executado toda
 * vez que o _updateDraft for chamado naquele estado. A função getState
 * retorna qual o estado atual, e deve implementar a lógica dos fluxos.
 *
 * A função de endCheck retorna um booleano informado se o fluxo já
 * passou por aquele estado.
 *
 * A função de sequence informa qual será o próximo estado do fluxo.
 *
 * @input {
 *      object: _object da atividade humana,
 *      oldObject: _oldObject do _updateDraft da atividade humana
 *      _id: ID da instância do processo
 * }
 *
 * @output: _object de entrada atualizado
 *
 * @see _updateDraft e _start da atividade humana.
 */

var _activity = _input.object;
var _oldObject = _input.oldObject;
var type = _input.type; // FINALIZE, UPDATE_DRAFT

if (!_activity) {
	return;
}

//////////////////////////////////////////////////

var stateFlowMap = {
	PROCESS_START: {
		start: processStartStart,
		onState: processStartOnState,
		endCheck: processStartEndCheck,
		sequence: processStartSequence,
		end: processStartEnd
	},
	PARSE_CLIENT: {
		start: parseClientStart,
		onState: parseClientOnState,
		endCheck: parseClientEndCheck,
		sequence: parseClientSequence,
		end: parseClientEnd
	},
	SEEKING_CLIENT: {
		start: seekingClientStart,
		onState: seekingClientOnState,
		endCheck: seekingClientEndCheck,
		sequence: seekingClientSequence,
		end: seekingClientEnd
	},
	REGISTER_CUSTOMER: {
		start: registerCustomerStart,
		onState: registerCustomerOnState,
		endCheck: registerCustomerEndCheck,
		sequence: registerCustomerSequence,
		end: registerCustomerEnd
	},
	REGISTER_ENTERPRISE: {
		start: registerEnterpriseStart,
		onState: registerEnterpriseOnState,
		endCheck: registerEnterpriseEndCheck,
		sequence: registerEnterpriseSequence,
		end: registerEnterpriseEnd
	},
	CREATE_CUSTOMER: {
		start: createCustomerStart,
		onState: createCustomerOnState,
		endCheck: createCustomerEndCheck,
		sequence: createCustomerSequence,
		end: createCustomerEnd
	},
	CREATE_ENTERPRISE: {
		start: createEnterpriseStart,
		onState: createEnterpriseOnState,
		endCheck: createEnterpriseEndCheck,
		sequence: createEnterpriseSequence,
		end: createEnterpriseEnd
	},
	UPDATE_CUSTOMER: {
		start: updateCustomerStart,
		onState: updateCustomerOnState,
		endCheck: updateCustomerEndCheck,
		sequence: updateCustomerSequence,
		end: updateCustomerEnd
	},
	UPDATE_ENTERPRISE: {
		start: updateEnterpriseStart,
		onState: updateEnterpriseOnState,
		endCheck: updateEnterpriseEndCheck,
		sequence: updateEnterpriseSequence,
		end: updateEnterpriseEnd
	},
	ATENDIMENTO_ESCRITORIOS: {
		start: atendimentoEscritoriosStart,
		onState: atendimentoEscritoriosOnState,
		endCheck: atendimentoEscritoriosEndCheck,
		sequence: atendimentoEscritoriosSequence,
		end: atendimentoEscritoriosEnd
	},
// 	CUSTOMER_PROCURATION: {
// 	    start: customerProcurationStart,
// 		onState: customerProcurationOnState,
// 		endCheck: customerProcurationEndCheck,
// 		sequence: customerProcurationSequence,
// 		end: customerProcurationEnd
// 	},
	CREDIT_RATING: {
	    start: creditRatingStart,
		onState: creditRatingOnState,
		endCheck: creditRatingEndCheck,
		sequence: creditRatingSequence,
		end: creditRatingEnd
	},
	POSITIVE_IDENTIFICATION: {
	    start: positiveIdentificationStart,
		onState: positiveIdentificationOnState,
		endCheck: positiveIdentificationEndCheck,
		sequence: positiveIdentificationSequence,
		end: positiveIdentificationEnd
	},
	REGISTER_PHONE_CONTACT: {
		start: registerPhoneContactStart,
		onState: registerPhoneContactOnState,
		endCheck: registerPhoneContactEndCheck,
		sequence: registerPhoneContactSequence,
		end: registerPhoneContactEnd
	},
	PARSE_RESPONSIBLE:{
	    start: parseResponsibleStart,
		onState: parseResponsibleOnState,
		endCheck: parseResponsibleEndCheck,
		sequence: parseResponsibleSequence,
		end: parseResponsibleEnd
	},
	CHOOSE_RESPONSIBLE:{
	    start: chooseResponsibleStart,
		onState: chooseResponsibleOnState,
		endCheck: chooseResponsibleEndCheck,
		sequence: chooseResponsibleSequence,
		end: chooseResponsibleEnd
	},
	PERFORM_ATTENDANCE: {
		start: performAttendanceStart,
		onState: performAttendanceOnState,
		endCheck: performAttendanceEndCheck,
		sequence: performAttendanceSequence,
		end: performAttendanceEnd
	},
	DOCUMENT_AUTENTICATION: {
		start: documentAutenticationStart,
		onState: documentAutenticationOnState,
		endCheck: documentAutenticationEndCheck,
		sequence: documentAutenticationSequence,
		end: documentAutenticationEnd
	},
	PERFORM_RETENTION: {
		start: performRetentionStart,
		onState: performRetentionOnState,
		endCheck: performRetentionEndCheck,
		sequence: performRetentionSequence,
		end: performRetentionEnd
	},
	PERFORM_SALE: {
		start: performSaleStart,
		onState: performSaleOnState,
		endCheck: performSaleEndCheck,
		sequence: performSaleSequence,
		end: performSaleEnd
	},
	TERMINAL_STATE: { //DEPRECATED
		start: terminalStateStart,
		onState: terminalStateOnState,
		endCheck: terminalStateEndCheck,
		sequence: terminalStateSequence,
		end: terminalStateEnd
	}
};

//////////////////////////////////////////////

/**
 * Lógica distinta de Gateways.
 */
var Gateways = {
	joinCustomerUpdate: function () {
	    var contactIncomplete;
	    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
	        contactIncomplete = _activity.customer && (!_activity.customer.name || !_activity.customer.dateOfBirth) ? true : false;
	    } else {
	        contactIncomplete = _activity.responsible && (!_activity.responsible.name || !_activity.responsible.dateOfBirth) ? true : false;
	    }
		
		if (_activity.contactCreationError || contactIncomplete) {
			return "REGISTER_CUSTOMER";
		}
		
		//cria conexão com os parametros passados (uso exclusivo quando são pas)
        if (_activity.responsible && _activity.responsibleCPF) {
    
            var company = crm.enterprise.findByCNPJ({
                cnpj: _activity.cnpj
            });
    
            var connectionParams = {
                "type" : {_id: "5ca4eabcb31639198e1c69b0"}, //id - Tipo de conexão: Contato (atendimento)
                "enterprise": company,
                "contacts": _activity.responsible
            };
            crm.connection._create(connectionParams);
        }
        
        if (getInstanceControlProperty('ATTENDANCE_REALIZED_BY_ATENDIMENTO_ESCRITORIOS_GROUP')) {
            return "ATENDIMENTO_ESCRITORIOS";
        } else if (_activity.cpf && getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED') && !_activity.personFoundinRFWS && !_activity.personFoundinSerasaConsumidor ||
        _activity.responsibleCPF && getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED') && !_activity.responsibleFoundinRFWS && !_activity.responsibleFoundinSerasaConsumidor) {
            return "PERFORM_ATTENDANCE";
        } else {
            return "POSITIVE_IDENTIFICATION";
        }

	},
	joinCompanyUpdate: function () {
		var contactIncomplete = _activity.customer && (!_activity.customer.name || !_activity.customer.companyName) ? true : false;
		if (_activity.contactCreationError || contactIncomplete) {
			return "REGISTER_ENTERPRISE";
		}
        
        
        
        var company = crm.enterprise.findByCNPJ({
            cnpj: _activity.cnpj
        });
        
        if (company && company._id) {
            let connections = crm.connection._search({
                "query": {
                    "match": {
                        "enterprise._id": company._id
                    }
                }
            }).hits.hits;
            
            if (connections && connections.length) {
                return "CHOOSE_RESPONSIBLE";
            }
        }
        return "PARSE_RESPONSIBLE";

	}
};

//////////////////////////////////////////////

/**
 * Informa etapa atual do processo.
 */
function getState(currentState) {

	var currentStateFlow = stateFlowMap[currentState];

	if (currentStateFlow.endCheck()) {
		return currentStateFlow.sequence();
	}

	return currentState;
}

//////////////////////////////////////////////////

if (type === "FINALIZE") {
	try {
		var currentState = getInstanceControlProperty('state');
		stateFlowMap[currentState].end();
	} catch (e) {
		var error = e.message ? e.message : e;
		throw error;
		/// FOR DEBUG
		//throw 'Erro ao finalizar estado ' + currentState + ': ' + error;
	}
} else {
	/**
	 * Repete iterações enquanto o estado corrente não for repetido.
	 */
	do {
		//try {
			_activity.canFinish = false;

			var currentState = getInstanceControlProperty('state');

			//log('Estado atual: ' + currentState);
			_activity.currentState = currentState;

			// inicializa o estado se não foi inicializado.
			if (!stateInitialized(currentState)) {
				initializeState(currentState);
			}

			stateFlowMap[currentState].onState();
			setInstanceControlProperty('state', getState(currentState));

			// finaliza o estado antigo caso ele tenha sido alterado
			if (currentState !== getInstanceControlProperty('state')) {
				finalizeState(currentState);
			}

			//log('Estado novo: ' + getInstanceControlProperty('state'));

// 		} catch (e) {
// 			var error = e.message ? e.message : e;
// 			throw error;
// 			/// FOR DEBUG
// 			//throw 'Erro ao processar estado ' + currentState + ': ' + error + ': ' + e.lineNumber;
// 		}
	} while (currentState !== getInstanceControlProperty('state'));
}

//////////////////////////////////////////////

/** Funções auxiliares de controle de fluxo. */
function setInstanceControlProperty(key, value) {
	var instanceControl = JSON.parse(_activity.instanceControl);
	instanceControl[key] = value;

	_activity.instanceControl = JSON.stringify(instanceControl);
}

function getInstanceControlProperty(key) {
	var instanceControl = JSON.parse(_activity.instanceControl);
	return instanceControl[key];
}

function stateInitialized(state) {
	return getInstanceControlProperty('LAST_INITIALIZED_STATE') === state;
}

function initializeState(state) {
	stateFlowMap[state].start();
	setInstanceControlProperty('LAST_INITIALIZED_STATE', state);
	//log ("Inicializando estado " + state);
}

function finalizeState(state) {
	stateFlowMap[state].end();
}

/**
 * Permite a conclusão da atividade manual
 * @param state estado
 */
function finalizeActivity(state) {
	_activity.canFinish = true;
	return state;
}

function log(msg) {
	_utils.log(">>>> " + msg);
}

function json(object) {
	return _utils.stringifyAsJson(object);
}

//////////////////////////////////////////////

/** Funções usadas em mais de um estado. */
function pad(n) {
	return n < 10 ? '0' + n :  '' + n;
}

function hasChange(a, b) {
	if (a && a._id && b && b._id) {
		if (a._id !== b._id) {
			return true;
		}
	} else if ((a && a._id) || (b && b._id)) {
		return true;
	}
	return false;
}

function calculateSLAData() {

	if (hasChange(_activity.prodServ, _oldObject.prodServ) ||
		hasChange(_activity.reason, _oldObject.reason) ||
		hasChange(_activity.subReason, _oldObject.subReason) ||
		hasChange(_activity.subSubReason, _oldObject.subSubReason)) {
		var tabOn = _activity.subSubReason ? _activity.subSubReason :
			_activity.subReason ? _activity.subReason :
			_activity.reason ? _activity.reason : null;
		var tabCanFinish = false;
		if (tabOn && tabOn.customerServiceData && tabOn.customerServiceData.length) {
			tabCanFinish = true;
		}

		if (tabCanFinish) {

			function calculateSLA() {
				var tabOn = _activity.subSubReason ? _activity.subSubReason :
					_activity.subReason ? _activity.subReason :
					_activity.reason ? _activity.reason : null;

				if (tabOn.customerServiceData) {
					return tabOn.customerServiceData.reduce(function (prevValue, cSD) {
						if (cSD.SLA) {
							return prevValue + cSD.SLA;
						} else {
							return prevValue;
						}
					}, 0);
				}
			}

			function calculateFinalDate(SLA) {
				if (!SLA) {
					return null;
				}

				var finalDate = _activity.n1DateAttendance ? new Date(_activity.n1DateAttendance.toISOString()) : new Date();

				var holidays = billing.holiday._search({
						size: 100
					});
				if (holidays && holidays.hits && holidays.hits.total > 0) {
					holidays = holidays.hits.hits.map(function (hit) {
							return hit._source;
						});
				}

				for (var i = 0; i < Math.ceil(SLA / 24); ++i) {
					finalDate.setDate(finalDate.getDate() + 1);
					while (checkWeekend(finalDate) || checkHoliday(finalDate, holidays)) {
						finalDate.setDate(finalDate.getDate() + 1);
					}
				}

				return finalDate;
			}

			function checkWeekend(date) {
				return date.getDay() === 6 || date.getDay() === 0;
			}

			function checkHoliday(date, holidays) {
				if (!holidays) {
					return false;
				}

				return holidays.some(function (holiday) {
					if (holiday.fixedDate) {
						var holidayDate = holiday.date && holiday.date[0] ? holiday.date[0] : null;
						if (holidayDate) {
							if (date.getDate() === holidayDate.getDate() && date.getMonth() === holidayDate.getMonth()) {
								return true;
							}
						}
					} else {
						var holidayDates = holiday.date ? holiday.date : null;
						if (holidayDates) {
							var dateNumber = [date.getDate(), date.getMonth(), date.getFullYear()];
							return holidayDates.some(function (holidayDate) {
								var holidayNumber = [holidayDate.getDate(), holidayDate.getMonth(), holidayDate.getFullYear()];
								if (dateNumber[0] === holidayNumber[0] && dateNumber[1] === holidayNumber[1] && dateNumber[2] === holidayNumber[2]) {
									return true;
								}
							});
						}
					}
				});
			}

			var sla = calculateSLA();
			var finalDate = calculateFinalDate(sla);
			if (sla && finalDate) {
				sla += " horas";
				_activity.slaData = {
					sla: sla,
					finalDate: finalDate
				};
			}
		} else {
			_activity.slaData = null;
		}
	}
}

function phoneAndEmailAndAddressMainContactControl() {
    var dataToControl = ["phoneContact", "emailContact", "addressContact"];
    
    dataToControl.forEach(function(data) {
        
        if (_activity[data] && _activity[data].length) {
            if (_activity[data].length === 1) {
                _activity[data][0].mainContact = true;
            } else {
                var mainContactCount = _activity[data].reduce(
                    function (acc, curr) {
                        return (curr.mainContact === true ? acc + 1 : acc);
                    }, 0
                ); 
                if (mainContactCount > 1) {
                    _activity[data].forEach(function(phoneOrEmail, index){
                        if (phoneOrEmail.mainContact === _oldObject[data][index].mainContact) {
                            _activity[data][index].mainContact = false;
                        }
                    });
                } else if (mainContactCount === 0){
                    _activity[data].some(function(phoneOrEmail, index){
                        if (phoneOrEmail.mainContact !== _oldObject[data][index].mainContact) {
                            _activity[data][index].mainContact = true;
                            return true;
                        }
                    });
                }
            }
        }
        
    });
    
}


//////////////////////////////////////////////

function processStartStart() {
    
    function userIsAtendimentoEscritorios() {
        return _user.aclUserGroups.some( (group) => {
            return (group.identifier === "officeAttendance");
        });
    }
    
    if (userIsAtendimentoEscritorios()) {
        setInstanceControlProperty('ATTENDANCE_REALIZED_BY_ATENDIMENTO_ESCRITORIOS_GROUP', true);
    }
}

function processStartOnState() {}

function processStartEndCheck() {
	return true;
}

function processStartSequence() {
	if (_activity.URAOrigin && (_activity.cpf || _activity.cnpj)) {
		setInstanceControlProperty('FROM_URA_AND_VALID_CPF_OR_CNPJ', true);
		return "SEEKING_CLIENT";
	}
	return "PARSE_CLIENT";
}

function processStartEnd() {}

//////////////////////////////////////////////

function parseClientStart() {
    // caso passe mais de uma vez pelo parse client, reseta o serasaConsumidorEmail
    _activity.serasaConsumidorEmail = null;
}

function parseClientOnState() {
	function checkCpf() {
		if (_activity.anonymousClient) {
			_activity.cpf = null;
		} else {
			_activity.cpf = _activity.cpf.replace(/\D/g, '');
		}
	}
	
	function checkCnpj() {
		if (_activity.anonymousClient) {
			_activity.cnpj = null;
		} else {
			_activity.cnpj = _activity.cnpj.replace(/\D/g, '');
		}
	}

	if (_activity.cpf) {
		checkCpf();
	}
	
	if (_activity.cnpj) {
		checkCnpj();
	}
}

function parseClientEndCheck() {
	function isValidCPF(cpf) {
		var v = crm.document.validateCPF({
				"cpf": cpf
			});
		if (!v || !v.success) {
			throw 'O CPF informado ' + _activity.cpf + ' não é válido.';
		}

		return v && v.success;
	}
	
	function isValidCNPJ(cnpj) {
		var v = crm.document.validateCNPJ({
				"cnpj": cnpj
			});
		if (!v || !v.success) {
			throw 'O CNPJ informado ' + _activity.cnpj + ' não é válido.';
		}

		return v && v.success;
    }
    
    function isValidEmail(serasaConsumidorEmail) {
        var v = crm.email.validateEmailAddress({
            "address": serasaConsumidorEmail
        });
        if (!v || !v.success) {
			throw 'O Email informado ' + _activity.serasaConsumidorEmail + ' não é válido.';
        }
        
        return v && v.success;
    }


    if (_activity.cnpj){
        return (_activity.cnpj && _activity.cnpj.length >= 14 && isValidCNPJ(_activity.cnpj));
    } else if (_activity.serasaConsumidorEmail) {
        return (_activity.serasaConsumidorEmail && isValidEmail(_activity.serasaConsumidorEmail));
    } else {
        return (_activity.cpf && _activity.cpf.length >= 11 && isValidCPF(_activity.cpf)) || _activity.anonymousClient;
    }
	
}

function parseClientSequence() {
	if (_activity.anonymousClient) {
		return "PERFORM_ATTENDANCE";
	}

	return "SEEKING_CLIENT";
}

function parseClientEnd() {}

//////////////////////////////////////////////

function seekingClientStart() {}

function seekingClientOnState() {

	function seekInCrm() {
	    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            if (_activity.cnpj){
                _activity.customer = crm.enterprise.findByCNPJ({
                    cnpj: _activity.cnpj
                });

                _activity.companyFoundinSystem = (_activity.customer && _activity.customer._id) ? true : false;
            } else {
                _activity.customer = crm.contact.findByCPF({
                    cpf: _activity.cpf
                });

                _activity.personFoundinSystem = (_activity.customer && _activity.customer._id) ? true : false;
            }
            
            setInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_CRM', true);
        } else {
            
            // busca um responsável para o atendimento via CNPJ

            _activity.responsible = crm.contact.findByCPF({
				cpf: _activity.responsibleCPF
			});
			
		    _activity.responsibleFoundinSystem = (_activity.responsible && _activity.responsible._id) ? true : false;
		    setInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_CRM', true);

        }
        
        /////////////////////
        
        var person;
        if (_activity.responsible && _activity.responsible._id) {
            person = _activity.responsible;
        } else if (_activity.customer && _activity.customer._id && _activity.cpf){
            person = _activity.customer;
        }
        // verifica se não há repetição de email e telefone. Se tiver, limpa os repetidos
        if(person){
            var i, j;
            if (person.phones && person.phones.length){
                for (i = 0; i<person.phones.length-1; i++ ){                
                    for ( j = i+1; j<person.phones.length; j++){
                        if (person.phones[i].number && person.phones[j].number && person.phones[i].number === person.phones[j].number) {
                            person.phones[j].number = null;
                        }
                    }
                }
                
                person.phones.forEach(function (phone,index){
                    if (!phone.number) {
                        person.phones[index] = null;
                    }
                });
                
                person.phones.forEach(function (phone,index){
                    if (phone && phone.type) {
                        var isCellPhone = false;
                        if (phone.type._id == "59124071116a1f2ac0ac1e9b"){
                            isCellPhone = true;
                        } else {
                            isCellPhone = false;
                        }
                        try {
                            if (!crm.phone.validatePhoneNumber({
                                number: phone.number,
                                isCellPhone: isCellPhone
                            }).success) {
                                person.phones[index] = null;
                            };
                        } catch (e) {
                            person.phones[index] = null;
                        }
                    }
                });
                
            }
            if (person.emails && person.emails.length){
                for (i = 0; i<person.emails.length-1; i++ ){                
                    for ( j = i+1; j<person.emails.length; j++){
                        if (person.emails[i].address && person.emails[j].address && person.emails[i].address === person.emails[j].address) {
                            person.emails[j].address = null;
                        }
                    }
                }
                
                person.emails.forEach(function (email,index){
                    if (!email.address) {
                        person.emails[index] = null;
                    }
                });
            }
            
            if (_activity.responsible) {
               _activity.responsible = crm.contact._update(person);
            } else if (_activity.customer && _activity.cpf){
                _activity.customer = crm.contact._update(person);
            }
        }
	}

	function seekInReceita() {
		try {
            var receitaResponse
            
            if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {

                if (_activity.cnpj){
                receitaResponse = serasa.receitaFederalIntegration.searchInReceita({
                        cnpj: _activity.cnpj
                    });
                } else {
                receitaResponse = serasa.receitaFederalIntegration.searchInReceita({
                        cpf: _activity.cpf
                    });
                }

            } else {
                receitaResponse = serasa.receitaFederalIntegration.searchInReceita({
                    cpf: _activity.responsibleCPF
                });
            }
			 
            
			if (receitaResponse.StatusCode === '0' || receitaResponse.StatusCode === '00') {
                if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                    if (_activity.cnpj){
                        _activity.RFWSResponse = _utils.stringifyAsJson(receitaResponse.companyData);
                        _activity.companyFoundinRFWS = true;
                    } else {
                        _activity.RFWSResponse = _utils.stringifyAsJson(receitaResponse.contactData);
                        _activity.personFoundinRFWS = true;
                    }
                } else {
                    _activity.RFWSResponse = _utils.stringifyAsJson(receitaResponse.contactData);
                    _activity.responsibleFoundinRFWS = true;
                }
				
				
			} else if (receitaResponse.StatusCode === '23') {
				// Não encontrado.
                _activity.RFWSResponse = null;
                if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                    if (_activity.cnpj){
                        _activity.companyFoundinRFWS = false;
                    } else {
                        _activity.personFoundinRFWS = false;
                    }
                } else {
                    _activity.responsibleFoundinRFWS = false;
                }
			} else {
				// Indefinido.
                _activity.RFWSResponse = null;
                if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                    if (_activity.cnpj){
                        _activity.companyFoundinRFWS = false;
                    } else {
                        _activity.personFoundinRFWS = false;
                    }
                } else {
                    _activity.responsibleFoundinRFWS = false;
                }
			}

		} catch (e) {
            _activity.RFWSResponse = null;
            if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                if (_activity.cnpj){
                    _activity.companyFoundinRFWS = false;
                } else {
                    _activity.personFoundinRFWS = false;
                }
            } else {
                _activity.responsibleFoundinRFWS = false;
            }
			if (e.message) {
				_activity.errorsLog.push("Erro na consulta de CPF/CNPJ pela Receita Federal: " + e.message);
			}
		}
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            setInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_RECEITA', true);
        } else {
            setInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_RECEITA', true);
        }
        
	}

	function seekInSerasaConsumidor() {
        var response, person;
        try {
            if (_activity.serasaConsumidorEmail) {
                response = serasa.serasaConsumidorIntegration.searchInSerasaConsumidorByEmail({
                    email: _activity.serasaConsumidorEmail
                });
    
                if (response && response.found && response.user.cpf) {
                    _activity.cpf = response.user.cpf;
                }
            } else {
                if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                    person = _activity.cpf;
                } else {
                    person = _activity.responsibleCPF;
                }
                response = serasa.serasaConsumidorIntegration.searchInSerasaConsumidor({
                    cpf: person
                });
            }
        } catch (e) {
            if (e.message) {
                _activity.errorsLog.push("Erro na busca em Serasa Consumidor: " + e.message);
            }
        }
    
        if (response && response.found) {
            _activity.serasaConsumidorResponse = _utils.stringifyAsJson(response.user);
            
            if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON') || _activity.serasaConsumidorEmail) {
                _activity.personFoundinSerasaConsumidor = true;
            } else {
                _activity.responsibleFoundinSerasaConsumidor = true;
            }
        } else {
            // Indefinido.
            _activity.serasaConsumidorResponse = null;
            if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON') || _activity.serasaConsumidorEmail) {
                _activity.personFoundinSerasaConsumidor = false;
            } else {
                _activity.responsibleFoundinSerasaConsumidor = false;
            }
            
        }
    
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON') || _activity.serasaConsumidorEmail) {
            setInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_SERASA_CONSUMIDOR', true);
        } else {
            setInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_SERASA_CONSUMIDOR', true);
        }
    }
    
    function seekInCadastroPositivo() {
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            if (_activity.cpf){
                _activity.cadastroPositivoResponse = _utils.stringifyAsJson(serasa.cadastroPositivoIntegration.searchStatusInCadastroPositivo({"cpf": _activity.cpf}));
            } else if(_activity.cnpj){
                _activity.cadastroPositivoResponse = _utils.stringifyAsJson(serasa.cadastroPositivoIntegration.searchStatusInCadastroPositivo({"cnpj": _activity.cnpj}));
            }
        }
    }
    
    if (_activity.serasaConsumidorEmail) {
        seekInSerasaConsumidor();
        if (_activity.personFoundinSerasaConsumidor){
            seekInReceita();
            seekInCrm();
            seekInCadastroPositivo();
        } else {
            _utils.addErrorMessage("Usuário não encontrado no Serasa Consumidor");
        }
    } else {
        seekInReceita();
        seekInCrm();
        seekInCadastroPositivo();
        
        //não possui CNPJ em serasa consumidor
        if (_activity.cpf || getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')){
            seekInSerasaConsumidor();
        }
    }
	
	
}

function seekingClientEndCheck() {
    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
        if (_activity.cnpj){
            return getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_RECEITA') &&
            getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_CRM');
        } else if (_activity.serasaConsumidorEmail) {
            return getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_SERASA_CONSUMIDOR');
        } else {
            return getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_SERASA_CONSUMIDOR') &&
            getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_RECEITA') &&
            getInstanceControlProperty('SEEKING_CLIENT_SOUGHT_IN_CRM');
        }
    } else {
        return getInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_SERASA_CONSUMIDOR') &&
        getInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_RECEITA') &&
        getInstanceControlProperty('SEEKING_RESPONSIBLE_SOUGHT_IN_CRM');
    }
	
}

function seekingClientSequence() {
    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {

        if (_activity.serasaConsumidorEmail && !_activity.cpf) {
            return "PARSE_CLIENT";
        }

        if (_activity.cpf && !_activity.personFoundinRFWS && !_activity.personFoundinSystem && !_activity.personFoundinSerasaConsumidor) {
            return "REGISTER_CUSTOMER";
        }
        
        if (_activity.cnpj && !_activity.companyFoundinRFWS && !_activity.companyFoundinSystem) {
            return "REGISTER_ENTERPRISE";
        }

        if (_activity.cpf && !_activity.personFoundinSystem && (_activity.personFoundinSerasaConsumidor || _activity.personFoundinRFWS)) {
            return "CREATE_CUSTOMER";
        }
        
        if (_activity.cnpj && !_activity.companyFoundinSystem && _activity.companyFoundinRFWS) {
            return "CREATE_ENTERPRISE";
        }

        if (_activity.cpf && _activity.personFoundinSystem && (_activity.personFoundinSerasaConsumidor || _activity.personFoundinRFWS)) {
            return "UPDATE_CUSTOMER";
        }
        
        /*
        Por hora, se a empresa for encontrada no CRM e na Receita não atualiza o CRM com os dados da Receita.
        
        if (_activity.cnpj && _activity.companyFoundinSystem && _activity.companyFoundinRFWS) {
            return "UPDATE_ENTERPRISE";
        }
        
        */
        
        if (_activity.cnpj){
            return Gateways.joinCompanyUpdate();
        } else {
            return Gateways.joinCustomerUpdate();
        }
        
    } else {
        if (!_activity.responsibleFoundinRFWS && !_activity.responsibleFoundinSystem && !_activity.responsibleFoundinSerasaConsumidor) {
            return "REGISTER_CUSTOMER";
        }
    
        if (!_activity.responsibleFoundinSystem && (_activity.responsibleFoundinSerasaConsumidor || _activity.responsibleFoundinRFWS)) {
            return "CREATE_CUSTOMER";
        }
    
        if (_activity.responsibleFoundinSystem && (_activity.responsibleFoundinSerasaConsumidor || _activity.responsibleFoundinRFWS)) {
            return "UPDATE_CUSTOMER";
        }
    
        return Gateways.joinCustomerUpdate();
    }
	
}

function seekingClientEnd() {}

//////////////////////////////////////////////

function registerCustomerStart() {

	function fillContactData() {

		if (!_activity.personToCreate) {
			_activity.personToCreate = {};
		}
        
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
    		if (_activity.customer) {
    			_activity.personToCreate._id = _activity.customer._id;
    			_activity.personToCreate.name = _activity.customer.name ? _activity.customer.name : null;
    			_activity.personToCreate.dateOfBirth = _activity.customer.dateOfBirth ? _activity.customer.dateOfBirth : null;
    		}
        } else {
            if (_activity.responsible) {
    			_activity.personToCreate._id = _activity.responsible._id;
    			_activity.personToCreate.name = _activity.responsible.name ? _activity.responsible.name : null;
    			_activity.personToCreate.dateOfBirth = _activity.responsible.dateOfBirth ? _activity.responsible.dateOfBirth : null;
    		}
        }
        
        let doc;
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            doc = _activity.cpf
        } else {
            doc = _activity.responsibleCPF
        }

		if (!_activity.personToCreate.documents || !_activity.personToCreate.documents.length) {
			_activity.personToCreate.documents = [{
					type: {_id: "5912258ff5ca530b54567bb4"}, //CPF
					number: doc
				}
			];
		}
		
		if (!_activity.personToCreate.phones || !_activity.personToCreate.phones.length) {
		    _activity.personToCreate.phones = [{
                type: {_id: "59124071116a1f2ac0ac1e9b"},     //Celular
                country: {_id: "590b71c2f5ca53091c89ea4e"},  //Brasil
                number: ""
            }];
		}
		
		if (!_activity.personToCreate.emails || !_activity.personToCreate.emails.length) {
		    _activity.personToCreate.emails = [{
                type: {_id: "59122c5f116a1f1dccc73472"},    //Pessoal
                address: ""
            }];
		}		
	}


	if (!_activity.URAOrigin) {
		fillContactData();
	}
}

function registerCustomerOnState() {

	function fillContactDataURA() {
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            if (_activity.customer) {
			    _activity.personToCreate._id = _activity.customer._id;
			    _activity.personToCreate.name = _activity.customer.name ? _activity.customer.name : null;
			    _activity.personToCreate.dateOfBirth = _activity.customer.dateOfBirth ? _activity.customer.dateOfBirth : null;
		    }
        } else {
            if (_activity.responsible) {
			    _activity.personToCreate._id = _activity.responsible._id;
			    _activity.personToCreate.name = _activity.responsible.name ? _activity.responsible.name : null;
			    _activity.personToCreate.dateOfBirth = _activity.responsible.dateOfBirth ? _activity.responsible.dateOfBirth : null;
		    }
        }

        let doc;
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            doc = _activity.cpf
        } else {
            doc = _activity.responsibleCPF
        }
        
		if (!_activity.personToCreate.documents || !_activity.personToCreate.documents.length) {
			_activity.personToCreate.documents = [{
					"type": {
						"_id": "5912258ff5ca530b54567bb4"
					},
					"number": doc
				}
			];
		}
		setInstanceControlProperty('REGISTER_CUSTOMER_FILL_CONTACT_URA', true);
	}

	function personToCreateIsValid() {
		var errors = [];
		var contactHasCPF = _activity.personToCreate && _activity.personToCreate.documents && _activity.personToCreate.documents.length &&
			_activity.personToCreate.documents.find(function (doc) {
				if (doc.type && doc.type._id === "5912258ff5ca530b54567bb4" && doc.number) { //CPF
					return true;
				}
			});

		if (!contactHasCPF) {
			errors.push("É necessário cadastrar o CPF do consumidor.");
		}
		if (!(_activity.personToCreate && _activity.personToCreate.name)) {
			errors.push("É necessário cadastrar o nome do consumidor.");
		}
		if (!(_activity.personToCreate && _activity.personToCreate.dateOfBirth)) {
			errors.push("É necessário cadastrar a data de nascimento do consumidor.");
		}

		if (errors.length === 0) {
			return true;
		} else {
			errors.forEach(function (error) {
				_utils.addErrorMessage(error);
			});
			return false;
		}

	}

	/////////////////////////////////////

    var i, j;
	if (_activity.personToCreate.phones && _activity.personToCreate.phones.length && _activity.personToCreate.phones.length > 1){
    
        for (i = 0; i<_activity.personToCreate.phones.length-1; i++ ){
            
            for ( j = i+1; j<_activity.personToCreate.phones.length; j++){
                if (_activity.personToCreate.phones[i].number && _activity.personToCreate.phones[j].number && _activity.personToCreate.phones[i].number === _activity.personToCreate.phones[j].number) {
                    _activity.personToCreate.phones[j].number = "";
                    _utils.addErrorMessage("O Telefone inserido já foi informado.");
                }
            }
        }
    }
    
    if (_activity.personToCreate.emails && _activity.personToCreate.emails.length && _activity.personToCreate.emails.length > 1){
        
        for (i = 0; i<_activity.personToCreate.emails.length-1; i++ ){
                    
            for ( j = i+1; j<_activity.personToCreate.emails.length; j++){
                if (_activity.personToCreate.emails[i].address && _activity.personToCreate.emails[j].address && _activity.personToCreate.emails[i].address === _activity.personToCreate.emails[j].address) {
                    _activity.personToCreate.emails[j].address = "";
                        _utils.addErrorMessage("O Email inserido já foi informado.");
                }
            }
        }
    }
    
	if (_activity.personToCreate && !getInstanceControlProperty('REGISTER_CUSTOMER_FILL_CONTACT_URA')) {
		fillContactDataURA();
	}

	if (_activity.createPersonTrigger) {
		if (personToCreateIsValid()) {
			setInstanceControlProperty('REGISTER_CUSTOMER_VALID_REGISTER', true);
		} else {
			setInstanceControlProperty('REGISTER_CUSTOMER_VALID_REGISTER', false);
			_activity.createPersonTrigger = false;
			_utils.addErrorMessage("Cadastro de pessoa inválido.");
		}
	}
}

function registerCustomerEndCheck() {
	return getInstanceControlProperty('REGISTER_CUSTOMER_VALID_REGISTER');
}

function registerCustomerSequence() {
	return "CREATE_CUSTOMER";
}

function registerCustomerEnd() {}

//////////////////////////////////////////////



function registerEnterpriseStart() {

	function fillCompanyData() {

		if (!_activity.companyToCreate) {
			_activity.companyToCreate = {};
		}

		if (_activity.customer) {
			_activity.companyToCreate._id = _activity.customer._id;
			_activity.companyToCreate.name = _activity.customer.name ? _activity.customer.name : null;
			_activity.companyToCreate.companyName = _activity.customer.companyName ? _activity.customer.companyName : null;
		}

		if (!_activity.companyToCreate.documents || !_activity.companyToCreate.documents.length) {
			_activity.companyToCreate.documents = [{
					type: {_id: "5912257df5ca530b545667f9"}, //CNPJ
					number: _activity.cnpj
				}
			];
		}	
	}


	if (!_activity.URAOrigin) {
		fillCompanyData();
	}
}

function registerEnterpriseOnState() {

	function fillCompanyDataURA() {

		if (_activity.customer) {
		    _activity.companyToCreate._id = _activity.customer._id;
			_activity.companyToCreate.name = _activity.customer.name ? _activity.customer.name : null;
			_activity.companyToCreate.companyName = _activity.customer.companyName ? _activity.customer.companyName : null;
			
		}

		if (!_activity.companyToCreate.documents || !_activity.companyToCreate.documents.length) {
			_activity.companyToCreate.documents = [{
					"type": {
						"_id": "5912257df5ca530b545667f9"
					},
					"number": _activity.cnpj
				}
			];
		}
		setInstanceControlProperty('REGISTER_ENTERPRISE_FILL_CONTACT_URA', true);
	}

	function companyToCreateIsValid() {
		var errors = [];
		var companyHasCNPJ = _activity.companyToCreate && _activity.companyToCreate.documents && _activity.companyToCreate.documents.length &&
			_activity.companyToCreate.documents.find(function (doc) {
				if (doc.type && doc.type._id === "5912257df5ca530b545667f9" && doc.number) { //CNPJ
					return true;
				}
			});

		if (!companyHasCNPJ) {
			errors.push("É necessário cadastrar o CNPJ do consumidor.");
		}
		
		if (!(_activity.companyToCreate && _activity.companyToCreate.name)) {
			errors.push("É necessário cadastrar o nome da empresa.");
		}
		
		if (!(_activity.companyToCreate && _activity.companyToCreate.companyName)) {
			errors.push("É necessário cadastrar a Razão Social da empresa.");
		}

		if (errors.length === 0) {
			return true;
		} else {
			errors.forEach(function (error) {
				_utils.addErrorMessage(error);
			});
			return false;
		}

	}

	/////////////////////////////////////

	if (_activity.companyToCreate && !getInstanceControlProperty('REGISTER_ENTERPRISE_FILL_CONTACT_URA')) {
		fillCompanyDataURA();
	}

	if (_activity.createCompanyTrigger) {
		if (companyToCreateIsValid()) {
			setInstanceControlProperty('REGISTER_ENTERPRISE_VALID_REGISTER', true);
		} else {
			setInstanceControlProperty('REGISTER_ENTERPRISE_VALID_REGISTER', false);
			_activity.createCompanyTrigger = false;
			_utils.addErrorMessage("Cadastro de empresa inválido.");
		}
	}
}

function registerEnterpriseEndCheck() {
	return getInstanceControlProperty('REGISTER_ENTERPRISE_VALID_REGISTER');
}

function registerEnterpriseSequence() {
	return "CREATE_ENTERPRISE";
}

function registerEnterpriseEnd() {}





//////////////////////////////////////////////
// CREATE_CUSTOMER




function createCustomerStart() {
	setInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED', false);
}

function createCustomerOnState() {
	if (_activity.personToCreate) {
        let person;
        
        if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            
    		if (_activity.customer) {
    			_activity.customer = crm.contact._update(_activity.personToCreate);
    		} else {
    			_activity.customer = crm.contact._create(_activity.personToCreate);
    		}
    		person = _activity.customer;
        } else {
            if (_activity.responsible) {
    			_activity.responsible = crm.contact._update(_activity.personToCreate);
    		} else {
    			_activity.responsible = crm.contact._create(_activity.personToCreate);
    		}
    		person = _activity.responsible;
        }
		_activity.personToCreate = null;

		if (_activity.personFoundinSerasaConsumidor || _activity.responsibleFoundinSerasaConsumidor) {
			insertSerasaConsumidorData();
			
			var paramsName, paramsBirth;
			
			paramsName = {
			    "name": person.name,
			    "user": JSON.parse(_activity.serasaConsumidorResponse)
			};
			    
			paramsBirth = {
			   "birthDate": person.dateOfBirth.getFullYear() + "-" + pad(person.dateOfBirth.getMonth() + 1) + "-" + pad(person.dateOfBirth.getUTCDate()),
			   "user": JSON.parse(_activity.serasaConsumidorResponse)
			};
			
			serasa.serasaConsumidorIntegration.editName(paramsName);
			serasa.serasaConsumidorIntegration.editBirthDate(paramsBirth);
		}
		_activity.contactCreationError = false;

	} else {
	    if (_activity.personFoundinRFWS|| _activity.responsibleFoundinRFWS) {
			var rfPerson = JSON.parse(_activity.RFWSResponse);
			try {

                let doc;
                if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
                    doc = _activity.cpf;
                } else {
                    doc = _activity.responsibleCPF;
                }

			    let person = serasa.receitaFederalIntegration.createContactWithReceitaData({
					"contactData": rfPerson,
					"cpf": doc
				});
				
				if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
				    _activity.customer = person.customer;
				} else {
				    _activity.responsible = person.customer;
				}
				
				// if (_activity.personFoundinSerasaConsumidor || _activity.responsibleFoundinSerasaConsumidor) {
				// 	insertSerasaConsumidorData();
				// }
			} catch (e) {
				_activity.contactCreationError = true;
				if (e.message) {
					_activity.errorsLog.push("Erro na criação de contato pela Receita Federal: " + e.message);
				}
			}
		}else if (_activity.personFoundinSerasaConsumidor || _activity.responsibleFoundinSerasaConsumidor) {
			var serasaConsumidorUser = JSON.parse(_activity.serasaConsumidorResponse);
			try {
			    
			    let person = serasa.serasaConsumidorIntegration.createContactWithSerasaConsumidorData({
					"contactData": serasaConsumidorUser
				});
				
				if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
				    _activity.customer = person.customer;
				} else {
				    _activity.responsible = person.customer;
				}
				
			} catch (e) {
				_activity.contactCreationError = true; //Try-catch para capturar erro na DQ (customer não criado)
				if (e.message) {
					_activity.errorsLog.push("Erro na criação de contato pelo Serasa Consumidor: " + e.message);
				}
			}
		}
	}
    
    
    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
        var contactIncomplete = _activity.customer && (!_activity.customer.name || !_activity.customer.dateOfBirth) ? true : false;
    } else {
        var contactIncomplete = _activity.responsible && (!_activity.responsible.name || !_activity.responsible.dateOfBirth) ? true : false;
    }
	

	if (!(_activity.contactCreationError || contactIncomplete)) {
		setInstanceControlProperty('CLIENT_UPDATED', true);
	} else {
		setInstanceControlProperty('CLIENT_UPDATED', false);
	}
	setInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED', true);

	//////////////////

	function insertSerasaConsumidorData() {
		var personData = JSON.parse(_activity.serasaConsumidorResponse);
		if (!personData) {
		    return;
		}
		
		var lastAccess
		
		if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
    		lastAccess = personData.last_access_date ? new Date(personData.last_access_date.toString().split('.')[0].replace(' ', 'T')) : null;
    		_activity.customer.serasaConsumidorRegistration = {
    			"status": personData.status,
    			"email": personData.email,
    			"registerDate": personData.created_at ? new Date(personData.created_at.toString().split('.')[0].replace(' ', 'T')) : null,
    			"socialNetwork": getSocialNetwork(personData.data),
    			"lastAccessDate": lastAccess
    		};
            
            
    		if (_activity.customer.emails == null) {
    			_activity.customer.emails = [];
    		}
    
    		if (_activity.customer.phones == null) {
    			_activity.customer.phones = [];
    		}
    		if (personData.email && crm.email.validateEmailAddress({
                    "address": personData.email
                }).success) {
    			_activity.customer.emails.push({
    				"type": {
    					"_id": "59122c5f116a1f1dccc73472",
    					"_classId": "591213de116a1f2b04df831a"
    				},
    				"address": personData.email
    			});
    		}
    		if (personData.cellphone && crm.phone.validatePhoneNumber({
    				number: personData.cellphone.slice(-11),
    				isCellPhone: true
    			}).success) {
    			_activity.customer.phones.push({
    				"type": {
    					"_id": "59124071116a1f2ac0ac1e9b",
    					"_classId": "59123cd8116a1f0b8428ad54"
    				},
    				"country": {
    					"_id": "590b71c2f5ca53091c89ea4e",
    					"_classId": "000000000000000000000040"
    				},
    				"number": personData.cellphone.slice(-11)
    			});
    		}
    		_activity.customer = crm.contact._update(_activity.customer);
    		
		} else {
		    
		    lastAccess = personData.last_access_date ? new Date(personData.last_access_date.toString().split('.')[0].replace(' ', 'T')) : null;
    		_activity.responsible.serasaConsumidorRegistration = {
    			"status": personData.status,
    			"email": personData.email,
    			"registerDate": new Date(personData.created_at.toString().split('.')[0].replace(' ', 'T')),
    			"socialNetwork": getSocialNetwork(personData.data),
    			"lastAccessDate": lastAccess
    		};
            
            
    		if (_activity.responsible.emails == null) {
    			_activity.responsible.emails = [];
    		}
    
    		if (_activity.responsible.phones == null) {
    			_activity.responsible.phones = [];
    		}
    		if (personData.email) {
    			_activity.customer.emails.push({
    				"type": {
    					"_id": "59122c5f116a1f1dccc73472",
    					"_classId": "591213de116a1f2b04df831a"
    				},
    				"address": personData.email
    			});
    		}
    		if (personData.cellphone && crm.phone.validatePhoneNumber({
    				number: personData.cellphone.slice(-11),
    				isCellPhone: true
    			}).success) {
    			_activity.customer.phones.push({
    				"type": {
    					"_id": "59124071116a1f2ac0ac1e9b",
    					"_classId": "59123cd8116a1f0b8428ad54"
    				},
    				"country": {
    					"_id": "590b71c2f5ca53091c89ea4e",
    					"_classId": "000000000000000000000040"
    				},
    				"number": personData.cellphone.slice(-11)
    			});
    		}
    		_activity.responsible = crm.contact._update(_activity.responsible);
		}
	}

	function getSocialNetwork(data) {
		var socialText = null;
		if (data && data.google) {
			socialText = "Google";
			if (data.google.associated_at) {
				socialText += ' em ' + getDateText(data.google.associated_at);
			}
		} else if (data && data.facebook) {
			socialText = "Facebook";
			if (data.facebook.associated_at) {
				socialText += ' em ' + getDateText(data.facebook.associated_at);
			}
		}
		return socialText;
	}

	function getDateText(dateText) {
		return new Date(dateText.toString().split('.')[0].replace(' ', 'T')).toLocaleDateString();
	}

}

function createCustomerEndCheck() {
	return getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED');
}

function createCustomerSequence() {
	return Gateways.joinCustomerUpdate();
}

function createCustomerEnd() {}






//////////////////////////////////////////////
// CREATE_ENTERPRISE

function createEnterpriseStart() {
	setInstanceControlProperty('CREATE_ENTERPRISE_ACTION_COMPLETED', false);
}

function createEnterpriseOnState() {
	if (_activity.companyToCreate) {

		if (_activity.customer) {
			_activity.customer = crm.enterprise._update(_activity.companyToCreate);
		} else {
			_activity.customer = crm.enterprise._create(_activity.companyToCreate);
		}
		_activity.companyToCreate = null;

		_activity.contactCreationError = false;

	} else {
		if (_activity.companyFoundinRFWS) {
			var rfCompany = JSON.parse(_activity.RFWSResponse);
			rfCompany.NomeFantasia = rfCompany.NomeCliente;
			try {
				_activity.customer = serasa.receitaFederalIntegration.createCompanyWithReceitaData({
						"companyData": rfCompany,
						"cnpj": _activity.cnpj
					}).customer;
			} catch (e) {
				_activity.contactCreationError = true;
				if (e.message) {
					_activity.errorsLog.push("Erro na criação de contato pela Receita Federal: " + e.message);
				}
			}
		}
	}

	var contactIncomplete = _activity.customer && (!_activity.customer.name || !_activity.customer.companyName) ? true : false;

	if (!(_activity.contactCreationError || contactIncomplete)) {
		setInstanceControlProperty('CLIENT_UPDATED', true);
	} else {
		setInstanceControlProperty('CLIENT_UPDATED', false);
	}
	setInstanceControlProperty('CREATE_ENTERPRISE_ACTION_COMPLETED', true);

}

function createEnterpriseEndCheck() {
	return getInstanceControlProperty('CREATE_ENTERPRISE_ACTION_COMPLETED');
}

function createEnterpriseSequence() {
	return Gateways.joinCompanyUpdate();
}

function createEnterpriseEnd() {}


///////////////////////////
// UPDATE_CUSTOMER


function updateCustomerStart() {}

function updateCustomerOnState() {

	let doc;
	
	if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
        doc = _activity.cpf
    } else {
        doc = _activity.responsibleCPF
    }

    var person = crm.contact.findByCPF({
	    cpf: doc
    });
    

	if ((_activity.personFoundinRFWS || _activity.responsibleFoundinRFWS) && _activity.RFWSResponse) {
		var rfPerson = JSON.parse(_activity.RFWSResponse);

		if (rfPerson) {
			person.receitaFederal = {};
			person.receitaFederal.rfLastConsultationDate = new Date();
			person.receitaFederal.rfSituation = getRFSituation(rfPerson.Situacao);
			person.mothersName = rfPerson.NomeMae;
			person.name = rfPerson.NomeCliente;
			person.gender = rfPerson.Sexo ? formatWord(rfPerson.Sexo) : null;
			person.dateOfBirth = buildBirthDate(rfPerson.DataNascimento);
		}
	}

	if (_activity.personFoundinSerasaConsumidor || _activity.responsibleFoundinSerasaConsumidor) {
		insertSerasaConsumidorData(person);
	}

	try {
	    if (!getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
	        _activity.customer = crm.contact._update(person);
	    } else {
	        _activity.responsible = crm.contact._update(person);
	    }
		
	} catch (e) {
		if (e.message) {
			_activity.errorsLog.push("Erro no _update do contato: " + e.message);
		}
	}

	setInstanceControlProperty('UPDATE_CUSTOMER_ACTION_COMPLETED', true);

	////////////////////////
	function getRFSituation(codeSituation) {
		var query = {
			"query": {
				"term": {
					"code": codeSituation
				}
			}
		};
		var result = crm.documentStatusAtReceita._search(query);
		if (result && result.hits && result.hits.total === 1) {
			return result.hits.hits[0]._source;
		} else {
			var naoIdentificadoID = "5b71b063b316392fd85a171a";
			return {
				"_id": naoIdentificadoID
			};
		}
	}

	function formatWord(word) {
		if (word) {
			var lowered = word.toLowerCase();
			return lowered.charAt(0).toUpperCase() + lowered.substr(1);
		}
		return null;
	}

	function buildBirthDate(dateString) {
		if (dateString.length === 7) {
			dateString = '0' + dateString;
		}
		var date = dateString.substr(0, 2);
		var month = dateString.substr(2, 2);
		var year = dateString.substr(4, 4);

		return new Date(parseInt(year, 10), parseInt(month, 10) - 1, parseInt(date, 10));
	}

	function insertSerasaConsumidorData(person) {
		var personData = JSON.parse(_activity.serasaConsumidorResponse);
		if (!personData) {
		    return;
		}
		var lastAccess = personData.last_access_date ? new Date(personData.last_access_date.toString().split('.')[0].replace(' ', 'T')) : null;
		person.serasaConsumidorRegistration = {
			"status": personData.status,
			"email": personData.email,
			"registerDate": personData.created_at ? new Date(personData.created_at.toString().split('.')[0].replace(' ', 'T')) : null,
			"socialNetwork": getSocialNetwork(personData.data),
			"lastAccessDate": lastAccess
		};

		if (person.emails == null) {
			person.emails = [];
		}
		if (person.phones == null) {
			person.phones = [];
		}
		if (personData.email && crm.email.validateEmailAddress({
                "address": personData.email
            }).success) {
        
            var registeredMail = false;
            if (person && person.emails && person.emails.length) {
                person.emails.forEach(function (email){
                    if (email.address ===  personData.email) {
                        registeredMail = true;
                    }
                });
            }
        
            if (!registeredMail){
                person.emails.push({
                    "type": {
                        "_id": "59122c5f116a1f1dccc73472",
                        "_classId": "591213de116a1f2b04df831a"
                    },
                    "address": personData.email,
                });
            }
        }
        
		if (personData.cellphone && crm.phone.validatePhoneNumber({
				number: personData.cellphone.slice(-11),
				isCellPhone: true
			}).success) {
			var registeredPhone = false;
            if (person && person.phones && person.phones.length) {
                var consumerPhone = personData.cellphone.slice(-11);
                person.phones.forEach(function (phone){
                    if (phone.number === consumerPhone) {
                        registeredPhone = true;
                    }
                });
            }
            
            if (!registeredPhone){
    			person.phones.push({
    				"type": {
    					"_id": "59124071116a1f2ac0ac1e9b",
    					"_classId": "59123cd8116a1f0b8428ad54"
    				},
    				"country": {
    					"_id": "590b71c2f5ca53091c89ea4e",
    					"_classId": "000000000000000000000040"
    				},
    				"number": personData.cellphone.slice(-11)
    			});
            }
		}
	}

	function getSocialNetwork(data) {
		var socialText = null;
		if (data && data.google) {
			socialText = "Google";
			if (data.google.associated_at) {
				socialText += ' em ' + getDateText(data.google.associated_at);
			}
		} else if (data && data.facebook) {
			socialText = "Facebook";
			if (data.facebook.associated_at) {
				socialText += ' em ' + getDateText(data.facebook.associated_at);
			}
		}
		return socialText;
	}

	function getDateText(dateText) {
		return new Date(dateText.toString().split('.')[0].replace(' ', 'T')).toLocaleDateString();
	}
}

function updateCustomerEndCheck() {
	return getInstanceControlProperty('UPDATE_CUSTOMER_ACTION_COMPLETED');
}

function updateCustomerSequence() {
	return Gateways.joinCustomerUpdate();
}

function updateCustomerEnd() {}




///////////////////////////
// UPDATE_ENTERPRISE


function updateEnterpriseStart() {}

function updateEnterpriseOnState() {

	var firm = crm.enterprise.findByCNPJ({
			cnpj: _activity.cnpj
		});

	if (_activity.companyFoundinRFWS && _activity.RFWSResponse) {
		var rfCompany = JSON.parse(_activity.RFWSResponse);

		if (rfCompany) {
			firm.receitaFederal = {};
			firm.receitaFederal.rfLastConsultationDate = new Date();
			firm.receitaFederal.rfSituation = getRFSituation(rfCompany.Situacao);
			firm.companyName = rfCompany.NomeCliente;
			if (rfCompany.NomeFantasia) {
			    firm.name = rfCompany.NomeFantasia;
			} else {
			    firm.name = rfCompany.NomeCliente;
			}
		}
	}

	try {
		_activity.customer = crm.enterprise._update(firm);
	} catch (e) {
		if (e.message) {
			_activity.errorsLog.push("Erro no _update do contato: " + e.message);
		}
	}

	setInstanceControlProperty('UPDATE_ENTERPRISE_ACTION_COMPLETED', true);

	////////////////////////
	function getRFSituation(codeSituation) {
		var query = {
			"query": {
				"term": {
					"code": codeSituation
				}
			}
		};
		var result = crm.documentStatusAtReceita._search(query);
		if (result && result.hits && result.hits.total === 1) {
			return result.hits.hits[0]._source;
		} else {
			var naoIdentificadoID = "5b71b063b316392fd85a171a";
			return {
				"_id": naoIdentificadoID
			};
		}
	}
}

function updateEnterpriseEndCheck() {
	return getInstanceControlProperty('UPDATE_ENTERPRISE_ACTION_COMPLETED');
}

function updateEnterpriseSequence() {
	return Gateways.joinCompanyUpdate();
}

function updateEnterpriseEnd() {}









/////////////////////////////////////////////
// ATENDIMENTO_ESCRITORIOS

function atendimentoEscritoriosStart() {}

function atendimentoEscritoriosOnState() {
    
    
    function isValidCPF(cpf) {
		var v = crm.document.validateCPF({
				"cpf": cpf
			});
		if (!v || !v.success) {
			throw 'O CPF informado ' + cpf + ' não é válido.';
		}

		return v && v.success;
	}
	
	function isValidCNPJ(cnpj) {
		var v = crm.document.validateCNPJ({
				"cnpj": cnpj
			});
		if (!v || !v.success) {
			throw 'O CNPJ informado ' + _activity.cnpj + ' não é válido.';
		}

		return v && v.success;
    }
    
    if (_activity.continue === true) {
        if (_activity.outorganteCPF) {
            if (isValidCPF(_activity.outorganteCPF)) {
                setInstanceControlProperty('VALID_OUTORGANTE_CPF', true);
            } else {
                _activity.continue = false;
            }
        } else {
            setInstanceControlProperty('DONT_HAVE_OUTORGANTE_CPF', true);
        }
        
        if (_activity.cnpj) {
            if (isValidCNPJ(_activity.cnpj)) {
                setInstanceControlProperty('VALID_OUTORGANTE_OR_OUTORGADO_CNPJ', true);
            } else {
                _activity.continue = false;
            }
        } else {
            setInstanceControlProperty('DONT_HAVE_OUTORGANTE_OR_OUTORGADO_CNPJ', true);
        }
    }
}

function atendimentoEscritoriosEndCheck() {
    if ((getInstanceControlProperty('VALID_OUTORGANTE_CPF') || (getInstanceControlProperty('DONT_HAVE_OUTORGANTE_CPF'))) && 
    (getInstanceControlProperty('VALID_OUTORGANTE_OR_OUTORGADO_CNPJ') || (getInstanceControlProperty('DONT_HAVE_OUTORGANTE_OR_OUTORGADO_CNPJ')))) {
        return true;
    }
}

function atendimentoEscritoriosSequence() {
    
    // if (_activity.customer && _activity.outorganteCPF) {
    //     return "CUSTOMER_PROCURATION";
    // }
    
    if (_activity.cnpj) {
        return "CREDIT_RATING";
    }
    if (_activity.cpf && getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED') && !_activity.personFoundinRFWS && !_activity.personFoundinSerasaConsumidor) {
        return "PERFORM_ATTENDANCE";
    } else {
        return "POSITIVE_IDENTIFICATION";
    }
}

function atendimentoEscritoriosEnd() {
    // seta a flag para false para reutilizá-la
    _activity.continue = false;
}




//////////////////////////////////////////////
// CUSTOMER_PROCURATION
/**********************/
// function customerProcurationStart() {}

// function customerProcurationOnState() {
    
//     // script da procuração
    
    
    
    
// }

// function customerProcurationEndCheck() {
    
    
// }

// function customerProcurationSequence() {
//     if (_activity.cnpj) {
//         return "CREDIT_RATING";
//     } else {
//         if (_activity.cpf && getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED') && !_activity.personFoundinRFWS && !_activity.personFoundinSerasaConsumidor) {
//             return "PERFORM_ATTENDANCE";
//         } else {
//             return "POSITIVE_IDENTIFICATION";
//         }
//     }
// }

// function customerProcurationEnd() {}
/**********************/



//////////////////////////////////////////////
// CREDIT_RATING

function creditRatingStart() {}

function creditRatingOnState() {
    
    var doc;
    
    if (_activity.outorganteCPF) {
        doc = _activity.outorganteCPF
    } else {
        doc = _activity.cpf
    }
    
    var docCPF = doc.substr(0,9);
    
    var docCNPJ = "0"+_activity.cnpj.substr(0,8);
    
    var params = {
    	"identifier": "creditRating",
    	"method": "GET",
    	"request": {
    		"logon_funcionario": _user.login,
    		"id_atendimento": _activity.protocol,
    		"documento_participante": docCPF,
    		"documento_empresa": docCNPJ
    	}
    };
    
    try {
        var creditRatingResponse = com.sydle.util.webService.send(params);
        _utils.log("zxcv creditRatingResponse: "+_utils.stringifyAsJson(creditRatingResponse));
        
        if (creditRatingResponse && creditRatingResponse.response && creditRatingResponse.response.response && 
        creditRatingResponse.response.response.indicador_participacao === true) {
            setInstanceControlProperty('CREDIT_RATING_POSITIVE_RESPONSE', true);
        } else if (creditRatingResponse && creditRatingResponse.response && creditRatingResponse.response.response && 
        creditRatingResponse.response.response.indicador_participacao === false) {
            setInstanceControlProperty('CREDIT_RATING_NEGATIVE_RESPONSE', true);
        }
    } catch (e) {
        setInstanceControlProperty('CREDIT_RATING_FAIL_RESPONSE', true);
    }
    
    
}

function creditRatingEndCheck() {
    
    if (getInstanceControlProperty('CREDIT_RATING_POSITIVE_RESPONSE') || 
    getInstanceControlProperty('CREDIT_RATING_NEGATIVE_RESPONSE') ||
    getInstanceControlProperty('CREDIT_RATING_FAIL_RESPONSE')) {
        return true;
    }
}

function creditRatingSequence() {
    
    if (getInstanceControlProperty('CREDIT_RATING_NEGATIVE_RESPONSE') || getInstanceControlProperty('CREDIT_RATING_FAIL_RESPONSE')) {
        return finalizeActivity("CREDIT_RATING");
    }
    
    if (_activity.cpf && getInstanceControlProperty('CREATE_CUSTOMER_ACTION_COMPLETED') && !_activity.personFoundinRFWS && !_activity.personFoundinSerasaConsumidor) {
        return "PERFORM_ATTENDANCE";
    } else {
        return "POSITIVE_IDENTIFICATION";
    }
}

function creditRatingEnd() {}





//////////////////////////////////////////////

function positiveIdentificationStart() {
    
    function fillEmailContact() {
        var emails;
        if (_activity.responsibleCPF || _activity.responsible){
            emails = _activity.responsible.emails ? _activity.responsible.emails : [];
        } else {
            emails = _activity.customer.emails ? _activity.customer.emails : [];
        }
        _activity.emailContact = [];
        
        if (emails && emails.length) {
            emails.forEach(function(email) {
                _activity.emailContact.push({
                    mainContact: false,
                    type: email.type,
                    address: email.address
                });
            });
        } else {
            _activity.emailContact.push({
                mainContact: false,
                type: {_id: "59122c5f116a1f1dccc73472"},    //Pessoal
                address: ""
            });
        }
        _activity.emailContact[0].mainContact = true;
    }
    
    function fillEmailCompany() {
        var emails = _activity.customer.emails ? _activity.customer.emails : [];
        _activity.emailContact = [];
        
        if (emails && emails.length) {
            emails.forEach(function(email) {
                _activity.emailContact.push({
                    mainContact: false,
                    type: email.type,
                    address: email.address
                });
            });
        } else {
            _activity.emailContact.push({
                mainContact: false,
                type: {_id: "59122c81116a1f1dccc7495e"},    //Comercial
                address: ""
            });
        }
        _activity.emailContact[0].mainContact = true;
    }

    
    function fillPhoneContact() {
        var phones;
        if (_activity.responsibleCPF || _activity.responsible){
            phones = _activity.responsible.phones ? _activity.responsible.phones : [];
        } else {
            phones = _activity.customer.phones ? _activity.customer.phones : [];
        }
        _activity.phoneContact = [];
        
        if (phones && phones.length) {
            phones.forEach(function(phone) {
                _activity.phoneContact.push({
                    mainContact: false,
                    type: phone.type,
                    country: phone.country,
                    number: phone.number
                });
            });
        } else {
            _activity.phoneContact.push({
                mainContact: false,
                type: {_id: "59124071116a1f2ac0ac1e9b"},    //Celular
                country: {_id: "590b71c2f5ca53091c89ea4e"},  //Brasil
                number: ""
            });        
        }
        _activity.phoneContact[0].mainContact = true;
    }  
    
    function fillPhoneCompany() {
        var phones = _activity.customer.phones ? _activity.customer.phones : [];
        _activity.phoneContact = [];
        
        if (phones && phones.length) {
            phones.forEach(function(phone) {
                _activity.phoneContact.push({
                    mainContact: false,
                    type: phone.type,
                    country: phone.country,
                    number: phone.number
                });
            });
        } else {
            _activity.phoneContact.push({
                mainContact: false,
                type: {_id: "59124046116a1f2ac0aba5df"},    //Comercial
                country: {_id: "590b71c2f5ca53091c89ea4e"},  //Brasil
                number: ""
            });        
        }
        _activity.phoneContact[0].mainContact = true;
    }  
    
    ////////////////////
    _activity.contactDataConfirmation = false;
    
    if (_activity.cpf || _activity.responsibleCPF || _activity.responsible) {
        fillPhoneContact();
        fillEmailContact();
    } else {
        fillPhoneCompany();
        fillEmailCompany();
    } 
    
    
}

function positiveIdentificationOnState() {
    
    phoneAndEmailAndAddressMainContactControl();
    
    var i, j;
    if (_activity.phoneContact && _activity.phoneContact.length && _activity.phoneContact.length > 1){
        
        var country = _location._country._search({
            "query": {
                "match":{
                    "_id": "590b71c2f5ca53091c89ea4e"
                }
            }
        }).hits.hits[0]._source;
        
        _activity.phoneContact.forEach(function (phone){
            if (!phone.country){
                phone.country = country;
            }
    
        });
    
        for (i = 0; i<_activity.phoneContact.length-1; i++ ){
            
            for ( j = i+1; j<_activity.phoneContact.length; j++){
                if (_activity.phoneContact[i].number && _activity.phoneContact[j].number && _activity.phoneContact[i].number === _activity.phoneContact[j].number) {
                    _activity.phoneContact[j].number = "";
                    _utils.addErrorMessage("O Telefone inserido já está cadastrado.");
                }
            }
        }
    }
    
    if (_activity.emailContact && _activity.emailContact.length && _activity.emailContact.length > 1){
        for (i = 0; i<_activity.emailContact.length-1; i++ ){
                    
            for ( j = i+1; j<_activity.emailContact.length; j++){
                if (_activity.emailContact[i].address && _activity.emailContact[j].address && _activity.emailContact[i].address === _activity.emailContact[j].address) {
                    _activity.emailContact[j].address = "";
                    _utils.addErrorMessage("O Email inserido já está cadastrado.");
                }
            }
        }
    }
}

function positiveIdentificationEndCheck() {
    if (_activity.contactDataConfirmation) {
        return true;
    }
}

function positiveIdentificationSequence() {
    
    function checkForOpenAttendance() {
    	var query;
    	
    	if (_activity.cnpj){
    	    query = {
        		"query": {
        			"bool": {
        				"must": [{
    						"nested": {
    							"path": "customer.documents",
    							"query": {
    								"term": {
    									"customer.documents.number.keyword": _activity.cnpj
    								}
    							}
    						}
    					}, {
    						"term": {
    							"_status.identifier.keyword": "open"
    						}
    					}],
        				"must_not": {
        					"term": {
        						"_id": _object._id
        					}
        				}
        			}
        		},
        		"size": 0
        	};
    	} else {
    	    query = {
        		"query": {
        			"bool": {
        				"must": [{
    						"nested": {
    							"path": "customer.documents",
    							"query": {
    								"term": {
    									"customer.documents.number.keyword": _activity.cpf
    								}
    							}
    						}
    					}, {
    						"term": {
    							"_status.identifier.keyword": "open"
    						}
    					}],
        				"must_not": {
        					"term": {
        						"_id": _object._id
        					}
        				}
        			}
        		},
        		"size": 0
        	};
    	}
    	
    	var openAttendance = _process.attendance.current._search(query);
    	if (openAttendance && openAttendance.hits && openAttendance.hits.total > 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    if (!_activity.responsibleCPF && !getInstanceControlProperty('ATTENDANCE_REALIZED_BY_ATENDIMENTO_ESCRITORIOS_GROUP')) {
        if (checkForOpenAttendance()) {
        	return "REGISTER_PHONE_CONTACT";
        }
    }
    
    return "PERFORM_ATTENDANCE";
        
}

function positiveIdentificationEnd() {

    function updateContact() {
        var customer;
        if (_activity.cnpj) {
            customer = crm.contact._get(_activity.responsible);
        } else {
            customer = crm.contact._get(_activity.customer);
        }
        
        if (_activity.emailContact && _activity.emailContact.length) {
            customer.emails = [];
            _activity.emailContact.forEach(function(email) {
                
                if (!email.type){
                    throw "O tipo do email é obrigatório.";
                }
                
                if (!email.address){
                    throw "O endereço de email é obrigatório.";
                }
                
                var isValidMail = crm.email.validateEmailAddress({"address": email.address}).success;;
                if (isValidMail){
                    if (email.mainContact) {
                        customer.emails.unshift({
                            type: email.type,
                            address: email.address
                        });
                    } else {
                        customer.emails.push({
                            type: email.type,
                            address: email.address
                        });
                    }
                } else {
                    throw "O enderço de email: "+email.address+" é inválido.";
                }
            });
        }
        
        if (_activity.phoneContact && _activity.phoneContact.length) {
            customer.phones = [];
            _activity.phoneContact.forEach(function(phone) {
                
                if (!phone.type){
                    throw "O tipo do telefone é obrigatório.";
                }
                
                if (!phone.number){
                    throw "O número do telefone é obrigatório.";
                }
                
                var isValidNumber;
                // se for celular
                if (phone.type._id == "59124071116a1f2ac0ac1e9b") {
                    isValidNumber = crm.phone.validatePhoneNumber({number: phone.number, isCellPhone: true}).success;
                } else {
                    isValidNumber = crm.phone.validatePhoneNumber({number: phone.number, isCellPhone: false}).success;
                }
        
                if (isValidNumber){
                    if (phone.mainContact) {
                        customer.phones.unshift({
                            type: phone.type,
                            country: phone.country,
                            number: phone.number
                        });
                    } else {
                        customer.phones.push({
                            type: phone.type,
                            country: phone.country,
                            number: phone.number
                        });
                    }
                } else {
                    throw "O número de telefone: "+phone.number+" é inválido.";
                }
            });
        }
        
        if (_activity.cnpj) {
            _activity.responsible = crm.contact._update(customer);
        }else {
            _activity.customer = crm.contact._update(customer);
        }
       
    }

    /////////////////////////
    
    updateContact();
	  
}

//////////////////////////////////////////////

function registerPhoneContactStart() {

	function fillOpenAttendanceHistory() {

		if (_activity.customer) {
		    
		    var doc;
		    if (_activity.cnpj) {
                doc = _activity.cnpj;
            } else {
                doc = _activity.cpf
            }
		    
			var query = {
				"query": {
					"bool": {
						"must": [{
								"nested": {
									"path": "customer.documents",
									"query": {
										"term": {
											"customer.documents.number.keyword": doc
										}
									}
								}

							}, {
								"term": {
									"_status.identifier.keyword": "open"
								}
							}
						],
						"must_not": {
							"term": {
								"_id": _object._id
							}
						}
					}
				},
				"sort": {
					"_creationDate": {
						"order": "desc"
					}
				}
			};
			
			

			function toAttendanceData(attendanceProcessInstance) {

				function getTab(attendanceProcessInstance) {
					if (attendanceProcessInstance.subSubReason) {
						return attendanceProcessInstance.subSubReason;
					}

					if (attendanceProcessInstance.subReason) {
						return attendanceProcessInstance.subReason;
					}

					if (attendanceProcessInstance.reason) {
						return attendanceProcessInstance.reason;
					}

					if (attendanceProcessInstance.prodServ) {
						return attendanceProcessInstance.prodServ;
					}

					return null;
				}

				function buildTab(tab) {

					if (!tab) {
						return '';
					}

					if (tab.parentTab) {
						return buildTab(tab.parentTab) + ' / ' + tab.name;
					}

					return tab.name;
				}

				function buildPhoneContactList(id) {
					var query = {
						"query": {
							"bool": {
								"must": [{
										"term": {
											"openAttendence._id": id
										}
									}, {
										"term": {
											"_status.identifier.keyword": "finished"
										}
									}
								],
								"must_not": {
									"term": {
										"_id": _object._id
									}
								}
							}
						},
						"sort": {
							"_creationDate": {
								"order": "desc"
							}
						},
						"size": 100
					};

					return _process.attendance.current._search(query).hits.hits.map(function (hit) {
						return toPhoneContact(hit._source);
					});
				}

				function toPhoneContact(phoneContact) {
					return {
						protocol: phoneContact,
						attendanceUser: phoneContact.attendanceUser ? phoneContact.attendanceUser : null,
						date: phoneContact._creationDate,
						observation: phoneContact.subject ? phoneContact.subject : null
					};
				}

				var tab = getTab(attendanceProcessInstance);
				var finalDate = attendanceProcessInstance.slaData && attendanceProcessInstance.slaData.length &&
					attendanceProcessInstance.slaData[0].finalDate ? attendanceProcessInstance.slaData[0].finalDate : null;

				return {
					processRef: attendanceProcessInstance,
					openDate: attendanceProcessInstance._creationDate,
					attendanceUser: attendanceProcessInstance.attendanceUser,
					tab: buildTab(tab),
					finalDate: finalDate,
					responsibleArea: attendanceProcessInstance.responsibleGroup ? attendanceProcessInstance.responsibleGroup : null,
					phoneContactList: buildPhoneContactList(attendanceProcessInstance._id)
				};
			}

			_activity.openAttendanceHistory = _process.attendance.current._search(query).hits.hits.map(function (hit) {
				return toAttendanceData(hit._source);
			});
		}
	}

	//////////////////////////////

	if (_activity.customer && _activity.customer.receitaFederal && _activity.customer.receitaFederal.rfSituation) {
		_activity.receitaFederalStatus = _activity.customer.receitaFederal.rfSituation;
	}

	fillOpenAttendanceHistory();
}

function registerPhoneContactOnState() {

	if (_activity.openAttendence && _activity.openAttendence.priority && _activity.openAttendence.priority === 'SOS') {
		_activity.SOSPriority = true;
	}
	if (_activity.openAttendence && _activity.openAttendence.description) {
		_activity.description = _activity.openAttendence.description;
	}
	if (_activity.openAttendence && !_activity.contactType) {
		_activity.contactType = 'telefonico';
	}

}

function registerPhoneContactEndCheck() {
	if (_activity.goToAttendanceTrigger || _activity.openAttendence) {
		return true;
	}
}

function registerPhoneContactSequence() {
	if (_activity.goToAttendanceTrigger) {
		_activity.openAttendence = null;
		_activity.subject = null;
		_activity.description = null;
		
		if (_activity.cnpj && !getInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON')) {
            return "CHOOSE_RESPONSIBLE";
        } else{
		    return "PERFORM_ATTENDANCE";
		}
		
	}
	if (_activity.openAttendence) {
		return finalizeActivity("REGISTER_PHONE_CONTACT");
	}
}

function registerPhoneContactEnd() {
	_activity.isSolved = "sim";
	_activity.attendanceUser = _user;
    var identifier;

	if (_activity.openAttendence) {
		_activity.prodServ = _activity.openAttendence.prodServ;
		if (_activity.contactType === 'telefonico' && _activity.prodServ) {
			identifier = _activity.openAttendence.prodServ.identifier + '.contatotelefonico';
			_activity.reason = crm.customerServiceTab.findByIdentifier({
					identifier: identifier
				});
		} else if (_activity.contactType === 'email' && _activity.prodServ) {
			identifier = _activity.openAttendence.prodServ.identifier + '.contatoporemail';
			_activity.reason = crm.customerServiceTab.findByIdentifier({
					identifier: identifier
				});
		}
	}

	_activity.attendanceTab = [{
			prodServ: _activity.prodServ,
			reason: _activity.reason,
			subReason: _activity.subReason,
			subSubReason: _activity.subSubReason
		}
	];
}

////////////////////////////////////////////////////
//PARSE_RESPONSIBLE
function parseResponsibleStart() {}

function parseResponsibleOnState() {
	function checkResponsibleCpf() {
			_activity.responsibleCPF = _activity.responsibleCPF.replace(/\D/g, '');
		}
	if (_activity.responsibleCPF) {
		checkResponsibleCpf();
	}
    
    
}

function parseResponsibleEndCheck() {
	function isValidCPF(cpf) {
		var v = crm.document.validateCPF({
				"cpf": cpf
			});
		if (!v || !v.success) {
			throw 'O CPF informado ' + _activity.responsibleCPF + ' não é válido.';
		}

		return v && v.success;
    }
    
	return (_activity.responsibleCPF && _activity.responsibleCPF.length >= 11 && isValidCPF(_activity.responsibleCPF));
}

function parseResponsibleSequence() {
    setInstanceControlProperty('SEEKING_RESPONSIBLE_PERSON', true);
	return "SEEKING_CLIENT";
}

function parseResponsibleEnd() {}




////////////////////////////////////////////////////
// CHOOSE_RESPONSIBLE



function chooseResponsibleStart(){}

function chooseResponsibleOnState(){
    
    if (_activity.createResponsibleTrigger) {
		setInstanceControlProperty('CONNECTION_TO_CREATE', true);
	}
	
	if (_activity.responsibleIdentification) {
        setInstanceControlProperty('CONNECTION_VALID', true);
        _activity.responsible = crm.contact._search({
            "query": {
                "match": {
                    "_id": _activity.responsibleIdentification
                }
            }
        }).hits.hits[0]._source;
    }
        
}

function chooseResponsibleEndCheck(){
    // retorna se há uma conexão a ser criada ou se já foi escolhida uma conexão
    return (getInstanceControlProperty('CONNECTION_TO_CREATE') || getInstanceControlProperty('CONNECTION_VALID'));
}

function chooseResponsibleSequence(){
    if (getInstanceControlProperty('CONNECTION_TO_CREATE')) {
        // cadastra um responsável / conexao
        return "PARSE_RESPONSIBLE";
    } else {
        // inicia atendimento com responsável iniciado na conexão
        return "POSITIVE_IDENTIFICATION";
    }
    
}

function chooseResponsibleEnd(){
    // verifica se não há repetição de email e telefone. Se tiver, limpa os repetidos
    var person = _activity.responsible;
    if(person){
        var i, j;
        if (person.phones && person.phones.length){
            for (i = 0; i<person.phones.length-1; i++ ){                
                for ( j = i+1; j<person.phones.length; j++){
                    if (person.phones[i].number && person.phones[j].number && person.phones[i].number === person.phones[j].number) {
                        person.phones[j].number = null;
                    }
                }
            }
            
            person.phones.forEach(function (phone,index){
                if (!phone.number) {
                    person.phones[index] = null;
                }
            });
        }
        if (person.emails && person.emails.length){
            for (i = 0; i<person.emails.length-1; i++ ){                
                for ( j = i+1; j<person.emails.length; j++){
                    if (person.emails[i].address && person.emails[j].address && person.emails[i].address === person.emails[j].address) {
                        person.emails[j].address = null;
                    }
                }
            }
            
            person.emails.forEach(function (email,index){
                if (!email.address) {
                    person.emails[index] = null;
                }
            });
        }
        
        if (_activity.responsible) {
           _activity.responsible = crm.contact._update(person);
        } 
    }
}



//////////////////////////////////////////////

function performAttendanceStart() {
	function initFunctions() {

		function slaExpirationDate() {
			if (!_activity.expiredSLA) {
				var SLA = _process.attendance.current.getSLA({
						_id: _activity._id
					}).SLA;
				if (SLA) {
					_activity.expiredSLA = new Date();
					_activity.expiredSLA.setHours(_activity.expiredSLA.getHours() + SLA);
				}
			}
		}

		function checkIfIsntBackOffice() {
			if (_activity.attendanceLevel.name === "N2") {
				var tabOn;
				if (_activity.reason) {
					tabOn = _activity.subSubReason ? _activity.subSubReason :
						_activity.subReason ? _activity.subReason : _activity.reason;
				}
				var isntBO = false;
				if (tabOn && tabOn.customerServiceData && tabOn.customerServiceData.length) {
					tabOn.customerServiceData.some(function (cSD) {
						if (cSD.level.name === "N2" && cSD.userGroups[0].name !== "Back Office") {
							isntBO = true;
							return true;
						}
					});
				}
				_activity.isntBackOfficeN2 = isntBO;
			}
		}

		function assignmentJustification() {
			if (_activity.attendanceLevel && _activity.attendanceLevel.name !== 'N1' && _activity.description && _activity.statusAction !== 'erro') {
				var levelName = _activity.attendanceLevel.name === 'N2' ? 'N1' :
					_activity.attendanceLevel.name === 'N3' ? 'N2' :
					_activity.attendanceLevel.name === 'N4' ? 'N3' : 'N';
				if (_activity.assignmentJustification === null) {
					_activity.assignmentJustification = levelName + ': ' + _activity.description;
				} else {
					_activity.assignmentJustification += ' \n\n' + levelName + ': ' + _activity.description;
				}
				_activity.description = null;
			}
		}

		function updateAttendanceTab() {
			var tabTypes = ["prodServ", "reason", "subReason", "subSubReason"];
			tabTypes.forEach(function (tabType) {
				if (_activity.attendanceTab && _activity.attendanceTab.length) {
					_activity.attendanceTab[0][tabType] = _activity[tabType];
				}
			});

			tabTypes.forEach(function (tabType) {
				if (_oldObject.attendanceTab && _oldObject.attendanceTab.length) {
					_oldObject.attendanceTab[0][tabType] = _oldObject[tabType];
				}
			});
		}

		//////////////////////////////////////////////////////////////////////////////////////

		//_activity.isSolved = _activity.backOfficeSolving ? "sim" : null;
		_activity.isSolved = null;
		_activity.labels = _activity.customer && _activity.customer.labels ? _activity.customer.labels : null;
		/*_activity.contract = _activity.reason && _activity.reason.identifier === "serasaantifraude.cancelamento" ?
			null : _activity.contract;*/

		var hasRFSituation = _activity.customer && _activity.customer.receitaFederal && _activity.customer.receitaFederal.rfSituation;
		_activity.receitaFederalStatus = hasRFSituation ? _activity.customer.receitaFederal.rfSituation : null;
        _activity.attendanceData = {};

		slaExpirationDate();       ///Calcula a data de expiração do SLA
		checkIfIsntBackOffice();   ///Checa se atendimento não é Back Office (para evitar não-Back Office editar em N2)
		assignmentJustification(); ///Move o que está escrito em Observações para Justificativa da atribuição
		updateAttendanceTab();     ///Atualizando a tabulação completa
	}

	function initializeAttendanceHistory() {
		/**
		 * Preenche o histórico de atendimento para o cliente.
		 * @author Juliano Ladeira
		 */

		if (_activity.customer) {
            var doc;
            if (_activity.cnpj) {
                doc = _activity.cnpj;
            } else {
                doc = _activity.cpf;
            }
			var query = {
				"query": {
					"bool": {
						"must": [{
								"nested": {
									"path": "customer.documents",
									"query": {
										"term": {
											"customer.documents.number.keyword": doc
										}
									}
								}
							}, {
								"term": {
									"_status.identifier.keyword": "finished"
								}
							}
						]
					}
				},
				"sort": {
					"_endDate": {
						"order": "desc"
					}
				},
				"size": 5
			};

			function toAttendanceData(attendanceProcessInstance) {

				function getTab(attendanceProcessInstance) {
					if (attendanceProcessInstance.subSubReason) {
						return attendanceProcessInstance.subSubReason;
					}

					if (attendanceProcessInstance.subReason) {
						return attendanceProcessInstance.subReason;
					}

					if (attendanceProcessInstance.reason) {
						return attendanceProcessInstance.reason;
					}

					if (attendanceProcessInstance.prodServ) {
						return attendanceProcessInstance.prodServ;
					}

					return null;
				}

				function buildTab(tab) {

					if (!tab) {
						return '';
					}

					if (tab.parentTab) {
						return buildTab(tab.parentTab) + ' / ' + tab.name;
					}

					return tab.name;
				}

				function buildPhoneContactList(id) {
					var query = {
						"query": {
							"bool": {
								"must": [{
										"term": {
											"openAttendence._id": id
										}
									}, {
										"term": {
											"_status.identifier.keyword": "finished"
										}
									}
								],
								"must_not": {
									"term": {
										"_id": _object._id
									}
								}
							}
						},
						"sort": {
							"_creationDate": {
								"order": "desc"
							}
						},
						"size": 100
					};

					return _process.attendance.current._search(query).hits.hits.map(function (hit) {
						return toPhoneContact(hit._source);
					});
				}

				function toPhoneContact(phoneContact) {
					return {
						protocol: phoneContact,
						attendanceUser: phoneContact.attendanceUser ? phoneContact.attendanceUser : null,
						date: phoneContact._creationDate,
						observation: phoneContact.subject ? phoneContact.subject : null
					};
				}

				var tab = getTab(attendanceProcessInstance);

				return {
					processRef: attendanceProcessInstance,
					openDate: attendanceProcessInstance._creationDate,
					tab: buildTab(tab),
					endDate: attendanceProcessInstance._endDate,
					attendanceUser: attendanceProcessInstance.attendanceUser,
					phoneContactList: buildPhoneContactList(attendanceProcessInstance._id)
				};
			}

			///////////////////////////////////////////////////////////
			_activity.attendanceHistory = _process.attendance.current._search(query).hits.hits.map(function (hit) {
				return toAttendanceData(hit._source);
			});
		}
	}

	function fillAFAndDigitalCertificateData() {
		
		function getRemittance(contractId) {
		    var targetInvoiceState = ["EMITTED", "PAID"];
			var query = {
				"query": {
					"bool": {
						"must": [{
								"nested": {
									"path": "invoiceItem",
									"query": {
										"term": {
											"invoiceItem.contract._id": contractId
										}
									}
								}
							}, {
								"term": {
									"paymentMethod.identifier.keyword": "BANK_BILLET"
								}
							}, {
								"terms": {
									"invoiceState.identifier.keyword": targetInvoiceState
								}
							}
						]
					}
				},
				"sort": {
					"_creationDate": {
						"order": "desc"
					}
				},
				"size": 1
			};
			var result = billing.invoice._search(query);
			if (result && result.hits && result.hits.total) {
				var invoice = result.hits.hits[0]._source;
				query = {
					"query": {
						"bool": {
							"must": [{
									"nested": {
										"path": "invoices",
										"query": {
											"term": {
												"invoices._id": invoice._id
											}
										}
									}
								}, {
									"term": {
										"paymentMethod.identifier.keyword": "BANK_BILLET"
									}
								}
							]
						}
					},
					"sort": {
						"_creationDate": {
							"order": "desc"
						}
					},
					"size": 1
				};
				result = billing.serasaSantanderRemittance._search(query);
				if (result && result.hits && result.hits.total) {
					return result.hits.hits[0]._source;
				}
			}
			return null;
		}
		
		
		/**
		 * Procura os dados do plano antifraude do cliente.
		 * @author Juliano Ladeira
		 */

		var cpf = _activity.cpf;
		var targetServiceStates = ["ACTIVE", "PENDING"];

		if (cpf) {

			var afContract = billing.contract.getContractOfContact({
					cpf: cpf,
					serviceStates: targetServiceStates
				}).contracts[0];

			_activity.antifraudeContract = afContract;
			
			if (afContract) {
			    _activity.afRemittance = getRemittance(afContract._id);
			}
			
		}

		/**
		 * Procura os dados do certificado digital do cliente.
		 * @author Danilo Silveira
		 */

		if (cpf) {
			var certificateDigitalResponse = serasa.eIDDigitalCertificate.findIssuedCertificateByCPF({
					cpf: cpf
				});
			if (certificateDigitalResponse.success) {
				_activity.digitalCertificate = {
					productName: certificateDigitalResponse.name,
					issuedDate: new Date(certificateDigitalResponse.updateDate)
				};
			}
		}
	}

	function fillTypeAndSubtype() {
		_activity.tabType = null;

		var tabOn = _activity.subSubReason ? _activity.subSubReason :
			_activity.subReason ? _activity.subReason :
			_activity.reason ? _activity.reason : null;

		if (tabOn) {
			var result = crm.tabType._search({
					"query": {
						"term": {
							"customerServiceTab._id": tabOn._id
						}
					}
				});

			if (result && result.hits && result.hits.total > 0) {
				var tabType = result.hits.hits[0]._source;
				if (tabType.types && _activity.responsibleGroup) {
					tabType.types.some(function (type) {
						if (type.group._id === _activity.responsibleGroup._id) {
							_activity.tabType = type;
							return true;
						}
					});
				}
			}
		}
	}

    function fillNegotiations() {
    	/**
    	 * Procura e seta as negociações do cliente
    	 * @author Danilo Silveira
    	 */
    
    	if (_activity.customer) {
    
    		function toAttendanceNegotiation(negotiation) {
    			return {
    				negotiation: negotiation,
    				partner: negotiation.partner,
    				productCategory: negotiation.product.productCategories,
    				status: negotiation.status
    			};
    		}
    
    		_activity.negotiations = [];
            
    		var eCredNegotiations = serasa.eCredNegotiation._search({
    				"query": {
    					"term": {
    						"customer._id": _activity.customer._id
    					}
    				}
    			});
    
    		if (eCredNegotiations && eCredNegotiations.hits && eCredNegotiations.hits.total > 0) {
    			_activity.negotiations = _activity.negotiations.concat(eCredNegotiations.hits.hits.map(
					function (eCred) {
					    return toAttendanceNegotiation(eCred._source);
				}));
    		}
    		
        
    		var lnoNegotiations = serasa.lnoNegotiation._search({
    				"query": {
    					"term": {
    						"customer._id": _activity.customer._id
    					}
    				}
    			});
    
    		if (lnoNegotiations && lnoNegotiations.hits && lnoNegotiations.hits.total > 0) {
    			_activity.negotiations = _activity.negotiations.concat(lnoNegotiations.hits.hits.map(
					function (LNO) {
				    	return toAttendanceNegotiation(LNO._source);
				}));
    		}
    	}
    }

    function setInitialConditionsForN1() {

        function serasaConsumidorTabulation() {
		    var user = JSON.parse(_activity.serasaConsumidorResponse);
		    if (!user || !user.status) {
		        return;
		    }
            prodServ = "serasaconsumidorsite";
			switch (user.status) {
    			case 'active':
    			    if (user.is_blocked) {
    			        reason = "serasaconsumidorsite.acesso";
        				subReason = "serasaconsumidorsite.acesso.dificuldadedeacesso";
        				subSubReason = "serasaconsumidorsite.acesso.dificuldadedeacesso.bloqueiode30minutos";
    			    } else {
    			        //Ativo
    			    }
    				break;
    			case 'needs_approved':
    				reason = "serasaconsumidorsite.acesso";
    				subReason = "serasaconsumidorsite.acesso.dificuldadedeacesso";
    				subSubReason = "serasaconsumidorsite.acesso.dificuldadedeacesso.erro48horas";
    				break;
    			case 'light_signup':
    			    //Cadastro Excluído
    				break;
				case 'pending':
				    //Cadastro pendente de confirmação
				    break;
				case 'revalidate_blocked':
					reason = "serasaconsumidorsite.acesso";
					subReason = "serasaconsumidorsite.acesso.dificuldadedeacesso";
					subSubReason = "serasaconsumidorsite.acesso.dificuldadedeacesso.desbloqueiorevalida";
					break;
			}            
        }
            
        function antifraudeContractTabulation () {
            var afContract = _activity.antifraudeContract;
            prodServ = "serasaantifraude";
            if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'SUSPENDED') {
                reason = "serasaantifraude.reativacao";
            } else if (afContract.paymentMethod && afContract.paymentMethod.identifier === 'BANK_BILLET') {
                var remittance = _activity.afRemittance;
                if (remittance) {
            	    if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'ACTIVE') {
            	        reason = "serasaantifraude.boleto";
            		} else if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'PENDING') {
            	        reason = "serasaantifraude.boleto";
            		}
                }
        	} else if (afContract.paymentMethod && afContract.paymentMethod.identifier === 'CREDIT_CARD') {
        	    if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'PENDING') {
        	        reason = "serasaantifraude.cartaodecredito";
        	        subReason = "serasaantifraude.cartaodecredito.reenviodolink";
        	    }
        	} 
        }
        
        function lnoECredTabulation() {
            var lnoNegotiationNumber = 0;
            var lnoObjects = [];
            var eCredNegotiationNumber = 0;
            var eCredObjects = [];
            _activity.negotiations.forEach(function (negotiation, index) {
                if (negotiation.productCategory && negotiation.productCategory.length) {
                    negotiation.productCategory.forEach(function (category) {
                        if (category._id === '5bbceb7fb3163958ca0a5338') {   /// LNO
                            lnoNegotiationNumber++;
                            lnoObjects.push(_activity.negotiations[index]);
                        } else if (category._id === '5bbceb6db3163958ca0a52dc') {   /// eCred
                            eCredNegotiationNumber++;
                            eCredObjects.push(_activity.negotiations[index]);
                        }
                    });
                }
            });
            
            if (lnoNegotiationNumber > 0) {
                prodServ = "serasalimpanome";
                _activity.negotiations = lnoObjects;
            } else if (eCredNegotiationNumber > 0) {
                prodServ = "serasaecred";
                _activity.negotiations = eCredObjects;
            }
        }
        
        /////////////
        
        if(_activity && _activity.attendanceLevel && _activity.attendanceLevel.name === "N1"){
            _activity.attendanceGroup = serasa.attendanceGroup.getByID({"id": _user._id});
        }
        
        
		if (_activity.attendanceLevel.name !== "N1" || _activity.statusAction === 'erro') {
			return;
		}

		// Grupo responsável "Call Center"
		var callCenterId = "59dfebe6ce0272416c46fe79";
		_activity.responsibleGroup = _system._userGroup._get({
			_id: callCenterId
		});

		// Origem e Tipo de atendimento padrão
		if (!_activity.attendanceClassification) {
			_activity.attendanceClassification = [{
				source: {_id: "5b76d967b316392fd862eb44"},   /// Call Center
				type: null
			}];
		}

		// Setar tabulação"
		if (_activity.prodServ === null) {
			var prodServ;
			var reason;
			var subReason;
			var subSubReason;
            
            
			if (_activity.customer && _activity.personFoundinSerasaConsumidor) {
			    serasaConsumidorTabulation();
			} else if (_activity.antifraudeContract) {
			    antifraudeContractTabulation();
			} else if (_activity.negotiations && _activity.negotiations.length) {
                lnoECredTabulation();
            } else if (_activity.cnpj) {
                var prod;
                prod = crm.customerServiceTab.findByIdentifier({"identifier": "cadastropositivo"});
                _activity.prodServ = prod;
            }

			var tabulationTypes = ["prodServ", "reason", "subReason", "subSubReason"];
			tabulationTypes.forEach(function (type) {
				if (eval(type)) {
					_activity[type] = crm.customerServiceTab.findByIdentifier({
							identifier: eval(type)
						});
					_oldObject[type] = _activity[type];
				}
			});

			_activity.attendanceTab = [{
					prodServ: _activity.prodServ,
					reason: _activity.reason,
					subReason: _activity.subReason,
					subSubReason: _activity.subSubReason
				}
			];

		}
	}

	initFunctions();
	initializeAttendanceHistory();
	fillAFAndDigitalCertificateData();
	fillTypeAndSubtype();
    fillNegotiations();                    ///Necessário para saber se cliente possui negociações
	setInitialConditionsForN1();        ///Condições iniciais de N1

}

function performAttendanceOnState() {
	function updateDraftDetails() {
	    //retentionActivityMessage();         /// Mensagem para atividade de retenção/cancelamento
		//saleProcessMessage();               /// Mensagem para processo de vendas
		seeAFHistoryMessage();              /// Mensagem para ação de histórico SAF
		setAreaField();                     /// Seta o campo Ilha
		serasaConsumidorActionValidation(); /// Faz a validação se o cliente possui dados no Serasa Consumidor para executar uma ação desta área
		removeUserGroupSelection();         /// Retirar a seleção passada de grupo quando atendenter tiver que escolher
		removeInconsistencySelection();     /// Desmarcar as opções de inconsistência
		removeTabulationIfMissAttendance(); /// Seta null nas tabulações se for engano/ligação caiu (evitar aparecer campos desnecessários)
		fillSerasaConsumidorPhoneChange();  /// Seta valores do campo para a escolha do novo número de celular
        antifraudeChangePlan();
		////////////////////////////////

// 		function retentionActivityMessage() {
// 			if (_activity.reason) {
// 				var reasonChanged = _oldObject.reason ? _activity.reason.identifier !== _oldObject.reason.identifier : true;
// 				if (_activity.reason && _activity.reason.identifier === "serasaantifraude.cancelamento" && reasonChanged) {
// 					_utils.addInfoMessage("Após selecionar o contrato o atendimento seguirá para retenção/cancelamento.");
// 				}
// 			}
// 		}

// 		function saleProcessMessage() {
// 			if (_activity.prodServ) {
// 				var prodServChanged = _oldObject.prodServ ? _activity.prodServ.identifier !== _oldObject.prodServ.identifier : true;
// 				if (_activity.prodServ && _activity.prodServ.identifier === "serasaantifraude" && prodServChanged) {
// 					_utils.addInfoMessage("Após selecionar 'Oferta', o atendimento seguirá para a atividade de Vendas.");
// 				}
// 			}
// 		}

        function antifraudeChangePlan() {
            if (_activity.contract && 
                _activity.availablePlans && 
                _activity.customerServiceAction && 
                _activity.customerServiceAction.identifier === "changePlan") {
                    var balance = null;
                    try {
                        balance = billing.contract.newPlanBalanceCalculation({
                            "contract": _activity.contract,
                            "product": _activity.availablePlans
                        });
                    } catch (e) {
                        balance = null;
                    }

                    if (balance == null){
                        _utils.addWarningMessage("Contrato selecionado não pode alterar de plarno: período faturado não estabelecido.");
                        _activity.contract = null;
                        _activity.availablePlans = null;
                    }
            }
        }

		function seeAFHistoryMessage() {
			if (_activity.showAntifraudeHistory === 'show' && !_activity.contract && !(_activity.antifraudeEmailHistory || _activity.antifraudeSMSHistory)) {
				_activity.showAntifraudeHistory = null;
				_utils.addInfoMessage("Para visualizar o histórico AntiFraude favor selecionar o contrato.");
			}
		}

		function serasaConsumidorActionValidation() {
			var serasaConsumidorActions = ["serasaConsumidorEditCellphone", "serasaConsumidorEditEmail", "serasaConsumidorDeleteUser",
				"serasaConsumidorUnlock30Minutes", "serasaConsumidorUnlock48Hours", "serasaConsumidorUnlockRevalidate"];
			if (_activity.customerServiceAction && (serasaConsumidorActions.indexOf(_activity.customerServiceAction.identifier) > 0) &&
				!_activity.serasaConsumidorResponse) {
				throw "Ação não pode ser selecionada pois cliente não foi encontrado no Serasa Consumidor.";
			}
		}

		function setAreaField() {
			var tabOn = null;
			if (_activity.reason) {
				tabOn = _activity.subSubReason ? _activity.subSubReason :
					_activity.subReason ? _activity.subReason : _activity.reason;
			}

			if (tabOn && tabOn.customerServiceData && tabOn.customerServiceData.length) {
				tabOn.customerServiceData.some(function (cSD) {
					if (cSD.level.name === "N1" && cSD.ilhaSkill) {
						_activity.area = cSD.ilhaSkill;
						return true;
					}
				});
			}
		}

		function removeUserGroupSelection() {
			if ((_activity.isSolved === "encaminhar" || _activity.isSolved === "retornar") && _activity.responsibleGroup) {
				var tabOn = _activity.subSubReason ? _activity.subSubReason :
					_activity.subReason ? _activity.subReason :
					_activity.reason ? _activity.reason : null;
				if (tabOn.customerServiceData) {
					tabOn.customerServiceData.some(function (csd) {
					    var nextLevel;
                        if (_activity.isSolved === "retornar") {
                            nextLevel = _object.previousAttendanceLevel;
                        } else {
                            nextLevel = _activity.attendanceLevel.nextLevel;
                        }
						if (nextLevel.name === csd.level.name && (csd.userGroups.length > 1)) {
							var notInList = csd.userGroups.every(function (ug) {
									return ug._id !== _activity.responsibleGroup._id;
								});
							if (notInList) {
								_activity.responsibleGroup = null;
							}
							return true;
						}
					});
				}
			}
		}

		function removeInconsistencySelection() {
			if (!_activity.inconsistentAttendance) {
				if (_activity.inconsistencyTable && _activity.inconsistencyTable.length) {
					_activity.inconsistencyTable[0].attendanceInconsistency = null;
					_activity.inconsistencyTable[0].inconsistencyReason = null;
				}
			}
		}

		function removeTabulationIfMissAttendance() {
			var attendanceTypes = ["ligacaoPerdida", "engano"];

			var attClass = _activity.attendanceClassification;
			if (attClass && attClass.length && attClass[0].type && attendanceTypes.indexOf(attClass[0].type) >= 0) {
				_activity.prodServ = null;
				_activity.reason = null;
				_activity.subReason = null;
				_activity.subSubReason = null;
				_activity.customerServiceAction = null;
			}
		}
		
        function fillSerasaConsumidorPhoneChange() {
            if (_activity.customerServiceAction && _activity.customerServiceAction.identifier === "serasaConsumidorEditCellphone") {
                if (!_activity.phoneAntifraude || !_activity.phoneAntifraude.type) {
                    _activity.phoneAntifraude = {
                        type: {_id: "59124071116a1f2ac0ac1e9b"}     //Celular
                    };
                }
            }
        }		

	}
	
	function checkForFieldChanges() {
		// Ao mudar uma opção pai, limpa os demais campos dependentes
		if (hasChange(_activity.prodServ, _oldObject.prodServ)) {
			_activity.reason = null;
			_activity.subReason = null;
			_activity.subSubReason = null;
			resetValues();
		} else if (hasChange(_activity.reason, _oldObject.reason)) {
			_activity.subReason = null;
			_activity.subSubReason = null;
			resetValues();
		} else if (hasChange(_activity.subReason, _oldObject.subReason)) {
			_activity.subSubReason = null;
			resetValues();
		} else if (hasChange(_activity.subSubReason, _oldObject.subSubReason)) {
			resetValues();
		}
		if (hasChange(_activity.customerServiceAction, _oldObject.customerServiceAction)) {
			_activity.contract = null;
			_activity.order = null;
			_activity.paymentMethod = null;
			_activity.invoices = null;
			_activity.emailAddresses = null;
			_activity.availablePlans = null;
			_activity.emailAntifraude = null;
			_activity.phoneAntifraude = null;
			_activity.dependentData = null;
			_activity.dependentContract = null;
			_activity.dependent = null;
			_activity.newValue = null;
            _activity.report = null;
            _activity.extracts = null;
		}

		/////////////////////////////

		function resetValues() {
			_activity.customerServiceAction = null;
			_activity.area = null;
			_activity.isSolved = null;
		}

	}

	function treatVcpeAfResendInviteEmail() {

		function validatePhone(number) {
			return crm.phone.validatePhoneNumber({
				number: number,
				isCellPhone: true
			}).success;
		}
		
		////////////////////

		if (_activity.customerServiceAction && _activity.customerServiceAction.identifier) {
			if (_activity.customerServiceAction.identifier === "resendVCPEMail") {
				if (hasChange(_activity.order, _oldObject.order)) {
					if (_activity.order.orderItems[0].provisioningParameters.email) {
						_activity.emailAddresses = [_activity.order.orderItems[0].provisioningParameters.email];
					} else {
						_activity.emailAddresses = [];
					}
				}
			} else if (_activity.customerServiceAction.identifier === "changeEmailAndPhoneAntiFraude") {
				if (_activity.contract && _activity.contract.productData[0] && _activity.contract.productData[0].provisioningParameters) {

					var provisioningParameters = _activity.contract.productData[0].provisioningParameters;
					if (_activity.subSubReason && _activity.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.emaildoservico") {
						if (!_activity.emailAntifraude && provisioningParameters.email) {
							_activity.emailAntifraude = provisioningParameters.email;
						}

					} else if (_activity.subSubReason && _activity.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.telefonedoservico") {
						if (!_activity.phoneAntifraude && provisioningParameters.phone) {
							var phone_aux = null;
							if (validatePhone(provisioningParameters.phone.number)) {
								phone_aux = provisioningParameters.phone.number;
							}
							var phone = {
								number: phone_aux,
								type: crm.phoneType._get({
									"_id": "59124071116a1f2ac0ac1e9b"
								})
							};
							_activity.phoneAntifraude = phone;
						}
					}
				}
			} else if (_activity.customerServiceAction.identifier === "resendDependentInvite") {
			    var dependentChange = hasChange(_activity.dependent, _oldObject.dependent);
			    if (dependentChange && _activity.dependent.emails && _activity.dependent.emails.length) {
			        _activity.newValue = _activity.dependent.emails[0].address;
			    }
			}
		}

	}

    function fillNegotiations() {
    	/**
    	 * Procura e seta as negociações do cliente
    	 * @author Danilo Silveira
    	 */
    
        var eCredOrLNO = _activity.prodServ && (_activity.prodServ.identifier === 'serasalimpanome' || _activity.prodServ.identifier === 'serasaecred');
        var prodServChanged = hasChange(_activity.prodServ, _oldObject.prodServ);
    	if (_activity.customer && _activity.attendanceLevel.name === 'N1'&& eCredOrLNO && prodServChanged) {
    
    		function toAttendanceNegotiation(negotiation) {
    			return {
    				negotiation: negotiation,
    				partner: negotiation.partner,
    				productCategory: negotiation.product.productCategories,
    				status: negotiation.status
    			};
    		}
    
    		_activity.negotiations = [];
            
            if (_activity.prodServ.identifier === 'serasaecred' || _activity.attendanceLevel.name !== 'N1') {
        		var eCredNegotiations = serasa.eCredNegotiation._search({
        				"query": {
        					"term": {
        						"customer._id": _activity.customer._id
        					}
        				}
        			});
        
        		if (eCredNegotiations && eCredNegotiations.hits && eCredNegotiations.hits.total > 0) {
        			_activity.negotiations = _activity.negotiations.concat(eCredNegotiations.hits.hits.map(
    					function (eCred) {
    					    return toAttendanceNegotiation(eCred._source);
    				}));
        		}
        		
            } 
            
            if (_activity.prodServ.identifier === 'serasalimpanome' || _activity.attendanceLevel.name !== 'N1') {
        		var lnoNegotiations = serasa.lnoNegotiation._search({
        				"query": {
        					"term": {
        						"customer._id": _activity.customer._id
        					}
        				}
        			});
        
        		if (lnoNegotiations && lnoNegotiations.hits && lnoNegotiations.hits.total > 0) {
        			_activity.negotiations = _activity.negotiations.concat(lnoNegotiations.hits.hits.map(
    					function (LNO) {
    				    	return toAttendanceNegotiation(LNO._source);
    				}));
        		}
            }
    	}
    }

	function initializeEmailHistory() {
		if (_activity.customer && _activity.showEmailHistory === "show" && !_activity.emailHistory) {
			var servicesConfigs = serasa.servicesConfigs._get({"_id": "59dd445ace02720a06cde961"});
			var campaignMonitorWSConfigs = servicesConfigs.wsConfigs.find(function (wsConfig) {
					return wsConfig.configIdentifier === 'CAMPAIGN_MONITOR';
				});

			_activity.emailHistory = [];
			if (_activity.customer.emails && _activity.customer.emails.length > 0) {
				_activity.customer.emails.forEach(function (email) {
					if (email.address === "serasacorreios@br.experian.com") {
						return;
					}
					var count = 20;
					var target = campaignMonitorWSConfigs.wsURL + "messages?clientID=abab776b92cf57e031845278b55173d8&query=" +
						email.address + "&count=" + count;
					var user = campaignMonitorWSConfigs.user;
					var password = campaignMonitorWSConfigs.password;
					try {
						var webTarget = _utils.connector.web(target, null, null).setBasicAuth(user, password);
						var wsResponse = webTarget.get();
						_activity.emailHistory = _activity.emailHistory.concat(wsResponse.map(function (message) {
									return toEmailHistory(message);
								}));
					} catch (err) {
						_utils.log("Erro na consulta do histórico de emails do cliente.");
						_utils.log("ERROR: " + JSON.stringify(err));
					}
				});
			}
			if (!_activity.emailHistory.length) {
				_activity.noInfoEmailHistory = "Nenhuma informação disponível";
			}
		}

		////////////////////////////
		function toEmailHistory(email) {
			var status = email.Status ? email.Status : "";
			var openingCount = email.TotalOpens ? email.TotalOpens : 0;
			openingCount = openingCount > 1 ? openingCount + " aberturas" :
				openingCount === 1 ? openingCount + " abertura" : "Nenhuma abertura";

			var statusOpening = status + ' - ' + openingCount;
			return {
				messageID: email.MessageID,
				recipient: email.Recipient,
				sendAt: new Date(email.SentAt),
				status: statusOpening,
				subject: email.Subject,
				openingCount: email.TotalOpens
			};
		}
	}

	function initialiazeZendeskHistory() {
		if (_activity.customer && _activity.showZendeskHistory === 'show' && !_activity.zendeskHistory) {
			var servicesConfigs = serasa.servicesConfigs._get({"_id": "59dd445ace02720a06cde961"});
			var zendeskConsumidorConfigs = servicesConfigs.wsConfigs.find(
					function (wsConfig) {
					return wsConfig.configIdentifier === 'ZENDESK_SERASA_CONSUMIDOR';
				});
			var zendeskExperianConfigs = servicesConfigs.wsConfigs.find(
					function (wsConfig) {
					return wsConfig.configIdentifier === 'ZENDESK_SERASA_EXPERIAN';
				});

			var termsToBeConsulted = [];
			termsToBeConsulted.push(_activity.cpf);

			_activity.zendeskHistory = [];
			termsToBeConsulted.forEach(
				function (term) {
				getZendeskHistory(term, zendeskConsumidorConfigs);
				getZendeskHistory(term, zendeskExperianConfigs);
			});

			if (_activity.zendeskHistory.length) {
				_activity.zendeskHistory.sort(
					function (a, b) {
					return a.date.getTime() - b.date.getTime();
				});
			} else {
				_activity.noInfoZendeskHistory = "Nenhuma informação disponível";
			}
		}

		////////////////////////////
		function getZendeskHistory(term, wsConfigs) {
			var target = (wsConfigs.wsURL + "search.json?query=type:ticket " + term + "&sort_by=created_at").replace(/ /g, "+");
			var user = wsConfigs.user;
			var password = wsConfigs.password;
			try {
				var webTarget = _utils.connector.web(target, null, null).setBasicAuth(user, password);
				var wsResponse = webTarget.get();
				_activity.zendeskHistory = _activity.zendeskHistory.concat(wsResponse.results.map(
							function (attendance) {
							return toZendeskHistory(attendance, wsConfigs.configIdentifier);
						}));
			} catch (err) {
				_utils.log("Erro na consulta do histórico Zendesk.");
				_utils.log("ERROR: " + JSON.stringify(err));
			}
		}

		function toZendeskHistory(attendance, identifier) {
			var channel = attendance.via && attendance.via.channel ? attendance.via.channel : "";
			var status = attendance.status ? attendance.status : "";
			var channelStatus = channel + ' - ' + status;
			return {
				messageID: attendance.id,
				uRLInstance: identifier,
				channel: channelStatus,
				date: new Date(attendance.created_at),
				type: attendance.type,
				status: attendance.status,
				subject: attendance.subject,
				description: attendance.description,
				comments: null
			};
		}
	}

	function initializeSmsHistoryAndEmail() {
		if (_activity.showAntifraudeHistory === 'show' && !(_activity.antifraudeEmailHistory || _activity.antifraudeSMSHistory)) {
			var firstTime = _activity.antifraudeEmailHistory === null && _activity.antifraudeSMSHistory === null;
			if (firstTime && _activity.contract) {
				var methodStr = "_process.attendance.current.seeNotificationsHistory";
				var method = eval(methodStr);
				var result = method({
						_id: _object._id,
						code: _activity.contract.code
					});
				if (result) {
					_activity.antifraudeEmailHistory = result.antifraudeEmailHistory;
					_activity.antifraudeSMSHistory = result.antifraudeSMSHistory;
				}
			}
		}
	}

	function initializeContactData() {
		
		var tableIsFilled = _activity.clientContactDataTable && _activity.clientContactDataTable.length && _activity.clientContactDataTable[0];
	    if (_activity.customer && !tableIsFilled) {
			_activity.clientContactDataTable = [{
			    clientContactData: {
    				emails: _activity.customer.emails,
    				phones: _activity.customer.phones,
    				addresses: _activity.customer.addresses
			    }
			}];
		}

		/**
		 * Preenche os dados do cadastro do Serasa Consumidor do cliente.
		 * @author Matheus Marzano
		 */
		if (_activity.personFoundinSerasaConsumidor && _activity.showSerasaConsumidorClientData === 'show' && !_activity.serasaConsumidorClientData && _activity.customer.serasaConsumidorRegistration) {
			_activity.serasaConsumidorClientData = {
				"status": _activity.customer.serasaConsumidorRegistration.status,
				"email": _activity.customer.serasaConsumidorRegistration.email,
				"registerDate": _activity.customer.serasaConsumidorRegistration.registerDate,
				"socialNetwork": _activity.customer.serasaConsumidorRegistration.socialNetwork,
				"lastAccessDate": _activity.customer.serasaConsumidorRegistration.lastAccessDate
			};
		}
	}

	function verifyAnonymousContactActivity() {
		if (_activity.anonymousClient && _activity.reason) {
			var tabOn = _activity.subSubReason ? _activity.subSubReason :
				_activity.subReason ? _activity.subReason : _activity.reason;
			var tabsAnonymousCantProceed = ["serasaantifraude.cancelamento", "serasaantifraude.oferta.venda"];
			if (tabsAnonymousCantProceed.indexOf(tabOn.identifier) !== -1) {
				throw "Contato anônimo não pode prosseguir este fluxo de atendimento.";
			}
		}
	}

	function resetShowFields() {
		var fields = [
			"showSerasaConsumidorClientData",
			"showNegotiations",
			"showEmailHistory",
			"showZendeskHistory",
			"showAntifraudeHistory"
		];

		fields.forEach(function (field) {
			if (_activity[field] === "") {
				_activity[field] = null;
			}
		});
	}
	
	function treatGenerateReport(){

        if(!_activity.report){
            _activity.report = [];
            _activity.extracts = [];
            _activity.attendanceData.fontsReport = null;
            _activity.attendanceData.historyReport = null;
            _activity.attendanceData.consulentsReport = null;
            _activity.attendanceData.positiveInformationsReport = null;
            _activity.positiveInformations = null;
            _activity.showPositiveInformations = null;
		}
		if (!_activity.protocols)
			_activity.protocols = [];

	   if (_activity.customerServiceAction && _activity.customerServiceAction.identifier === "cadastroPositivoGenerateReport" && _activity.newValue){
	        try{
	            var htmlFile = null;
	            var file = null;
	            var response;
                var template;
                var body = {};
                
                if (_activity.customer.emails && _activity.customer.emails.length && _activity.customer.emails[0] && _activity.customer.emails[0].address) {
                    body.email = _activity.customer.emails[0].address;
                }
                
                if (_activity.cpf) {
                    body.cpf = _activity.cpf;
                } else if (_activity.cnpj) {
                    body.cnpj = _activity.cnpj;
				}
				var nome = _activity.customer.name;
				
				if (!(_activity.attendanceData.protocols && _activity.attendanceData.protocols.length && _activity.attendanceData.protocols.length != 0))
					_activity.attendanceData.protocols = [];

				function padNumber(number){
					return ("00" + number).slice(-2);
				}
				
				function formatDateFilipeta(date){
					return padNumber(date.getDay()) + '/' + padNumber(date.getMonth()) + '/' + date.getFullYear();
				}
				
				function formatHourFilipeta(date){
					return padNumber(date.getHours()) + ':' + padNumber(date.getMinutes());
				}

				function maskCPF(cpf){
					cpf = cpf.toString();
					if (!(cpf && cpf.length && cpf.length == 11))
						throw 'CPF com menos que 11 digitos';
					return cpf.slice(0, 3) + '.' + cpf.slice(3, 6) + '.' + cpf.slice(6, 9) + '-' + cpf.slice(9,12)
				}

                function generateProtocol(type){
					var date = new Date(_activity._lastUpdateDate);
					var mapper = {
						'fontes': {
							operation_type: '22 - Consulta por Escrito Positivo',
							protocol_type: 'Fontes',
							file_name: 'Protocolo de consulta - Fontes.pdf'
						},
						'consulentes': {
							operation_type: '22 - Consulta por Escrito Positivo',
							protocol_type: 'Consulentes',
							file_name: 'Protocolo de consulta - Consulentes.pdf'
						},
						'historico': {
							operation_type: '22 - Historico de Adesao/Cancelamento ',
							protocol_type: '',
							file_name: 'Protocolo de consulta - Histórico de adesão.pdf'
						}
					}
					var protocol_template = serasa.cadastroPositivoTemplates.findByName({
						"name": "Protocolo"
					}).template;
					
					protocol_template = protocol_template.replace('{hora}', formatHourFilipeta(date));

					// PENDENTE
					protocol_template = protocol_template.replace('{numero_agencia}', '');
					protocol_template = protocol_template.replace('{nome_agencia}', '');
					// ########

					protocol_template = protocol_template.replace('{operation_type}', mapper[type].operation_type);
					protocol_template = protocol_template.replace('{protocol_type}', mapper[type].protocol_type);
					protocol_template = protocol_template.replace('{nome_consumidor}', nome.toUpperCase());
					
					if (body.cpf) {
						protocol_template = protocol_template.replace('{tipo_documento}', 'CPF');
						protocol_template = protocol_template.replace('{documento}', maskCPF(body.cpf));
					} else if (body.cnpj) {
						protocol_template = protocol_template.replace('{tipo_documento}', 'CNPJ');
						protocol_template = protocol_template.replace('{documento}', body.cnpj);
					}
					protocol_template = protocol_template.replace('{data}', formatDateFilipeta(date));
					protocol_template = protocol_template.replace('{protocolo}', _activity.protocol);
					
					var htmlFile = _util._fileHelper.create({
						content: protocol_template,
						targerContentType:  "text/html",
						targetFileName: "Relatorio Fontes.html",
						targetCharset: "UTF-8"
					});
				
					var file = _util._fileHelper.convert({
						file: htmlFile,
						targerContentType: "application/pdf",
						targetFileName: mapper[type].file_name,
						targetCharset: "UTF-8"
					});
					return file;
				}

                switch (_activity.newValue){
                    
                    /////RELATÓRIO DE FONTES//////
                    case 'Fontes':
                        
						if (_activity.attendanceData && !_activity.attendanceData.fontsReport){
							response = serasa.cadastroPositivoIntegration.getFontsInformation(body).response;
							
							if(response.length && response.length > 0){
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Fontes"
								}).template;
								
								let date = new Date(_activity._lastUpdateDate)
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
								var html = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Fontes"
								}).reportDivs;
								html = (JSON.parse(html)).font;
								
								
								var fonts = '';
								response.forEach((font) => {
									var f = html;
									
									f = f.replace('#EMPRESA#', font.nomeCompletoEmpresa);
									f = f.replace('#CNPJ#', font.numeroCnpjParticipante);
									
									var adress = font.nomeLogradouroEmpresa + ', ' + font.complementoEnderecoEmpresa + 
									', ' + font.nomeBairroEmpresa + ', ' + font.nomeMunicipioEmpresa + 
									', '+ font.estadoEmpresa + ', CEP: ' + font.cepEmpresa;
									
									f = f.replace('#ENDEREÇO#', adress);
									
									var phones = getFormatedPhones(font);
									f = f.replace('#TELEFONES#', phones);
									
									f = f.replace('#SITE#', font.siteEmpresa);
									
									fonts += f;
								}); 
								template = template.replace('#FONTS#', fonts);
								
							}else if(response.length === 0){
								
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Fontes Vazias"
								}).template;
								
								let date = new Date(_activity._lastUpdateDate)
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
							}
							
							htmlFile = _util._fileHelper.create({
									content: template,
									targerContentType:  "text/html",
									targetFileName: "Relatorio Fontes.html",
									targetCharset: "UTF-8"
								});
							
							
							file = _util._fileHelper.convert({
									file: htmlFile,
									targerContentType:  "application/pdf",
									targetFileName: "Relatorio Fontes.pdf",
									targetCharset: "UTF-8"
								});
								
							_activity.report.push(file);
							_activity.attendanceData.fontsReport = file;
							
							var protocol_file = generateProtocol('fontes');
							_activity.attendanceData.protocols.push(protocol_file);
							_activity.protocols.push(protocol_file);
						}
                     
                        break;
                        
                    /////RELATÓRIO DE INFORMAÇÕES POSITIVAS//////    
                    case 'Informações Positivas':
                        if (_activity.attendanceData && !_activity.attendanceData.positiveInformationsReport){
                            _activity.positiveInformations = [];
                            _activity.positiveInformations[0] = {};
                            _activity.positiveInformations[0].operations = [];
                            _activity.positiveInformations[0].protocol = [];
                            response = serasa.cadastroPositivoIntegration.getPositiveInformations(body).response;
                            _activity.informationPositiveResponse = _utils.stringifyAsJson(response);
                            
                            template = serasa.cadastroPositivoTemplates.findByName({
                                    "name": "Relatório de Informações Positivas"
                                }).template;
                            
                            let extractTemplate = serasa.cadastroPositivoTemplates.findByName({
								"name": "Extrato Informações Positivas"
							}).template;
                            
                            let cardExtractTemplateAcc = "", utilExtractTemplateAcc = "", parcExtractTemplateAcc = "";
                            let consExtractTemplateAcc = "", rotativoExtractTemplateAcc = "";
                            
                            function padStart(numChars, textoEntrada){
                                if(textoEntrada == undefined){
                                    textoEntrada = " ";
                                }
                                let stringOriginal = "" + textoEntrada;
                                let stringRetorno = "";
                                let tam = 47 - numChars - stringOriginal.length;
                                if (tam > 0){
                                    for(let i = 0; i < tam; i++){
                                        stringRetorno += " ";   
                                    }
                                }
                                return stringRetorno + stringOriginal;
                            }
                            
                            function tratarParcelas(parcelas){
                                
                                let hoje = new Date();
                                let retornoParcelas = {}, tam = 12, saldoTotal = 0;
                                let parcelasNaoVencidas = 0, parcelasPgAtrasadas = 0, parcelasPgEmDia = 0, parcelasPgUltMeses = 0, valorProxParcela = null;
                                
                                parcelas.forEach(p => {
                                    
                                    // soma saldo total de parcelas
                                    saldoTotal += p.valorDaParcela;
                                    
                                    // qntd de parcelas a vencer
                                    let dtVenc = new Date(p.dataDoVencimento);
                                    dtVenc.setDate(dtVenc.getDate() + 1);
                                    
                                    let dist = 10e+1000;
                                    if(dtVenc > hoje){
                                        parcelasNaoVencidas++;
                                        if(dtVenc - hoje < dist){
                                            dist = dtVenc - hoje;
                                            valorProxParcela = p.valorDaParcela;
                                        }
                                    } 
                                    
                                    // qntd de parcelas pagas em atraso
                                    let dtPagamento = new Date(p.dataDoPagamento);
                                    dtPagamento.setDate(dtPagamento.getDate() + 1);
                                    
                                    if(dtPagamento > dtVenc){
                                        parcelasPgAtrasadas++;
                                    } else {
                                        parcelasPgEmDia++;
                                    }
                                    
                                    // qntd de parcelas pagas nos ultimos 12 meses ou meses apresentados
                                    
                                    if(parcelas.length < 12){
                                        tam = parcelas.length;
                                    }
                                    if(p.dataDoPagamento != null){
                                        parcelasPgUltMeses++;
                                    }
                                    
                                });
                                
                                retornoParcelas.parcelasNaoVencidas = parcelasNaoVencidas;
                                retornoParcelas.parcelasPgAtrasadas = parcelasPgAtrasadas;
                                retornoParcelas.parcelasPgEmDia = parcelasPgEmDia;
                                retornoParcelas.parcelasPgUltMeses = parcelasPgUltMeses;
                                retornoParcelas.valorProxParcela = valorProxParcela;
                                retornoParcelas.saldoTotal = saldoTotal;
                                
                                return retornoParcelas;
                                
                            }
                            
                            function tratarData(dataEntrada){
                                
                                try{
                                    let data = dataEntrada;
                                    data = data.split("-");
                                    let dataSaida = data[2] + "/" + data[1] + "/" + data[0];
                                    return dataSaida;    
                                } catch (e){
                                    return " ";
                                }
                            }
                            
                            if(response){
                                
                                let date = new Date(response.protocolo.dataGeracao);
                                template = template.replace('#DATA#', getFormatedDate(date));
                                template = template.replace('#NOME#', nome);
                                template = template.replace('#PROTOCOLO#', response.protocolo.numeroProtocolo);
                                
                                _activity.positiveInformations[0].protocol[0] = {};
                                _activity.positiveInformations[0].protocol[0].date = getFormatedDate(date);
                                _activity.positiveInformations[0].protocol[0].protocolNumber = response.protocolo.numeroProtocolo.toString();
                                
                                if (body.cpf) {
                                     template = template.replace('#TIPODOCUMENTO#', "CPF");
                                     template = template.replace('#DOCUMENTO#', body.cpf);
                                 } else if (body.cnpj) {
                                     template = template.replace('#TIPODOCUMENTO#', "CNPJ");
                                     template = template.replace('#DOCUMENTO#', body.cnpj);
                                }
                                
                                var divs = serasa.cadastroPositivoTemplates.findByName({
                                    "name": "Relatório de Informações Positivas"
                                }).reportDivs;
                                divs = JSON.parse(divs);
                                
                                var cards = '';
                                var paymentTemplate = '';
                                var categoryTemplate = ''
                                response.operacoes.forEach( (op) => {
                                    var phones = "";
                                    var listaVencimento = '';
                                    var listaValor = '';
                                    var listaDataPagamento = '';
                                    var listaValorPagamento = '';
                                    var contractStart = '';
                                    var lastUpdate = '';
                                    let parcelas;
                                    
                                    switch (op.modalidade.tipo){
                                        case 'U':
                                            categoryTemplate = divs.categoryU;
                                            paymentTemplate = divs.payment;
                                            categoryTemplate = categoryTemplate.replace('#U.DESCRICAO#', op.modalidade.descricaoFiltro);
                                            categoryTemplate = categoryTemplate.replace('#U.EMPRESA#', op.empresa.nomeCompletoEmpresa);
                                            categoryTemplate = categoryTemplate.replace('#U.CNPJ#', op.empresa.cnpj);
                                            categoryTemplate = categoryTemplate.replace('#U.NUMERO_OPERACAO#', op.numeroDaOperacao);
                                            contractStart = op.inicioDoContrato? ''+ op.inicioDoContrato.substr(8) + '/' + op.inicioDoContrato.substr(5,2) + '/' + op.inicioDoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#U.INICIO_CONTRATO#', contractStart);
                                            lastUpdate = op.ultimaAtualizacao? '' + op.ultimaAtualizacao.substr(8) + '/' + op.ultimaAtualizacao.substr(5,2) + '/' + op.ultimaAtualizacao.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#U.ULTIMA_ATUALIZACAO#', lastUpdate);
                                            
                                            phones = getFormatedPhones(op.empresa);
                                            categoryTemplate = categoryTemplate.replace('#U.TELEFONES#', phones);
                                            op.parcelas.forEach( (parcela) => {
                                                let dueDate = parcela.dataDoVencimento? '' + parcela.dataDoVencimento.substr(8) + '/' + parcela.dataDoVencimento.substr(5,2) + '/' + parcela.dataDoVencimento.substr(0,4) : '';
                                                let paymentDate = parcela.dataDoPagamento? '' + parcela.dataDoPagamento.substr(8) + '/' + parcela.dataDoPagamento.substr(5,2) + '/' + parcela.dataDoPagamento.substr(0,4) : '';
                                                listaVencimento += '<span class="text-muted">' + dueDate + '</span><br>';
                                                listaValor += '<span class="text-muted">R$ ' + parcela.valorDaParcela + '</span><br>';
                                                listaDataPagamento += '<span class="text-muted">' + paymentDate + '</span><br>';
                                                listaValorPagamento += '<span class="text-muted">R$ ' + parcela.valorDoPagamento + '</span><br>';
                                            });
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VENCIMENTO#', listaVencimento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR#', listaValor);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_DATA_PAGAMENTO#', listaDataPagamento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR_PAGAMENTO#', listaValorPagamento);
                                            
                                            categoryTemplate = categoryTemplate.replace('#PAYMENTS#', paymentTemplate);
                                            
                                            cards += categoryTemplate;
                                            
                                            _activity.positiveInformations[0].operations.push(serasa.cadastroPositivoOperation._create({
                                                "enterprise": op.empresa.nomeCompletoEmpresa,
                                                "cnpj": op.empresa.cnpj,
                                                "operationDetails": null,
                                                "operationNumber": op.numeroDaOperacao,
                                                "description": op.modalidade.descricaoFiltro
                                            }));
                                            
                                            //EXTRATO
                                            
                                            let utilExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
                        						"name": "Extrato Informações Positivas - Utilities"
                        					}).template;
                        					
                        					parcelas = tratarParcelas(op.parcelas);
                                            utilExtractTemplate = utilExtractTemplate.replace('{operacao}', padStart(20, op.numeroDaOperacao));
                							utilExtractTemplate = utilExtractTemplate.replace('{nome_instituicao}', padStart(1000, op.empresa.nomeCompletoEmpresa));
                							utilExtractTemplate = utilExtractTemplate.replace('{cnpj_credor}', padStart(1000, op.empresa.cnpj));
                							utilExtractTemplate = utilExtractTemplate.replace('{descricao_telefone_1}', ("TEL1: " + padStart(1000, op.empresa.descricaoTipoTelefone1)));
                							utilExtractTemplate = utilExtractTemplate.replace('{telefone_1}', padStart(1000, (op.empresa.codigoTelefonePais1 + " " + op.empresa.numeroTelefoneDdd1 + " " + op.empresa.numeroTelefone1)));
                							utilExtractTemplate = utilExtractTemplate.replace('{descricao_telefone_2}', ("TEL2: " + padStart(1000, op.empresa.descricaoTipoTelefone2)));
                							utilExtractTemplate = utilExtractTemplate.replace('{telefone_2}', padStart(1000, (op.empresa.codigoTelefonePais2 + " " + op.empresa.numeroTelefoneDdd2 + " " + op.empresa.numeroTelefone2)));
                							utilExtractTemplate = utilExtractTemplate.replace('{modalidade}', padStart(1000, op.modalidade.descricaoFiltro));
                							utilExtractTemplate = utilExtractTemplate.replace('{numero_contrato}', padStart(1000, op.numeroDoContrato));
                							utilExtractTemplate = utilExtractTemplate.replace('{agencia}', padStart(9, op.numeroDaAgencia));
                							utilExtractTemplate = utilExtractTemplate.replace('{valor_contrato}', padStart(19, op.valorDaUltimaParcela));
                							utilExtractTemplate = utilExtractTemplate.replace('{valor_proxima_parcela}', padStart(20, parcelas.valorProxParcela));
                							utilExtractTemplate = utilExtractTemplate.replace('{data_inicio}', padStart(23, tratarData(op.inicioDoContrato)));
                							utilExtractTemplate = utilExtractTemplate.replace('{numero_parcelas}', padStart(24, op.parcelas.length));
                							utilExtractTemplate = utilExtractTemplate.replace('{parcelas_vencer}', padStart(19, parcelas.parcelasNaoVencidas));
                							utilExtractTemplate = utilExtractTemplate.replace('{parcelas_12}', padStart(30, parcelas.parcelasPgUltMeses));
                							utilExtractTemplate = utilExtractTemplate.replace('{em_dia}', padStart(1000, parcelas.parcelasPgEmDia));
                							utilExtractTemplate = utilExtractTemplate.replace('{atrasado}', padStart(1000, parcelas.parcelasPgAtrasadas));
                							utilExtractTemplate = utilExtractTemplate.replace('{data_atualizacao}', padStart(12, tratarData(op.ultimaAtualizacao)));
                							
                							utilExtractTemplateAcc += utilExtractTemplate;
                                            
                                            break;
                                        case 'S':
                                            categoryTemplate = divs.categoryS;
                                            paymentTemplate = divs.payment;
                                            
                                            categoryTemplate = divs.categoryS;
                                            paymentTemplate = divs.payment;
                                            categoryTemplate = categoryTemplate.replace('#S.DESCRICAO#', op.modalidade.descricaoFiltro);
                                            categoryTemplate = categoryTemplate.replace('#S.EMPRESA#', op.empresa.nomeCompletoEmpresa);
                                            categoryTemplate = categoryTemplate.replace('#S.CNPJ#', op.empresa.cnpj);
                                            categoryTemplate = categoryTemplate.replace('#S.AGENCIA#', op.numeroDaAgencia);
                                            categoryTemplate = categoryTemplate.replace('#S.NUMERO_OPERACAO#', op.numeroDaOperacao);
                                            contractStart = op.inicioDoContrato? ''+ op.inicioDoContrato.substr(8) + '/' + op.inicioDoContrato.substr(5,2) + '/' + op.inicioDoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#S.INICIO_CONTRATO#', contractStart);
                                            lastUpdate = op.ultimaAtualizacao? '' + op.ultimaAtualizacao.substr(8) + '/' + op.ultimaAtualizacao.substr(5,2) + '/' + op.ultimaAtualizacao.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#S.ULTIMA_ATUALIZACAO#', lastUpdate);
                                            
                                            phones = getFormatedPhones(op.empresa);
                                            categoryTemplate = categoryTemplate.replace('#S.TELEFONES#', phones);
                                            
                                            categoryTemplate = categoryTemplate.replace('#S.TOTAL_PARCELAS#', op.totalDeParcelas);
                                            categoryTemplate = categoryTemplate.replace('#S.PARCELAS_A_VENCER#', op.qtdParcelasFuturas);
                                            categoryTemplate = categoryTemplate.replace('#S.GRUPO#', op.codigoDoGrupo);
                                            categoryTemplate = categoryTemplate.replace('#S.COTA#', op.numeroQuota);
                                            categoryTemplate = categoryTemplate.replace('#S.SEQUENCIA#', op.numeroSequenciaQuota);
                                            categoryTemplate = categoryTemplate.replace('#S.SITUACAO_COTA#', op.statusQuota);
                                            let dataDaUltimaParcela = op.dataDaUltimaParcela? '' + op.dataDaUltimaParcela.substr(8) + '/' + op.dataDaUltimaParcela.substr(5,2) + '/' + op.dataDaUltimaParcela.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#S.DATA_CONTEMPLACAO#', dataDaUltimaParcela);
                                            categoryTemplate = categoryTemplate.replace('#S.VALOR_OBRIGACAO#', 'R$ ' + op.valorContratado);
                                            categoryTemplate = categoryTemplate.replace('#S.SALDO_DEVEDOR#', 'R$ ' + op.saldoDevedor);
                                            categoryTemplate = categoryTemplate.replace('#S.VALOR_PROXIMA_PARCELA#', 'R$ ' + op.valorDaProximaParcela);
                                            let dataDoConsorcio = op.dataDoConsorcio? '' + op.dataDoConsorcio.substr(8) + '/' + op.dataDoConsorcio.substr(5,2) + '/' + op.dataDoConsorcio.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#S.TERMINO_PREVISTO#', dataDoConsorcio);
                                            op.parcelas.forEach( (parcela) => {
                                                let dueDate = parcela.dataDoVencimento? '' + parcela.dataDoVencimento.substr(8) + '/' + parcela.dataDoVencimento.substr(5,2) + '/' + parcela.dataDoVencimento.substr(0,4) : '';
                                                let paymentDate = parcela.dataDoPagamento? '' + parcela.dataDoPagamento.substr(8) + '/' + parcela.dataDoPagamento.substr(5,2) + '/' + parcela.dataDoPagamento.substr(0,4) : '';
                                                listaVencimento += '<span class="text-muted">' + dueDate + '</span><br>';
                                                listaValor += '<span class="text-muted">R$ ' + parcela.valorDaParcela + '</span><br>';
                                                listaDataPagamento += '<span class="text-muted">' + paymentDate + '</span><br>';
                                                listaValorPagamento += '<span class="text-muted">R$ ' + parcela.valorDoPagamento + '</span><br>';
                                                
                                            });
                                            
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VENCIMENTO#', listaVencimento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR#', listaValor);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_DATA_PAGAMENTO#', listaDataPagamento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR_PAGAMENTO#', listaValorPagamento);
                                            
                                            categoryTemplate = categoryTemplate.replace('#PAYMENTS#', paymentTemplate);
                                            
                                            cards += categoryTemplate;
                                            
                                            _activity.positiveInformations[0].operations.push(serasa.cadastroPositivoOperation._create({
                                                "enterprise": op.empresa.nomeCompletoEmpresa,
                                                "cnpj": op.empresa.cnpj,
                                                "operationDetails": null,
                                                "operationNumber": op.numeroDaOperacao,
                                                "description": op.modalidade.descricaoFiltro
                                            }));
                                            
                                            //EXTRATO
                                            
                                            let consExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
                        						"name": "Extrato Informações Positivas - Consórcio"
                        					}).template;
                							
                							parcelas = tratarParcelas(op.parcelas);
                							consExtractTemplate = consExtractTemplate.replace('{operacao}', padStart(20, op.numeroDaOperacao));
                							consExtractTemplate = consExtractTemplate.replace('{nome_instituicao}', padStart(1000, op.empresa.nomeCompletoEmpresa));
                							consExtractTemplate = consExtractTemplate.replace('{cnpj_credor}', padStart(1000, op.empresa.cnpj));
                							consExtractTemplate = consExtractTemplate.replace('{descricao_telefone_1}', ("TEL1: " + padStart(1000, op.empresa.descricaoTipoTelefone1)));
                							consExtractTemplate = consExtractTemplate.replace('{telefone_1}', (op.empresa.codigoTelefonePais1 + " " + op.empresa.numeroTelefoneDdd1 + " " + op.empresa.numeroTelefone1));
                							consExtractTemplate = consExtractTemplate.replace('{descricao_telefone_2}', ("TEL2: " + padStart(1000, op.empresa.descricaoTipoTelefone2)));
                							consExtractTemplate = consExtractTemplate.replace('{telefone_2}', (op.empresa.codigoTelefonePais2 + " " + op.empresa.numeroTelefoneDdd2 + " " + op.empresa.numeroTelefone2));
                							consExtractTemplate = consExtractTemplate.replace('{modalidade}', padStart(1000, op.modalidade.descricaoFiltro));
                							consExtractTemplate = consExtractTemplate.replace('{numero_contrato}', padStart(1000, op.numeroDoContrato));
                							consExtractTemplate = consExtractTemplate.replace('{agencia}', padStart(9, op.numeroDaAgencia));
                							consExtractTemplate = consExtractTemplate.replace('{codigo_grupo}', padStart(7, op.codigoDoGrupo));
                							consExtractTemplate = consExtractTemplate.replace('{codigo_quota}', padStart(6, op.numeroQuota));
                							consExtractTemplate = consExtractTemplate.replace('{sequencia_quota}', padStart(11, op.numeroSequenciaQuota));
                							consExtractTemplate = consExtractTemplate.replace('{status_quota}', padStart(18, op.statusQuota));
                							consExtractTemplate = consExtractTemplate.replace('{data_contemplacao}', padStart(22, tratarData(op.dataDaUltimaParcela)));
                							consExtractTemplate = consExtractTemplate.replace('{data_inicio}', padStart(23, tratarData(op.inicioDoContrato)));
                							consExtractTemplate = consExtractTemplate.replace('{valor_obrigacao}', padStart(20, op.valorContratado));
                							consExtractTemplate = consExtractTemplate.replace('{saldo}', padStart(15, op.saldoDevedor));
                							consExtractTemplate = consExtractTemplate.replace('{valor_proxima_parcela}', padStart(20, op.valorDaProximaParcela));
                							consExtractTemplate = consExtractTemplate.replace('{data_termino}', padStart(21, tratarData(op.dataDoConsorcio)));
                							consExtractTemplate = consExtractTemplate.replace('{quantidade_parcelas}', padStart(24, op.totalDeParcelas));
                							consExtractTemplate = consExtractTemplate.replace('{parcelas_vencer}', padStart(19, op.qtdParcelasFuturas));
                							consExtractTemplate = consExtractTemplate.replace('{parcelas_12}', padStart(30, parcelas.parcelasPgUltMeses));
                							consExtractTemplate = consExtractTemplate.replace('{em_dia}', padStart(1000, parcelas.parcelasPgEmDia));
                							consExtractTemplate = consExtractTemplate.replace('{atrasado}', padStart(1000, parcelas.parcelasPgAtrasadas));
                							consExtractTemplate = consExtractTemplate.replace('{data_atualizacao}', padStart(12, tratarData(op.ultimaAtualizacao)));
                							
                							consExtractTemplateAcc += consExtractTemplate;
                                            
                                            break;
                                        case 'P':
                                            categoryTemplate = divs.categoryP;
                                            paymentTemplate = divs.payment;
                                            
                                            categoryTemplate = categoryTemplate.replace('#P.DESCRICAO#', op.modalidade.descricaoFiltro);
                                            categoryTemplate = categoryTemplate.replace('#P.EMPRESA#', op.empresa.nomeCompletoEmpresa);
                                            categoryTemplate = categoryTemplate.replace('#P.CNPJ#', op.empresa.cnpj);
                                            categoryTemplate = categoryTemplate.replace('#P.AGENCIA#', op.numeroDaAgencia);
                                            categoryTemplate = categoryTemplate.replace('#P.NUMERO_OPERACAO#', op.numeroDaOperacao);
                                            contractStart = op.inicioDoContrato? ''+ op.inicioDoContrato.substr(8) + '/' + op.inicioDoContrato.substr(5,2) + '/' + op.inicioDoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#P.INICIO_CONTRATO#', contractStart);
                                            lastUpdate = op.ultimaAtualizacao? '' + op.ultimaAtualizacao.substr(8) + '/' + op.ultimaAtualizacao.substr(5,2) + '/' + op.ultimaAtualizacao.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#P.ULTIMA_ATUALIZACAO#', lastUpdate);
                                            
                                            phones = getFormatedPhones(op.empresa);
                                            categoryTemplate = categoryTemplate.replace('#P.TELEFONES#', phones);
                                            
                                            categoryTemplate = categoryTemplate.replace('#P.NUMERO_CONTRATO#', op.numeroDoContrato);
                                            categoryTemplate = categoryTemplate.replace('#P.VALOR_CONTRATO#', 'R$ ' + op.totalContrato);
                                            categoryTemplate = categoryTemplate.replace('#P.TOTAL_PARCELAS#', op.qtdTotalParcelas);
                                            categoryTemplate = categoryTemplate.replace('#P.PARCELAS_A_VENCER#', op.qtdParcelasFuturas);
                                            categoryTemplate = categoryTemplate.replace('#P.VALOR_PROXIMA_PARCELA#', 'R$ ' + op.valorDaProximaParcela);
                                            let expectedEnd = op.dataExpiracaoContrato? '' + op.dataExpiracaoContrato.substr(8) + '/' + op.dataExpiracaoContrato.substr(5,2) + '/' + op.dataExpiracaoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#P.TERMINO_PREVISTO#', expectedEnd);
                                            op.parcelas.forEach( (parcela) => {
                                                let dueDate = parcela.dataDoVencimento? '' + parcela.dataDoVencimento.substr(8) + '/' + parcela.dataDoVencimento.substr(5,2) + '/' + parcela.dataDoVencimento.substr(0,4) : '';
                                                let paymentDate = parcela.dataDoPagamento? '' + parcela.dataDoPagamento.substr(8) + '/' + parcela.dataDoPagamento.substr(5,2) + '/' + parcela.dataDoPagamento.substr(0,4) : '';
                                                listaVencimento += '<span class="text-muted">' + dueDate + '</span><br>';
                                                listaValor += '<span class="text-muted">R$ ' + parcela.valorDaParcela + '</span><br>';
                                                listaDataPagamento += '<span class="text-muted">' + paymentDate + '</span><br>';
                                                listaValorPagamento += '<span class="text-muted">R$ ' + parcela.valorDoPagamento + '</span><br>';
                                            });
                                            
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VENCIMENTO#', listaVencimento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR#', listaValor);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_DATA_PAGAMENTO#', listaDataPagamento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR_PAGAMENTO#', listaValorPagamento);
                                            
                                            categoryTemplate = categoryTemplate.replace('#PAYMENTS#', paymentTemplate);
                                            
                                            cards += categoryTemplate;
                                            
                                            _activity.positiveInformations[0].operations.push(serasa.cadastroPositivoOperation._create({
                                                "enterprise": op.empresa.nomeCompletoEmpresa,
                                                "cnpj": op.empresa.cnpj,
                                                "operationDetails": null,
                                                "operationNumber": op.numeroDaOperacao,
                                                "description": op.modalidade.descricaoFiltro
                                            }));
                                            
                                            //EXTRATO
                                            
                                            let parcExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
                        						"name": "Extrato Informações Positivas - Parcelado"
                        					}).template;
                							
                							parcelas = tratarParcelas(op.parcelas);
                							parcExtractTemplate = parcExtractTemplate.replace('{operacao}', padStart(20, op.numeroDaOperacao));
                							parcExtractTemplate = parcExtractTemplate.replace('{nome_instituicao}', padStart(1000, op.empresa.nomeCompletoEmpresa));
                							parcExtractTemplate = parcExtractTemplate.replace('{cnpj_credor}', padStart(1000, op.empresa.cnpj));
                							parcExtractTemplate = parcExtractTemplate.replace('{descricao_telefone_1}', ("TEL1: " + padStart(1000, op.empresa.descricaoTipoTelefone1)));
                							parcExtractTemplate = parcExtractTemplate.replace('{telefone_1}', (op.empresa.codigoTelefonePais1 + " " + op.empresa.numeroTelefoneDdd1 + " " + op.empresa.numeroTelefone1));
                							parcExtractTemplate = parcExtractTemplate.replace('{descricao_telefone_2}', ("TEL2: " + padStart(1000, op.empresa.descricaoTipoTelefone2)));
                							parcExtractTemplate = parcExtractTemplate.replace('{telefone_2}', (op.empresa.codigoTelefonePais2 + " " + op.empresa.numeroTelefoneDdd2 + " " + op.empresa.numeroTelefone2));
                							parcExtractTemplate = parcExtractTemplate.replace('{modalidade}', padStart(1000, op.modalidade.descricaoFiltro));
                							parcExtractTemplate = parcExtractTemplate.replace('{numero_contrato}', padStart(1000, op.numeroDoContrato));
                							parcExtractTemplate = parcExtractTemplate.replace('{agencia}', padStart(9, op.numeroDaAgencia));
                							parcExtractTemplate = parcExtractTemplate.replace('{valor_obrigacao}', padStart(19, op.totalContrato));
                							parcExtractTemplate = parcExtractTemplate.replace('{valor_proxima_parcela}', padStart(20, op.valorDaProximaParcela));
                							parcExtractTemplate = parcExtractTemplate.replace('{data_inicio}', padStart(23, tratarData(op.inicioDoContrato)));
                							parcExtractTemplate = parcExtractTemplate.replace('{data_termino}', padStart(21, tratarData(op.dataExpiracaoContrato)));
                							parcExtractTemplate = parcExtractTemplate.replace('{quantidade_parcelas}', padStart(24, op.qtdParcelasFuturas));
                							parcExtractTemplate = parcExtractTemplate.replace('{parcelas_vencer}', padStart(19, op.qtdParcelasFuturas));
                							parcExtractTemplate = parcExtractTemplate.replace('{parcelas_12}', padStart(30, parcelas.parcelasPgUltMeses));
                							parcExtractTemplate = parcExtractTemplate.replace('{em_dia}', padStart(1000, parcelas.parcelasPgEmDia));
                							parcExtractTemplate = parcExtractTemplate.replace('{atrasado}', padStart(1000, parcelas.parcelasPgAtrasadas));
                							parcExtractTemplate = parcExtractTemplate.replace('{quantidade_parcelas}', padStart(24, op.qtdTotalParcelas));
                							parcExtractTemplate = parcExtractTemplate.replace('{data_atualizacao}', padStart(12, tratarData(op.ultimaAtualizacao)));
                							
                							parcExtractTemplateAcc += parcExtractTemplate;
                                            
                                            break;
                                        case 'R':
                                            categoryTemplate = divs.categoryR;
                                            paymentTemplate = divs.paymentRotative;
                                            
                                            categoryTemplate = categoryTemplate.replace('#R.DESCRICAO#', op.modalidade.descricaoFiltro);
                                            categoryTemplate = categoryTemplate.replace('#R.EMPRESA#', op.empresa.nomeCompletoEmpresa);
                                            categoryTemplate = categoryTemplate.replace('#R.CNPJ#', op.empresa.cnpj);
                                            categoryTemplate = categoryTemplate.replace('#R.AGENCIA#', op.numeroDaAgencia);
                                            categoryTemplate = categoryTemplate.replace('#R.NUMERO_OPERACAO#', op.numeroDaOperacao);
                                            contractStart = op.inicioDoContrato? ''+ op.inicioDoContrato.substr(8) + '/' + op.inicioDoContrato.substr(5,2) + '/' + op.inicioDoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#R.INICIO_CONTRATO#', contractStart);
                                            lastUpdate = op.ultimaAtualizacao? '' + op.ultimaAtualizacao.substr(8) + '/' + op.ultimaAtualizacao.substr(5,2) + '/' + op.ultimaAtualizacao.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#R.ULTIMA_ATUALIZACAO#', lastUpdate);
                                            phones = getFormatedPhones(op.empresa);
                                            categoryTemplate = categoryTemplate.replace('#R.TELEFONES#', phones);
                                            
                                            op.saldosRotativo.forEach( (rotativo) => {
                                                let dueDate = rotativo.dataVencimento? '' + rotativo.dataVencimento.substr(8) + '/' + rotativo.dataVencimento.substr(5,2) + '/' + rotativo.dataVencimento.substr(0,4) : '';
                                                listaVencimento += '<span class="text-muted">' + dueDate + '</span><br>';
                                                listaValor += '<span class="text-muted">R$ ' + rotativo.saldoUtilizado + '</span><br>';
                                            });
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VENCIMENTO#', listaVencimento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR#', listaValor);
                                            categoryTemplate = categoryTemplate.replace('#PAYMENTS#', paymentTemplate);
                                            cards += categoryTemplate;
                                            
                                            _activity.positiveInformations[0].operations.push(serasa.cadastroPositivoOperation._create({
                                                "enterprise": op.empresa.nomeCompletoEmpresa,
                                                "cnpj": op.empresa.cnpj,
                                                "operationDetails": null,
                                                "operationNumber": op.numeroDaOperacao,
                                                "description": op.modalidade.descricaoFiltro
                                            }));
                                            
                                            //EXTRATO
                                            
                                            let rotativoExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
                        						"name": "Extrato Informações Positivas - Rotativo"
                        					}).template;
                							
                							
                							function tratarRotativo(saldosRotativo){
                							    let stringRetorno = "";
                							    saldosRotativo.forEach((sr, index) => {
                							        if (index == 0){
                							            stringRetorno += tratarData(sr.dataVencimento) + padStart(sr.dataVencimento.length, sr.saldoUtilizado);
                							        } else {
                							            stringRetorno += "\n" + tratarData(sr.dataVencimento) + padStart(sr.dataVencimento.length, sr.saldoUtilizado);
                							        }
                							    });
                							    return stringRetorno;
                							}
                							
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{operacao}', padStart(20, op.numeroDaOperacao));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{nome_instituicao}', op.empresa.nomeCompletoEmpresa);
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{cnpj_credor}', op.empresa.cnpj);
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{descricao_telefone_1}', ("TEL1: " + padStart(1000, op.empresa.descricaoTipoTelefone1)));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{telefone_1}', (op.empresa.codigoTelefonePais1 + " " + op.empresa.numeroTelefoneDdd1 + " " + op.empresa.numeroTelefone1));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{descricao_telefone_2}', ("TEL2: " + padStart(1000, op.empresa.descricaoTipoTelefone2)));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{telefone_2}', (op.empresa.codigoTelefonePais2 + " " + op.empresa.numeroTelefoneDdd2 + " " + op.empresa.numeroTelefone2));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{modalidade}', padStart(1000, op.modalidade.descricaoFiltro));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{numero_contrato}', padStart(1000, op.numeroDoContrato));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{agencia}', padStart(9, op.numeroDaAgencia));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{dt_contrato}', padStart(23, op.inicioDoContrato));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{data_vencimento}', tratarRotativo(op.saldosRotativo));
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{saldo}', "");
                							rotativoExtractTemplate = rotativoExtractTemplate.replace('{data_atualizacao}', padStart(12, tratarData(op.ultimaAtualizacao)));
                							
                							rotativoExtractTemplateAcc += rotativoExtractTemplate;
                                            
                                            break;
                                        case 'C':
                							
                                            categoryTemplate = divs.categoryC;
                                            paymentTemplate = divs.payment;
                                            
                                            categoryTemplate = categoryTemplate.replace('#C.DESCRICAO#', op.modalidade.descricaoFiltro);
                                            categoryTemplate = categoryTemplate.replace('#C.EMPRESA#', op.empresa.nomeCompletoEmpresa);
                                            categoryTemplate = categoryTemplate.replace('#C.CNPJ#', op.empresa.cnpj);
                                            categoryTemplate = categoryTemplate.replace('#C.AGENCIA#', op.numeroDaAgencia);
                                            categoryTemplate = categoryTemplate.replace('#C.NUMERO_OPERACAO#', op.numeroDaOperacao);
                                            contractStart = op.inicioDoContrato? ''+ op.inicioDoContrato.substr(8) + '/' + op.inicioDoContrato.substr(5,2) + '/' + op.inicioDoContrato.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#C.INICIO_CONTRATO#', contractStart);
                                            lastUpdate = op.ultimaAtualizacao? '' + op.ultimaAtualizacao.substr(8) + '/' + op.ultimaAtualizacao.substr(5,2) + '/' + op.ultimaAtualizacao.substr(0,4) : '';
                                            categoryTemplate = categoryTemplate.replace('#C.ULTIMA_ATUALIZACAO#', lastUpdate);
                                            
                                            phones = getFormatedPhones(op.empresa);
                                            categoryTemplate = categoryTemplate.replace('#C.TELEFONES#', phones);
                                            
                                            categoryTemplate = categoryTemplate.replace('#C.NUMERO_CARTAO#', op.numeroDoCartao);
                                            categoryTemplate = categoryTemplate.replace('#C.FATURAS_VALOR#', 'R$ ' + op.valorDaUltimaParcela);
                                            categoryTemplate = categoryTemplate.replace('#C.FATURAS_SALDO#', 'R$ ' + op.valorDaProximaParcela);
                                            
                                            op.parcelas.forEach( (parcela) => {
                                                let dueDate = parcela.dataDoVencimento? '' + parcela.dataDoVencimento.substr(8) + '/' + parcela.dataDoVencimento.substr(5,2) + '/' + parcela.dataDoVencimento.substr(0,4) : '';
                                                let paymentDate = parcela.dataDoPagamento? '' + parcela.dataDoPagamento.substr(8) + '/' + parcela.dataDoPagamento.substr(5,2) + '/' + parcela.dataDoPagamento.substr(0,4) : '';
                                                listaVencimento += '<span class="text-muted">' + dueDate + '</span><br>';
                                                listaValor += '<span class="text-muted">R$ ' + parcela.valorDaParcela + '</span><br>';
                                                listaDataPagamento += '<span class="text-muted">' + paymentDate + '</span><br>';
                                                listaValorPagamento += '<span class="text-muted">R$ ' + parcela.valorDoPagamento + '</span><br>';
                                            });
                                            
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VENCIMENTO#', listaVencimento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR#', listaValor);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_DATA_PAGAMENTO#', listaDataPagamento);
                                            paymentTemplate = paymentTemplate.replace('#LISTA_VALOR_PAGAMENTO#', listaValorPagamento);
                                            
                                            categoryTemplate = categoryTemplate.replace('#PAYMENTS#', paymentTemplate);
                                            
                                            cards += categoryTemplate;
                                            
                                            _activity.positiveInformations[0].operations.push(serasa.cadastroPositivoOperation._create({
                                                "enterprise": op.empresa.nomeCompletoEmpresa,
                                                "cnpj": op.empresa.cnpj,
                                                "operationDetails": null,
                                                "operationNumber": op.numeroDaOperacao,
                                                "description": op.modalidade.descricaoFiltro
                                            }));
                                            
                                            //EXTRATO
                                            
                                            let cardExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
                        						"name": "Extrato Informações Positivas - Cartão"
                        					}).template;
                                            
                                            parcelas = tratarParcelas(op.parcelas);
                							cardExtractTemplate = cardExtractTemplate.replace('{nome_instituicao}', padStart(1000, op.empresa.nomeCompletoEmpresa));
                							cardExtractTemplate = cardExtractTemplate.replace('{cnpj_credor}', padStart(1000, op.empresa.cnpj));
                							cardExtractTemplate = cardExtractTemplate.replace('{operacao}', padStart(20, op.numeroDaOperacao));
                							cardExtractTemplate = cardExtractTemplate.replace('{descricao_telefone_1}', ("TEL1: " + padStart(1000, op.empresa.descricaoTipoTelefone1)));
                							cardExtractTemplate = cardExtractTemplate.replace('{telefone_1}', (op.empresa.codigoTelefonePais1 + " " + op.empresa.numeroTelefoneDdd1 + " " + op.empresa.numeroTelefone1));
                							cardExtractTemplate = cardExtractTemplate.replace('{descricao_telefone_2}', ("TEL2: " + padStart(1000, op.empresa.descricaoTipoTelefone2)));
                							cardExtractTemplate = cardExtractTemplate.replace('{telefone_2}', (op.empresa.codigoTelefonePais2 + " " + op.empresa.numeroTelefoneDdd2 + " " + op.empresa.numeroTelefone2));
                							cardExtractTemplate = cardExtractTemplate.replace('{modalidade}', op.modalidade.descricaoFiltro);
                							cardExtractTemplate = cardExtractTemplate.replace('{numero_contrato}', padStart(1000, op.numeroDoContrato));
                							cardExtractTemplate = cardExtractTemplate.replace('{agencia}', padStart(9, op.numeroDaAgencia));
                							cardExtractTemplate = cardExtractTemplate.replace('{saldo}', padStart(15, parcelas.saldoTotal));
                							cardExtractTemplate = cardExtractTemplate.replace('{valor_fatura}', padStart(21, tratarData(op.valorDaUltimaParcela)));
                							cardExtractTemplate = cardExtractTemplate.replace('{dt_contrato}', padStart(23, tratarData(op.inicioDoContrato)));
                							cardExtractTemplate = cardExtractTemplate.replace('{faturas_12}', padStart(29, parcelas.parcelasPgUltMeses));
                							cardExtractTemplate = cardExtractTemplate.replace('{faturas_vencer}', padStart(18, parcelas.parcelasNaoVencidas));
                							cardExtractTemplate = cardExtractTemplate.replace('{em_dia}', padStart(1000, parcelas.parcelasPgEmDia));
                							cardExtractTemplate = cardExtractTemplate.replace('{atrasado}', padStart(1000, parcelas.parcelasPgAtrasadas));
                							cardExtractTemplate = cardExtractTemplate.replace('{data_atualizacao}', padStart(12, tratarData(op.ultimaAtualizacao)));
                							
                							cardExtractTemplateAcc += cardExtractTemplate;
                							
                                            break;
                                    }
                                });
                                let templateAcc = utilExtractTemplateAcc + consExtractTemplateAcc + parcExtractTemplateAcc + rotativoExtractTemplateAcc + cardExtractTemplateAcc;
                                extractTemplate = extractTemplate.replace('{infos}', templateAcc);
                                
                                template = template.replace('#CARDS#', cards);
                            } 
                            
                            function isEmpty(obj) {
                                for(var prop in obj) {
                                    if(obj.hasOwnProperty(prop))
                                        return false;
                                }
                            
                                return true;
                            }
                            
                            function dataDiaMesAno(data){
                                let varData = (data.getDate() > 9 ?  data.getDate() : "0" + data.getDate()) + "/" +
                                    ((data.getMonth() + 1) > 9 ? (data.getMonth() + 1) : "0" + (data.getMonth() + 1)) + "/" +
                                    (data.getFullYear());
                                    
                                return varData;
                            }
                            
                            function horaFormatada(data){
                                let varHora = (data.getHours() > 9 ? data.getHours() : "0" + data.getHours()) + ":" + 
                                    (data.getMinutes() > 9 ? data.getMinutes() : "0" + data.getMinutes()) + ":" + 
                                    (data.getSeconds() > 9 ? data.getSeconds() : "0" + data.getSeconds());
                                    
                                return varHora;
                            }
                            
                            let data = new Date();
                            
                            if(isEmpty(response.operacoes)){
                                //EXTRATO INFORMAÇÕES POSITIVAS VAZIO
                                let emptyExtractTemplate = serasa.cadastroPositivoTemplates.findByName({
    								"name": "Extrato Vazio Informações Positivas"
    							}).template;
    							
    							emptyExtractTemplate = emptyExtractTemplate.replace('{data}', padStart(29, dataDiaMesAno(data)));
    							emptyExtractTemplate = emptyExtractTemplate.replace('{hora}', padStart(0, horaFormatada(data)));
    							if(_activity.cpf)
    							    emptyExtractTemplate = emptyExtractTemplate.replace('{cpf}', padStart(0, "CPF: " + maskCPF(_activity.cpf)));
    							if(_activity.cnpj)
    							    emptyExtractTemplate = emptyExtractTemplate.replace('{cpf}', padStart(0, "CNPJ: " + _activity.cnpj));
    							emptyExtractTemplate = emptyExtractTemplate.replace('{data}', dataDiaMesAno(data));
    							emptyExtractTemplate = emptyExtractTemplate.replace('{hora}', horaFormatada(data));
    								
                                let emptyExtractHtmlFile = _util._fileHelper.create({
                                    content: emptyExtractTemplate,
                                    targerContentType:  "text/html",
                                    targetFileName: "Extrato - Informações Positivas.html",
                                    targetCharset: "UTF-8"
                                });
    
                                let emptyExtractFile = _util._fileHelper.convert({
                                    file: emptyExtractHtmlFile,
                                    targerContentType:  "application/pdf",
                                    targetFileName: "Extrato Vazio - Informações Positivas.pdf",
                                    targetCharset: "UTF-8"
                                });
                                
                                _activity.extracts = [];
                                _activity.extracts.push(emptyExtractFile);
                                _activity.attendanceData.extracts = emptyExtractFile; // disponibilizar extrato ao final do atendimento
                            } else {
                                //EXTRATO INFORMAÇÕES POSITIVAS PREENCHIDO
                                extractTemplate = extractTemplate.replace('{data}', padStart(29, dataDiaMesAno(data)));
    							extractTemplate = extractTemplate.replace('{hora}', padStart(0, horaFormatada(data)));
    							if(_activity.cpf)
    							    extractTemplate = extractTemplate.replace('{cpf}', padStart(0, "CPF: " + maskCPF(_activity.cpf)));
    							if(_activity.cnpj)
    							    extractTemplate = extractTemplate.replace('{cpf}', padStart(0, "CNPJ: " + _activity.cnpj));
                                extractTemplate = extractTemplate.replace('{relatorio}', response.protocolo.numeroProtocolo);
    							extractTemplate = extractTemplate.replace('{data}', dataDiaMesAno(data));
    							extractTemplate = extractTemplate.replace('{hora}', horaFormatada(data));
    							extractTemplate = extractTemplate.replace('{data}', dataDiaMesAno(data));
    							extractTemplate = extractTemplate.replace('{hora}', horaFormatada(data));
    								
                                let extractHtmlFile = _util._fileHelper.create({
                                    content: extractTemplate,
                                    targerContentType:  "text/html",
                                    targetFileName: "Extrato - Informações Positivas.html",
                                    targetCharset: "UTF-8"
                                });
    
                                let extractFile = _util._fileHelper.convert({
                                    file: extractHtmlFile,
                                    targerContentType:  "application/pdf",
                                    targetFileName: "Extrato - Informações Positivas.pdf",
                                    targetCharset: "UTF-8"
                                });
                                
                                _activity.extracts = [];
                                _activity.extracts.push(extractFile);
                                _activity.attendanceData.extracts = extractFile; // disponibilizar extrato ao final do atendimento
                            }
                            
                            htmlFile = _util._fileHelper.create({
                                    content: template,
                                    targerContentType:  "text/html",
                                    targetFileName: "Relatorio Informações Positivas.html",
                                    targetCharset: "UTF-8"
                                });
                            
                            file = _util._fileHelper.convert({
                                    file: htmlFile,
                                    targerContentType:  "application/pdf",
                                    targetFileName: "Relatorio Informações Positivas.pdf",
                                    targetCharset: "UTF-8"
                                });
                                
                            _activity.report.push(file);
                            _activity.attendanceData.positiveInformationsReport = file;
                            
						}
						
                        break;
                        
                    /////RELATÓRIO DE CONSULENTES//////
                    case 'Consulentes':
                     
						if (_activity.attendanceData && !_activity.attendanceData.consulentsReport){
							
							response = serasa.cadastroPositivoIntegration.getConsulentsInformation(body).response;
							
							let date = new Date(_activity._lastUpdateDate);
							
							if(response.length && response.length > 0){
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Consulentes"
								}).template;
								
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
								let html = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Consulentes"
								}).reportDivs;
								
								html = JSON.parse(html).consulents;
								
								
								var consulents = '';
								response.forEach((consulent) => {
									var f = html;
									
									f = f.replace('#RAZAO_SOCIAL#', consulent.razaoSocial? consulent.razaoSocial : "");
									f = f.replace('#NOME_FANTASIA#', consulent.nomeFantasia? consulent.nomeFantasia : "");
									f = f.replace('#CNPJ#', consulent.cnpj? consulent.cnpj : "");
									f = f.replace('#MOTIVO#', consulent.razao? consulent.razao : "");
									
									consulents += f;
								}); 
								template = template.replace('#COMPANY#', consulents);
								
							}else if(response.length === 0){
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Consulentes Vazio"
								}).template;
								
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
							}
							
							htmlFile = _util._fileHelper.create({
									content: template,
									targerContentType:  "text/html",
									targetFileName: "Relatorio Consulents.html",
									targetCharset: "UTF-8"
								});
								
							var month = twoDigits(date.getMonth() + 1);
							var day =  twoDigits(date.getDate());
							var hours = twoDigits(date.getHours());
							var minutes = twoDigits(date.getMinutes());
							var seconds = twoDigits(date.getSeconds());
							
							var doc = body.cpf? body.cpf: body.cnpj;
							
							file = _util._fileHelper.convert({
									file: htmlFile,
									targerContentType:  "application/pdf",
									targetFileName: "Relatório Consulentes - "+ doc +"_" + date.getFullYear()+ "" + month + "" + day + "" + hours + "" + minutes + "" + seconds + ".pdf",
									targetCharset: "UTF-8"
								});
							
							_activity.report.push(file);
							_activity.attendanceData.consulentsReport = file;
							
							var protocol_file = generateProtocol('consulentes');
							_activity.attendanceData.protocols.push(protocol_file);
							_activity.protocols.push(protocol_file);
						}
                        
                        break;
                        
                    /////RELATÓRIO DE HISTÓRICO//////            
                    case 'Histórico':
                        
						if (_activity.attendanceData && !_activity.attendanceData.historyReport){
								
							response = serasa.cadastroPositivoIntegration.getHistoryInformation(body).response;
							
							let date = new Date(_activity._lastUpdateDate)
							
							if(response.length && response.length > 0){
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Histórico de Adesão/Cancelamento"}
								).template;
								
								
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
								let html = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Histórico de Adesão/Cancelamento"}
								).reportDivs;
								
								html = JSON.parse(html).history;
								
								
								var history = '';
								response.forEach((h) => {
									var f = html;
									
									if(h.ativo == true){
										f = f.replace('#STATUS#', "Adesão");   
									}else if(h.ativo == false){
										f = f.replace('#STATUS#', "Cancelamento");   
									}
									
									
									if(h.dataRegistro){
										var d = new Date(h.dataRegistro);
										f = f.replace('#DATA_HISTORICO#', twoDigits(d.getDate()) + '/' + twoDigits((d.getMonth() + 1)) + '/' + d.getFullYear() + ' ' + twoDigits(d.getUTCHours()) + ':' + twoDigits(d.getMinutes()));   
									}
									
									
									if(h.empresa && h.empresa.razaoSocial){
										f = f.replace('#EMPRESA#', h.empresa.razaoSocial);
									}
									if(h.empresa && h.empresa.cnpj)
									f = f.replace('#CNPJ#', h.empresa.cnpj);
									
									history += f;
								}); 
								template = template.replace('#HISTORY#', history);
								
							
							} else if(response.length === 0){
								template = serasa.cadastroPositivoTemplates.findByName({
									"name": "Relatório de Histórico de Adesão/Cancelamento"}
								).template;
								
								template = template.replace('#DATA#', getFormatedDate(date));
								template = template.replace('#NOME#', nome);
								if (body.cpf) {
									template = template.replace('#TIPODOCUMENTO#', "CPF");
									template = template.replace('#DOCUMENTO#', body.cpf);
								} else if (body.cnpj) {
									template = template.replace('#TIPODOCUMENTO#', "CNPJ");
									template = template.replace('#DOCUMENTO#', body.cnpj);
								}
								
								template = template.replace('#HISTORY#', '');
							}
							
							htmlFile = _util._fileHelper.create({
									content: template,
									targerContentType:  "text/html",
									targetFileName: "Relatorio Historico.html",
									targetCharset: "UTF-8"
								});
								
							let month = twoDigits(date.getMonth() + 1);
							let day =  twoDigits(date.getDate());
							let hours = twoDigits(date.getHours());
							let minutes = twoDigits(date.getMinutes());
							let seconds = twoDigits(date.getSeconds());
							
							let doc = body.cpf? body.cpf: body.cnpj;
							
							file = _util._fileHelper.convert({
									file: htmlFile,
									targerContentType:  "application/pdf",
									targetFileName: "Relatório Histórico de Adesão Cancelamento - "+ doc + "_" + date.getFullYear()+ "" + month + "" + day + "" + hours + "" + minutes + "" + seconds + ".pdf",
									targetCharset: "UTF-8"
								});
								
							_activity.report.push(file);
							_activity.attendanceData.historyReport = file;
							
							var protocol_file = generateProtocol('historico');
							_activity.attendanceData.protocols.push(protocol_file);
							_activity.protocols.push(protocol_file);
						}
                        
                        break;
                        
                    default:
                        break;
                } 
            
            } catch (e){
                _utils.addErrorMessage("Não foi possível obter relatórios do cliente!");
                var log;
                log = e.message? e.message : e;
                _utils.log('>>> ERRO - Geração de Relatório Cadastro Positivo: ' + _utils.stringifyAsJson(log));
                //throw (log);
            }    
	    }
	    
        
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    
        function getFormatedDate(date){
            var months = ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"];
            return date.getDate() + ' de ' + months[date.getMonth()] + ' de ' + date.getFullYear() + ' ' + twoDigits(date.getHours()) + ':' + twoDigits(date.getMinutes());
        }
        
        function twoDigits(number){
            var n = number+'';
            n = n.length > 1? n : '0'+n;
            return n;
        }
        
        function formatPhone(string){
        	if(string !== null && string !== undefined && string !== "0" && string !== "00"){
        		return string + ' ';
        	}
    	    return '';
        }
        
        function getFormatedPhones(enterprise){
            
            var unformatedPhones = [
                ''+ formatPhone(enterprise.descricaoTipoTelefone1) + formatPhone(enterprise.codigoTelefonePais1) + formatPhone(enterprise.numeroTelefoneDdd1) + formatPhone(enterprise.numeroTelefone1),
                ''+ formatPhone(enterprise.descricaoTipoTelefone2) + formatPhone(enterprise.codigoTelefonePais2) + formatPhone(enterprise.numeroTelefoneDdd2) + formatPhone(enterprise.numeroTelefone2),
                ''+ formatPhone(enterprise.descricaoTipoTelefone3) + formatPhone(enterprise.codigoTelefonePais3) + formatPhone(enterprise.numeroTelefoneDdd3) + formatPhone(enterprise.numeroTelefone3),
                ''+ formatPhone(enterprise.descricaoTipoTelefone4) + formatPhone(enterprise.codigoTelefonePais4) + formatPhone(enterprise.numeroTelefoneDdd4) + formatPhone(enterprise.numeroTelefone4),
                ''+ formatPhone(enterprise.descricaoTipoTelefone5) + formatPhone(enterprise.codigoTelefonePais5) + formatPhone(enterprise.numeroTelefoneDdd5) + formatPhone(enterprise.numeroTelefone5)];
            var phone = '';
            unformatedPhones.forEach((p)=>{
                if(p!== ''){
                    phone += (p + ' | ');
                }
            });
            phone = phone.substring(0, phone.length-3);
            return phone;
        }
        
	}
	
	function setAttendanceAction(){
	    function setAttendaceActionCadastroPositivo(){
	        if(_activity.prodServ && _activity.prodServ.identifier === "cadastropositivo" && _activity.reason && _activity.reason.identifier === "cadastropositivo.consultainfopositivas"){
	            var result = crm.customerServiceAction._search({
	                "query": {
	                    "bool": {
	                        "must": {
	                            "term": {
	                                "identifier.keyword": "cadastroPositivoGenerateReport"
	                            }
	                        }
	                    }
	                }
	            });
	            if(result && result.hits && result.hits.total > 0){
	                _activity.customerServiceAction = result.hits.hits[0]._source;
	            }
	       
	        }
	    }
	    ////////////////////////////////////////////
	    
	    setAttendaceActionCadastroPositivo();
	}

	///////// Para facilitar a transição, os campos _object.[prodServ, reason, subReason, subSubReason] continuarão a ser usados
	var tabTypes = ["prodServ", "reason", "subReason", "subSubReason"];

	tabTypes.forEach(function (tabType) {
		if (_activity.attendanceTab && _activity.attendanceTab.length) {
			_activity[tabType] = _activity.attendanceTab[0][tabType];
		}
	});

	tabTypes.forEach(function (tabType) {
		if (_oldObject.attendanceTab && _oldObject.attendanceTab.length) {
			_oldObject[tabType] = _oldObject.attendanceTab[0][tabType];
		}
	});

	/////////
	updateDraftDetails();
	checkForFieldChanges();
	treatVcpeAfResendInviteEmail();    /// Ações 'resendVCPEMail', 'changeEmailAndPhoneAntiFraude', 'resendDependentInvite'
	fillNegotiations();
	initializeEmailHistory();
	initialiazeZendeskHistory();
	initializeSmsHistoryAndEmail();
	initializeContactData();
	verifyAnonymousContactActivity(); ///Verificação se contato não identificado pode prosseguir no atendimento
	calculateSLAData();               ///Cálculo dos dados de SLA sempre que tabulação mudar
	resetShowFields();
	setAttendanceAction();
	treatGenerateReport();
	/////////
	tabTypes.forEach(function (tabType) {
		if (_activity.attendanceTab && _activity.attendanceTab.length) {
			_activity.attendanceTab[0][tabType] = _activity[tabType];
		}
	});
	tabTypes.forEach(function (tabType) {
		if (_oldObject.attendanceTab && _oldObject.attendanceTab.length) {
			_oldObject.attendanceTab[0][tabType] = _oldObject[tabType];
		}
	});
	/////////
}

function performAttendanceEndCheck() {
	function validateClientDataForSale() {

		if (_activity.antifraudeContract) {
			throw "Já existe um plano AntiFraude ativo para o cliente selecionado.";
		}
		
		if (_activity.cpf && _activity.customer.receitaFederal && _activity.customer.receitaFederal.rfSituation &&
			_activity.customer.receitaFederal.rfSituation.identifier !== 'regular') {
			throw "Não é possível prosseguir para a venda pois o consumidor não possui a situação regularizada na Receita Federal.";
		} 
		
		if (!_activity.cpf) {
		    throw "Contato anônimo não pode prosseguir este fluxo de atendimento.";
		}

	}

    ////////////////////////

	if (_activity.reason && _activity.reason.identifier === "serasaantifraude.cancelamento" && _activity.contract) {
		return true;
	}

	if (_activity.reason && _activity.reason.identifier === "serasaantifraude.oferta") {
		validateClientDataForSale();
		return true;
	}
    
	return true;
}

function performAttendanceSequence() {
	if (_activity.reason && _activity.reason.identifier === "serasaantifraude.cancelamento" && _activity.contract) {
		return "PERFORM_RETENTION";
	}

	if (_activity.reason && _activity.reason.identifier === "serasaantifraude.oferta") {
		return "PERFORM_SALE";
	}

	if (_activity.customerServiceAction && (_activity.customerServiceAction.identifier === "optOut" || _activity.customerServiceAction.identifier === "reOptIn" || _activity.customerServiceAction.identifier === "cadastroPositivoGenerateReport") && (_activity.documentType == null)) {
		return "DOCUMENT_AUTENTICATION";
	}

	return finalizeActivity("PERFORM_ATTENDANCE");
}

function performAttendanceEnd() {
	
	function setTabulationForMissAttendances() {
		var attendanceTypes = ["ligacaoPerdida", "engano"];

		var attClass = _activity.attendanceClassification;

		if (attClass && attClass.length && attClass[0].type && attendanceTypes.indexOf(attClass[0].type) >= 0) {
			_activity.prodServ = crm.customerServiceTab.findByIdentifier({
					identifier: "enganoquedadeligacao"
				});
			if (attClass[0].type === 'ligacaoPerdida') {
				_activity.reason = crm.customerServiceTab.findByIdentifier({
						identifier: "enganoquedadeligacao.quedadeligacao"
					});
			} else if (attClass[0].type === 'engano') {
				_activity.reason = crm.customerServiceTab.findByIdentifier({
						identifier: "enganoquedadeligacao.engano"
					});
			}

			_activity.subReason = null;
			_activity.subSubReason = null;
		}

	}

	function generalEndActions() {
		/// Status da ação
		if (_activity.customerServiceAction && _activity.customerServiceAction.identifier !== "seeNotificationsHistory") {
			_activity.statusAction = 'aguardando';
		} else {
			_activity.statusAction = 'sucesso';
		}

		/// Reseta o SLA vencimento
		if (_activity && _activity.isSolved) {
			_activity.expiredSLA = null;
		}

		/// Verificar se Tabulação escolhida por Back Office permite seu atendimento
		if (_activity.attendanceLevel && _activity.attendanceLevel.name === "N2") {
			if (_activity.reason && _activity.reason.identifier !== "serasaantifraude.cancelamento") {
				var tabOn = _activity.subSubReason ? _activity.subSubReason :
					_activity.subReason ? _activity.subReason : _activity.reason;
				checkCompleteTabulation(tabOn);
			}
		}

		_activity.attendanceUser = _user;

		///////////////////////////////////////

		function checkCompleteTabulation(tab) {
			var hasBackOffice = false;

			if (tab.customerServiceData && tab.customerServiceData.length) {
				tab.customerServiceData.forEach(function (cSD) {
					if (cSD.level.name === "N2") {
						hasBackOffice = true;
					}
				});
			}
			if (!hasBackOffice) {
				throw "Esta tabulação não permite atendimento do Back Office.";
			}
			return;
		}
	}

	function validateCellphone() {

		validateSerasaSitePhoneChange();
		validateAFPhoneChange();

		/////////////////////////////////////////
		function validateSerasaSitePhoneChange() {
			if (_activity && _activity.customerServiceAction && _activity.customerServiceAction.identifier === "serasaConsumidorEditCellphone") {
				if (_activity.phoneAntifraude) {
				    var newPhone = _activity.phoneAntifraude;
					if (newPhone.type && newPhone.type.type && newPhone.type.type !== "Celular") {
					    throw "Telefone deve ser tipo celular.";
    				} else if (newPhone.number) {
    					if (!validatePhone(newPhone.number)) {
    						throw "Telefone celular é inválido. Formato: DDD + nove números";
    					}
    				}
				}
			}
			
			//////////////
			function validatePhone(number) {
				return crm.phone.validatePhoneNumber({
					number: number,
					isCellPhone: true
				}).success;
			}
		}

		function validateAFPhoneChange() {
			var checkAction = _activity.customerServiceAction && _activity.customerServiceAction.identifier === "changeEmailAndPhoneAntiFraude";
			var checkTab = _activity.subSubReason && _activity.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.telefonedoservico";
			if (checkAction && checkTab && _activity.phoneAntifraude) {
				var phoneAF = _activity.phoneAntifraude;
				if (phoneAF.type && phoneAF.type.type && phoneAF.type.type !== "Celular") {
					throw "Telefone AntiFraude deve ser tipo celular.";
				} else if (phoneAF.number) {
					if (!validatePhone(phoneAF.number)) {
						throw "Telefone celular é inválido. Formato: DDD + nove números";
					}
				}
			}

			//////////////
			function validatePhone(number) {
				return crm.phone.validatePhoneNumber({
					number: number,
					isCellPhone: true
				}).success;
			}
		}

	}

	function updateClientData() {
		/**
		 * Atualiza as informações de contato do cliente sendo atendido.
		 * @author Juliano Ladeira
		 */
        
        var tableIsFilled = _activity.clientContactDataTable && _activity.clientContactDataTable.length && _activity.clientContactDataTable[0] && _activity.clientContactDataTable[0].clientContactData;
		if (_activity.customer && _activity.attendanceLevel.name === "N2" && tableIsFilled) {
			var customer = crm.contact._get(_activity.customer);
			customer.phones = _activity.clientContactDataTable[0].clientContactData.phones;
			customer.emails = _activity.clientContactDataTable[0].clientContactData.emails;
			customer.addresses = _activity.clientContactDataTable[0].clientContactData.addresses;

			_activity.customer = crm.contact._update(customer);
		}
	}

	function validateDependentInclusion() {
		/**
		 * Faz as validações necessárias caso tenha inclusão de dependente.
		 * @author Danilo Silveira
		 */
        
        function getDependentInput(dependent) {
			var date = dependent.birthDate;
			var birthDate = "" + pad(date.getDate()) + pad(date.getMonth() + 1) + date.getFullYear();
    		
    		return {
    			"fullName": dependent.fullName,
    			"cpf": dependent.cpf,
    			"birthDate": birthDate,
    			"mobileNumber": dependent.mobileNumber,
    			"email": dependent.email
    		};
    	}
    	
    	function checkDependentIsAlreadyRegistered(dependent) {
            var contract = _activity.contract;
            if (contract && contract.productData && contract.productData.length &&
        		contract.productData[0].provisioningParameters &&
        		contract.productData[0].provisioningParameters.dependentsData &&
        		contract.productData[0].provisioningParameters.dependentsData.length) {
        		    
		        contract.productData[0].provisioningParameters.dependentsData.forEach(
		            function (dependentRegistered) {
		                var dCPF = null;
		                if (dependentRegistered.dependent.documents) {
                			var cpf = dependentRegistered.dependent.documents.find(
            					function (doc) {
                					return doc.type.type === 'CPF';
            				    }
            				);
                			dCPF = cpf && cpf.number ? cpf.number : null;
                		}
                		if (dCPF === dependent.cpf) {
            				throw "Dependente já está incluso no contrato.";
            			}
                		
		            }
	            );
            }
        }
        
        //////////////////////////////////////////



		if (_activity.dependentData && _activity.customerServiceAction && _activity.customerServiceAction.identifier ==='includeDependent') {
			var holderContract = _activity.contract;
			var nextCountDependents = holderContract.productData[0].provisioningParameters.dependentsData.length + 1;
			if (nextCountDependents > holderContract.product.provisioningParameters.dependentsMaxNum) {
				throw "Número máximo de dependentes atingido.";
			}

			var dependent = getDependentInput(_activity.dependentData);
			
			dependent.validationExceptions = [];
			dependent.validationExceptions.push("validateRequiredEmailConfirmation");
			dependent.validationExceptions.push("validateRequiredAddress");

			crm.contact.validateSerasaAntiFraude(dependent);
			billing.contract.validateSaleAntifraudeForContact({
				"cpf": dependent.cpf,
				"sellerId": "59c41cb7ce02720a6579490f" // Serasa Experian
			});
			
			checkDependentIsAlreadyRegistered(dependent);
		}

	}
	
	
	function fillAttendanceData(){
	    
	     if(_activity.contract){
            _activity.attendanceData.contract = _activity.contract;
        }else if(_activity.order){
            _activity.attendanceData.order = _activity.order;
        }
        
	    
        if(_activity && _activity.customerServiceAction && _activity.customerServiceAction.identifier){
            
            _activity.attendanceData.attendanceAction = _activity.customerServiceAction;
            
            if (_activity.customerServiceAction.identifier === "changeEmailAndPhoneAntiFraude") {
            
                if (_activity.subSubReason && _activity.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.emaildoservico") {
                    if (_activity.emailAntifraude) {
                        _activity.attendanceData.newEmailAntifraude = _activity.emailAntifraude;
                    }
                } else if (_activity.subSubReason && _activity.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.telefonedoservico") {
                    if (_activity.phoneAntifraude) {
                        _activity.attendanceData.newPhoneAntifraude = _activity.phoneAntifraude;
                    }
                }			
            } else if (_activity.customerServiceAction.identifier === "changePlan"){
                if(_activity.contract.product){
                    _activity.attendanceData.oldPlan = _activity.contract.product;
                }
                if(_activity.availablePlans){
                    _activity.attendanceData.newPlan = _activity.availablePlans;
                }
            } else if (_activity.customerServiceAction.identifier === "changePaymentMethod"){
                if(_activity.paymentMethod){
                    _activity.attendanceData.newPaymentMethod = _activity.paymentMethod;
                }
            } else if (_activity.customerServiceAction.identifier === "serasaConsumidorEditCellphone"){
                if (_activity.phoneAntifraude) {
                    _activity.attendanceData.newPhoneSerasaConsumidor = _activity.phoneAntifraude;
                }
            } else if(_activity.customerServiceAction.identifier === "serasaConsumidorEditEmail"){
                if (_activity.newValue) {
                    _activity.attendanceData.newEmailSerasaConsumidor = _activity.newValue;
                }
            }
	    }
        
        ///Limpa campos com resposta de chamadas ou arquivos de vizualização apenas
        if(_activity.report){
            _activity.report = null;
        }
        if(_activity.extracts){
            _activity.extracts = null;
        }
        if(_activity.informationPositiveResponse){
            _activity.informationPositiveResponse = null;
        }
    }

	///////////////////////////////

	function getInitialBalance(){
        if (_activity.customerServiceAction && _activity.customerServiceAction.identifier === "changePlan"){
            _activity.changePlanBalance = billing.contract.newPlanBalanceCalculation({
                "contract": _activity.contract,
                "product": _activity.availablePlans
            }).remainingBalance.toFixed(2);
        }
    }

	///////////////////////////////
	setTabulationForMissAttendances();
	generalEndActions();
	validateCellphone();
	updateClientData();
	validateDependentInclusion();
	fillAttendanceData();
    getInitialBalance();
	
	_activity.attendanceTab = [{
		prodServ: _activity.prodServ,
		reason: _activity.reason,
		subReason: _activity.subReason,
		subSubReason: _activity.subSubReason
	}];
}

//////////////////////////////////////////////

function documentAutenticationStart() {}

function documentAutenticationOnState() {

	if(_activity.cpf){
		if((_activity.documentType && _activity.documentNumber && _activity.pfDocument) || (_activity.documentType && _activity.documentNumber && _activity.pfDocument && _activity.documentWithoutCpf == true && _activity.cpfDocument)){
			setInstanceControlProperty("VALID_PF_DOCUMENT", true);
		}
	}else {
		if((_activity.documentType && _activity.documentNumber && _activity.pfDocument && _activity.pjDocument) || (_activity.documentType && _activity.documentNumber && _activity.pfDocument && _activity.documentWithoutCpf == true && _activity.cpfDocument && _activity.pjDocument)){
			setInstanceControlProperty("VALID_PJ_DOCUMENT", true);
		}
	}
}

function documentAutenticationEndCheck() {
    if (getInstanceControlProperty('VALID_PF_DOCUMENT') || getInstanceControlProperty('VALID__PJ_DOCUMENT')) {
        return true;
    }
}

function documentAutenticationSequence() {
	return finalizeActivity("DOCUMENT_AUTENTICATION");
}

function documentAutenticationEnd() {}

//////////////////////////////////////////////

function performRetentionStart() {

	///////// Como foi necessário a criação de campos duplicados para organização, valores devem ser equivalentes
	var retentionFields = ["retentionSubReason", "retentionSubSubReason", "retentionCustomerServiceAction"];
	var normalFields = ["subReason", "subSubReason", "customerServiceAction"];

	retentionFields.forEach(
		function (field, index) {
		_activity[field] = _activity[normalFields[index]];
	});

	retentionFields.forEach(
		function (field, index) {
		_oldObject[field] = _oldObject[normalFields[index]];
	});
	
	

}

function performRetentionOnState() {
	function resetAndSetFields() {
		//Reset dos campos
		var withheldChanged = (_oldObject.isWithheld !== null) ? (_activity.isWithheld !== _oldObject.isWithheld) : false;
		var withheldDiscChanged = (_oldObject.isWithheldDiscount !== null) ? (_activity.isWithheldDiscount !== _oldObject.isWithheldDiscount) : false;
		var subReasonChanged = checkForChange(_activity.subReason, _oldObject.subReason);
		var subSubReasonChanged = checkForChange(_activity.subSubReason, _oldObject.subSubReason);

		if (withheldChanged || withheldDiscChanged) {
			_activity.subReason = null;
			_activity.subSubReason = null;
			_activity.customerServiceAction = null;
			_activity.isSolved = null;
			_activity.customerServiceAction = null;
// 			_activity.argumentation = null;
			_activity.retentionDiscount = null;
		} else if (subReasonChanged) {
			_activity.subSubReason = null;
			_activity.customerServiceAction = null;
			_activity.isSolved = null;
		} else if (subSubReasonChanged) {
			_activity.customerServiceAction = null;
			_activity.isSolved = null;
		}

		if (_activity.isWithheld == "sim") {
			_activity.isWithheldDiscount = "sim";
		}
        
        

		function checkForChange(tab, oldTab) {
			if (tab == null && oldTab != null) {
				return true;
			}
			if (tab != null && oldTab == null) {
				return true;
			}
			if (tab != null && oldTab != null && tab.identifier !== oldTab.identifier) {
				return true;
			}
			return false;
		}

	}
	function calculateRefund() {
		/// Cálculo do reembolso
		if (_activity.customerServiceAction && _activity.customerServiceAction.identifier === "suspendService") {
			if (_activity.contract && _activity.contract.product && _activity.contract.product.prvProduct && 
			(_activity.contract.product.prvProduct._id === "59d669f5ce02723b15ac40ea" || _activity.contract.product.prvProduct._id === "5b9000d4b316393b1303593f")) {
				var refund = billing.contract.calculateRefundAmount({
						"contractId": _activity.contract._id,
						"is100percent": _activity.totalRefundCancellation
					});
				if (refund.withinSevenDaysOfPurchase) {
					_activity.totalRefundCancellation = true;
				}
				_activity.refundAmount = refund.refundValue;
			}
		}

		/// Seta o campo Ilha
		if (_activity.reason) {
			var tabOn = _activity.subSubReason ? _activity.subSubReason :
				_activity.subReason ? _activity.subReason : _activity.reason;
			checkCompleteTabulation(tabOn);
		}

		/////////////////

		function checkCompleteTabulation(tab) {
			var ilha = null;
			if (tab.customerServiceData && tab.customerServiceData.length) {
				tab.customerServiceData.forEach(function (cSD) {
					if (cSD.level.name === "N1" && cSD.ilhaSkill) {
						ilha = cSD.ilhaSkill;
					}
				});
			}
			_activity.area = ilha;
		}
	}
	
	///////// Como foi necessário a criação de campos duplicados para organização, valores devem ser equivalentes
	_activity.subReason = _activity.retentionSubReason;
	_activity.subSubReason = _activity.retentionSubSubReason;
	_activity.customerServiceAction = _activity.retentionCustomerServiceAction;

	_oldObject.subReason = _oldObject.retentionSubReason;
	_oldObject.subSubReason = _oldObject.retentionSubSubReason;
	_oldObject.customerServiceAction = _oldObject.retentionCustomerServiceAction;

	
	
        
    /////////

	resetAndSetFields();
	calculateSLAData(); ///Cálculo dos dados de SLA sempre que tabulação mudar
	calculateRefund();

	/////////
	_activity.retentionSubReason = _activity.subReason;
	_activity.retentionSubSubReason = _activity.subSubReason;
	_activity.retentionCustomerServiceAction = _activity.customerServiceAction;

	_oldObject.retentionSubReason = _oldObject.subReason;
	_oldObject.retentionSubSubReason = _oldObject.subSubReason;
	_oldObject.retentionCustomerServiceAction = _oldObject.customerServiceAction;
	/////////
	
	//////////////////////////
	var subReasons = crm.customerServiceTab._search({
            "query": {
                "bool": {
                    "must": [{
                            "term": {
                                "active": true
                            }
                        }, {
                            "term": {
                                "parentTab._id": _activity.reason._id
                            }
                        }
                    ]
                }
            }
        }).hits.hits;
        var paymentMethodId;
        // se o método de pagamento do contrato for cartão de crédito
        if (_activity.contract.paymentMethod._id == "59c41f90ce02720a65794af9") {
            paymentMethodId = "5bedca8cb316390a8f8210e6";
        } else {
            paymentMethodId = "5bedca8cb316390a8f8210dc";
        }
        
        subReasons.forEach(function (subReason){
            if (subReason._id == paymentMethodId) {
                _activity.subReason = subReason._source;
            }
        });
		
		_activity.retentionSubReason = _activity.subReason;
	
	if ((_activity.isWithheld == "sim" && _activity.argumentation) || (_activity.isWithheldDiscount == "sim" && _activity.retentionDiscount)) {
		
// 		var subReasons = crm.customerServiceTab._search({
//             "query": {
//                 "bool": {
//                     "must": [{
//                             "term": {
//                                 "active": true
//                             }
//                         }, {
//                             "term": {
//                                 "parentTab._id": _activity.reason._id
//                             }
//                         }
//                     ]
//                 }
//             }
//         }).hits.hits;
//         var paymentMethodId;
//         // se o método de pagamento do contrato for cartão de crédito
//         if (_activity.contract.paymentMethod._id == "59c41f90ce02720a65794af9") {
//             paymentMethodId = "5bedca8cb316390a8f8210e6";
//         } else {
//             paymentMethodId = "5bedca8cb316390a8f8210dc";
//         }
        
//         subReasons.forEach(function (subReason){
//             if (subReason._id == paymentMethodId) {
//                 _activity.subReason = subReason._source;
//             }
//         });
		
// 		_activity.retentionSubReason = _activity.subReason;
		
		if (_activity.subReason && _activity.subReason._id) {
		   
		    var tabsToGet = [];
			var argumentationTabs = ["serasaantifraude.cancelamento.cartaodecredito.reversaocomargumentacao", "serasaantifraude.cancelamento.boleto.reversaocomargumentacao"];
			var retentionDiscountTabs = ["serasaantifraude.cancelamento.cartaodecredito.reversaocomdesconto", "serasaantifraude.cancelamento.boleto.reversaocomdesconto"];
			if (_activity.isWithheld == "sim") {
				tabsToGet = tabsToGet.concat(argumentationTabs);
				} else if (_activity.isWithheldDiscount == "sim") {
				tabsToGet = tabsToGet.concat(retentionDiscountTabs);
			}
			
			_activity.subSubReason = crm.customerServiceTab._search({
			     "query": {
    				"bool": {
    					"must": [{
    							"term": {
    								"active": true
    							}
    						}, {
    							"term": {
    								"parentTab._id": _activity.subReason._id
    							}
    						}, {
    							"terms": {
    								"identifier.keyword": tabsToGet
    							}
    						}
    					]
    				}
			     }, "size": 1
			}).hits.hits[0]._id;
	        _activity.retentionSubSubReason = _activity.subSubReason;
		}
    }
}

function performRetentionEndCheck() {
	if (_activity.isSolved && _activity.isWithheld) {
		return true;
	}
}

function performRetentionSequence() {
	return finalizeActivity("PERFORM_RETENTION");
}

function performRetentionEnd() {
	/// Status da ação
	if (_activity.customerServiceAction && _activity.customerServiceAction.identifier !== "seeNotificationsHistory") {
		_activity.statusAction = 'aguardando';
	} else {
		_activity.statusAction = 'sucesso';
	}


    function fillAttendanceDataRetention(){
        
        if(_activity.contract){
            _activity.attendanceData.contract = _activity.contract;
        }else if(_activity.order){
            _activity.attendanceData.order = _activity.order;
        }
    
        if(_activity.customerServiceAction){
            _activity.attendanceData.attendanceAction = _activity.customerServiceAction;
        }
        
        if (_activity.customerServiceAction && _activity.customerServiceAction.identifier === "suspendService") {
            if (_activity.refundAmount !== null){
                _activity.attendanceData.refundValue = _activity.refundAmount;
            }
        }
        
    }

    fillAttendanceDataRetention();

	_activity.attendanceTab = [{
			prodServ: _activity.prodServ,
			reason: _activity.reason,
			subReason: _activity.subReason,
			subSubReason: _activity.subSubReason
		}
	];

}

//////////////////////////////////////////////

function performSaleStart() {
	
    function fillEmailContact() {
        var emails = _activity.customer.emails ? _activity.customer.emails : [];
        _activity.emailContact = [];
        
        if (emails && emails.length) {
            emails.forEach(function(email) {
                _activity.emailContact.push({
                    mainContact: false,
                    type: email.type,
                    address: email.address,
                    exhibitionType: 'afContract'
                });
            });
        } else {
            _activity.emailContact.push({
                mainContact: false,
                type: {_id: "59122c5f116a1f1dccc73472"},    //Pessoal
                address: "",
                exhibitionType: 'afContract'
            });
        }
        _activity.emailContact[0].mainContact = true;
    }

    function fillPhoneContact() {
        var phones = _activity.customer.phones ? _activity.customer.phones : [];
        _activity.phoneContact = [];
        
        if (phones && phones.length) {
            phones.forEach(function(phone) {
                _activity.phoneContact.push({
                    mainContact: false,
                    type: phone.type,
                    country: phone.country,
                    number: phone.number,
                    exhibitionType: 'afContract'
                });
            });
        } else {
            _activity.phoneContact.push({
                mainContact: false,
                type: {_id: "59124071116a1f2ac0ac1e9b"},    //Celular
                country: {_id: "590b71c2f5ca53091c89ea4e"},  //Brasil
                number: "",
                exhibitionType: 'afContract'
            });        
        }
        _activity.phoneContact[0].mainContact = true;
    }    
    
    function fillAddressContact() {
        var addresses = _activity.customer.addresses ? _activity.customer.addresses : [];
        _activity.addressContact = [];
        
        if (addresses && addresses.length) {
            addresses.forEach(function(address) {
                _activity.addressContact.push({
                    mainContact: false,
                    address: address,
                    exhibitionType: 'afContract'
                });
            });
        } else {
            _activity.addressContact.push({
                mainContact: false,
                address: {},
                exhibitionType: 'afContract'
            });        
        }
        _activity.addressContact[0].mainContact = true;
    }      
    
    //////////////////////
    
    fillPhoneContact();
    fillEmailContact();
    fillAddressContact();
    
    _activity.saleSubReason = crm.customerServiceTab.findByIdentifier({identifier: 'serasaantifraude.oferta.venda'});
    _activity.saleContract = {};
}

function performSaleOnState() {
	
	function antifraudContactDataDraft() {
	    phoneAndEmailAndAddressMainContactControl();
	}
	
	function saleDraft() {
    	function controlFields() {
    		if (_activity.saleContract) {
    			init();
    
    			if (_activity.saleContract.product && _activity.saleContract.buyer) {
    				validateProductRestrictions(_activity.saleContract.product, _activity.saleContract.buyer);
    
    				_activity.value = (_activity.saleContract.product.deal.value).toFixed(2);
    
    				if (_activity.applyAutomaticDiscount) {
    					_activity.discount = setAutomaticDiscount();
    				} else if (_oldObject.applyAutomaticDiscount) {
    				    _activity.coupon = '';
    				    _activity.discount = null;
    				}
    
    				validateCoupon(_activity.coupon);
    			}
    		}
    
    		////////////////////
    		function isCourtesy(discountDeal, discount) {
    			var discountId = discount._id;
    			var productValue = _activity.saleContract.product.deal.value;
    
    			return billing.discount.allowCourtesy({
    				discountId: discountId,
    				productValue: productValue
    			}).allowCourtesy;
    		}
    
    		function init() {
    			if (!_activity.saleContract.buyer && _activity.customer) {
    				_activity.saleContract.buyer = _activity.customer;
    			}
    
    			if (!_activity.saleContract.seller) {
    				_activity.saleContract.seller = crm.enterprise._get({
    						"_id": "59c41cb7ce02720a6579490f"
    					});
    			}
    
    			if (!_activity.saleContract.responsibleForSale) {
    				_activity.saleContract.responsibleForSale = _user;
    			}
    
                if (_activity.attendanceGroup){
    			    _activity.saleContract.saleUserAttendanceGroup = _activity.attendanceGroup;
    			}
    			
    			if (_activity.saleContract.productData && _activity.saleContract.productData.length) {
    
    				if (!_activity.saleContract.productData[0].provisioningParameters) {
    					_activity.saleContract.productData[0].provisioningParameters = {
    						"_class": "59dce250116a1f1da405a0f6",
    						"identifier": null,
    						"email": _activity.saleContract.buyer.emails[0],
    						"phone": getValidMobileNumber(_activity.saleContract.buyer.phones)
    					};
    				}
    
    				if (!(_activity.saleContract.productData[0].provisioningParameters && _activity.saleContract.productData[0].provisioningParameters.email)) {
    					_activity.saleContract.productData[0].provisioningParameters.email = _activity.saleContract.buyer.emails[0];
    				}
    
    				if (!(_activity.saleContract.productData[0].provisioningParameters && _activity.saleContract.productData[0].provisioningParameters.phone)) {
    					_activity.saleContract.productData[0].provisioningParameters.phone = getValidMobileNumber(_activity.saleContract.buyer.phones);
    				}
    			}
    			_activity.saleContract.saleDate = new Date();
    		}
    
    		function validateCoupon(couponIdentifier) {
    
    			var product = _activity.saleContract.product;
    			var discount;
    			var discountValidation = {};
    
    			if (couponIdentifier) {
    				var discountResult = billing.discount._search({
    						"query": {
    							"match": {
    								"identifier": couponIdentifier
    							}
    						},
    						"size": 1
    					}).hits.hits;
    
    				if (discountResult && discountResult.length) {
    					discount = discountResult[0]._source;
    					if (discount.discountType && discount.discountType.identifier == 'RETENTION') {
    						throw "Opa! Desconto inválido para venda.";
    					}
    					
    					if(_activity.saleContract.paymentMethod && _activity.saleContract.paymentMethod.identifier === "CREDIT_CARD" && discount.discountType && discount.discountType.identifier === 'TRIAL'){
                            if(discount.requiresPaymentData){
                                throw "Opa! Não é possível aplicar esse desconto!";
                            }
                        }
                        
    					_activity.discount = discount;
    
    					// Validar Desconto
    					var params = {};
    					params.sellerId = _activity.saleContract.seller._id;
    					params.discountId = discount._id;
    					params.productId = product._id;
    					params.clientId = _activity.saleContract.buyer._id;
    					params.transactionBaseValue = product.deal.value;
    					params.userId = _user._id;
    
    					discountValidation = billing.discount.validateDiscount(params);
    
    					if (!discountValidation.isValid) {
    						discountValidation.errors.forEach(function (error, index) {
    							if ((discountValidation.errors.length - 1) == index) {
    								throw error;
    							}
    							_utils.addErrorMessage(error);
    						});
    					}
    					var discountedValue = product.deal.value;
    					var discountDeal = discount.deal;
    
    					if (_activity.saleContract.paymentMethod && _activity.saleContract.product) {
    						if (_activity.saleContract.paymentMethod.identifier == "COURTESY" && !isCourtesy(discountDeal, discount)) {
    							throw "Opa! Forma de pagamento cortesia não pode ser aplicada a este desconto.";
    						}
    						if (_activity.saleContract.paymentMethod.identifier != "COURTESY" && isCourtesy(discountDeal, discount)) {
    							var courtesyPaymentMethod = billing.paymentMethod._search({
    									query: {
    										term: {
    											"identifier.keyword": "COURTESY"
    										}
    									}
    								}).hits.hits[0]._source;
    							_activity.saleContract.paymentMethod = courtesyPaymentMethod;
    						}
    					}
    
    					if (discountDeal && discountDeal.type == 'Simples') {
    						discountedValue = product.deal.value - discountDeal.value;
    					} else if (discountDeal && discountDeal.type == 'Percentual') {
    						discountedValue = product.deal.value * ((100 - discountDeal.value) / 100);
    					} else if (discountDeal && discountDeal.type == "Normal") {
    						var value = 0;
    						// Para o tipo Normal, é gerada uma tarifa para cada linha da tabela de preço
    						discountDeal.ratingRules.forEach(function (ratingRule) {
    							if (checkParcel(discountDeal, ratingRule)) {
    								if (ratingRule.type == "Valor fixo") {
    									// Tipo de rating sem calculo de pró-rata
    									value += ratingRule.value;
    								} else if (ratingRule.type == "Percentual") {
    									value += (ratingRule.value / 100) * product.deal.value;
    								}
    							}
    						});
    						discountedValue = product.deal.value - value;
    					}
    
    					if (discountedValue < 0) {
    						discountedValue = 0;
    					}
    
    					_activity.value = discountedValue.toFixed(2);
    				} else {
    					throw "Opa! Esta promoção não foi encontrada";
    				}
    			} else {
    				_activity.discount = null;
    			}
    		}
    
    		function validateProductRestrictions(product, buyer) {
    			// Antifraude
    			if (product && isAntifraude(product)) {
    				validateForProductCategoryAntifraude(product, buyer);
    			}
    		}
    
    		function validateForProductCategoryAntifraude(product, buyer) {
    
    			if (buyer && buyer._id) {
    
    				var resultProducts = billing.product._search({
    						"query": {
    							"match": {
    								"prvProduct._id": "59d669f5ce02723b15ac40ea"
    							}
    						},
    						"size": 100
    					});
    
    				if (resultProducts && resultProducts.hits && resultProducts.hits.total > 0) {
    					var ids = [];
    					resultProducts.hits.hits.forEach(function (hit) {
    						ids.push(hit._id);
    					});
    
    					var resultContracts = billing.contract._search({
    							"query": {
    								"bool": {
    									"must": [{
    											"terms": {
    												"contractStates.serviceState._id": ["59726354ce027221e44534fb", "5972634cce027221e44534f9"]
    											}
    										}, {
    											"terms": {
    												"product._id": ids
    											}
    										}, {
    											"term": {
    												"buyer._id": buyer._id
    											}
    										}
    									]
    								}
    							},
    							"size": 0
    						});
    
    					if (resultContracts && resultContracts.hits && resultContracts.hits.total > 0) {
    						throw "Já existe um plano AntiFraude ativo para o cliente selecionado.";
    					}
    				}
    			}
    		}
    
    		function isAntifraude(product) {
    			if (product.productCategories && product.productCategories.length) {
    				for (var i = 0; i < product.productCategories.length; i++) {
    					if (product.productCategories[i].identifier == 'ANTIFRAUDE') {
    						return true;
    					}
    				}
    			}
    			return false;
    		}
    
    		function getMonthDifference(date1, date2) {
    			var months = (date2.getFullYear() - date1.getFullYear()) * 12;
    			months -= date1.getMonth();
    			months += date2.getMonth();
    			return months;
    		}
    
    		function checkParcel(deal, ratingRule) {
    			try {
    				var beginServiceState = new Date();
    				var month = beginServiceState.getMonth() + 1;
    				var parcelValues = ratingRule.expression.split("-");
    				var beginParcel = Number(parcelValues[0]);
    				var endParcel = parcelValues[1] != "*" ? Number(parcelValues[1]) : Number.MAX_SAFE_INTEGER;
    				var periodicityValue = billing.ratingRule.getPeriodicityValue({
    						"periodicity": ratingRule.periodicity
    					}).value;
    				var beginDate = new Date(beginServiceState.getFullYear(), month - (deal.prepaid ? 1 : (1 + periodicityValue)), beginServiceState.getDate());
    				var endDate = new Date(beginDate.getTime());
    				endDate.setMonth(endDate.getMonth() + periodicityValue);
    				endDate.setSeconds(-1);
    				var months = getMonthDifference(beginServiceState, beginDate);
    				// Somente 1a fatura no caso do pre-pago
    				if (deal.prepaid && (beginParcel == 0 || beginParcel == 1)) {
    					return true;
    				}
    				// O mês bate com a periodicidade
    				if (months >= 0 && months % (periodicityValue) === 0) {
    					if (deal.type == "Por uso") {
    						return true;
    					}
    					var actualParcel = ((months / periodicityValue) + 1).toFixed(0);
    					if (actualParcel >= beginParcel && actualParcel <= endParcel) {
    						return true;
    					}
    				}
    			} catch (err) {
    				_utils.log(JSON.stringify(err));
    				throw "Expressão de regra de preço não implementada: '" + ratingRule.expression + "'.";
    			}
    			return false;
    		}
    
    		/**
    		 * Seta o desconto automático caso exista.
    		 */
    		function setAutomaticDiscount() {
    
    			var discount = null;
    
    			var automaticDiscounts = billing.discount.getAutomaticApplicationDiscount().discounts;
    
    			if (automaticDiscounts) {
    				automaticDiscounts.some(function (automaticDiscount) {
    					var discountValidation = {};
    					var params = {};
    					params.discountId = automaticDiscount._source._id;
    					params.clientId = _activity.saleContract.buyer._id;
    					params.sellerId = "59c41cb7ce02720a6579490f";
    					params.productId = _activity.saleContract.product._id;
    					params.transactionBaseValue = _activity.saleContract.product.deal.value;
    					params.userId = _user._id;
    
    					discountValidation = billing.discount.validateDiscount(params);
    					if (discountValidation && discountValidation.isValid) {
    						discount = automaticDiscount._source;
    						_activity.coupon = discount.identifier;
    						return true;
    					}
    				});
    
    			}
    
    			if (discount && discount.identifier !== _oldObject.coupon) {
    				_activity.saleContract.paymentMethod = null;
    			}
    
    			return discount;
    		}
    
    		function validatePhone(number) {
    			return crm.phone.validatePhoneNumber({
    				number: number,
    				isCellPhone: true
    			}).success;
    		}
    
    		function getValidMobileNumber(phones) {
    			var validPhone = null;
    			phones.some(function (phone) {
    				if (phone.type && phone.type.type && phone.type.type === 'Celular') {
    					validPhone = phone;
    					return true;
    				}
    				return false;
    			});
    			return validPhone;
    		}
    	}
    	function setInstallmentsValueAndReselectPaymentMethod() {
    		/***
    		 * Define o valor das parcelas
    		 * @author Danilo Silveira
    		 */
    
    		if (_activity.saleContract) {
    			var contractProduct = _activity.saleContract.product && _oldObject.saleContract.product;
    			if (contractProduct && _activity.saleContract.product._id !== _oldObject.saleContract.product._id) {
    				//_activity.saleContract.paymentMethod = null;    //Ali embaixo seta o paymentMethod
    				_activity.saleContract.paymentCondition = null;
    			}
    			var contractPaymentMethod = _activity.saleContract.paymentMethod && _oldObject.saleContract.paymentMethod;
    			if (contractPaymentMethod && _activity.saleContract.paymentMethod.identifier !== _oldObject.saleContract.paymentMethod.identifier) {
    				_activity.saleContract.paymentCondition = null;
    			}
    			if (_activity.saleContract.paymentCondition && _activity.value) {
    				_activity.installmentsValue = (_activity.value / _activity.saleContract.paymentCondition.installments).toFixed(2);
    			}
    		}
    
    		if (_activity.coupon !== _oldObject.coupon) {
    			_activity.saleContract.paymentMethod = null;
    		}
    
    		/***
    		 * Reseleciona a forma de pagamento.
    		 * @author Juliano Ladeira
    		 */
    
    		try {
    			var searchQuery = {
    				query: billing.contract.getPaymentMethodQuery({
    					discount: _activity.discount
    				}).query
    			};
    			var optionsList = billing.paymentMethod._search(searchQuery).hits.hits.map(function (hit) {
    					return hit._source
    				});
    			if (optionsList.length === 1) {
    				_activity.saleContract.paymentMethod = optionsList[0];
    			} else if (_activity.saleContract.paymentMethod === null && optionsList.some(function (paymentMethod) {
    					return _oldObject.saleContract.paymentMethod._id === paymentMethod._id;
    				})) {
    				// Se opção escolhida previamente é válida e não existe opção selecionada atualmente.
    				_activity.saleContract.paymentMethod = _oldObject.saleContract.paymentMethod;
    			}
    		} catch (e) {
    			// Nada a fazer;
    		}
    
    	}
    	function setSellerProfile() {
    		/***
    		 * Adiciona o perfil de venda do vendedor.
    		 * @author Juliano Ladeira
    		 */
    
    		if (_activity.saleContract && (_activity.saleContract.salespersonProfile == null)) {
    
                function buildUserGroupMap(user) {
                    var map = {};
                    
                    user.aclUserGroups.forEach(
                        function (userGroup) {
                            map[userGroup._id] = true;
                        }  
                    );
                    return map;
                }            
                
    			var salesperson = _activity._executor;
                var profiles = _system.userGroupProfiles.getUserGroupProfileByIdentifier({ identifier: "SALESPERSON_PROFILES" }).userGroupProfiles.userGroups;
                var profile = null;
                
                var userGroupMap = buildUserGroupMap(salesperson);
                
                profiles.some(
                    function (userGroup) {
                        if (userGroupMap[userGroup._id] === true) {
                            profile = userGroup;
                            return true;
                        } else {
                            return false;
                        }
                    }
                );
                
    			_activity.saleContract.salespersonProfile = profile;
    		}
    	}
    
    
        if (_activity.saleSubReason && _activity.saleSubReason.identifier === "serasaantifraude.oferta.venda") {
        	if (_activity.saleContract === null) {
        	    _activity.saleContract = {};
        	}
        	controlFields();
        	setInstallmentsValueAndReselectPaymentMethod();
        	setSellerProfile();
        } else {
            _activity.saleContract = null;
        }
	}
	
	function checkContactDataConfirmation () {
	    
        function updateCustomerEmail(customer) {
			customer.emails = [];
			_activity.emailContact.forEach(function (email) {
				if (email.mainContact) {
					customer.emails.unshift({
						type: email.type,
						address: email.address
					});
				} else {
					customer.emails.push({
						type: email.type,
						address: email.address
					});
				}
			});
			return customer;
        }
        
        function updateCustomerPhone(customer) {
			customer.phones = [];
			_activity.phoneContact.forEach(function (phone) {
				if (phone.mainContact) {
					customer.phones.unshift({
						type: phone.type,
						country: phone.country,
						number: phone.number
					});
				} else {
					customer.phones.push({
						type: phone.type,
						country: phone.country,
						number: phone.number
					});
				}
			});
			return customer;
        }	    
        
        function updateCustomerAddress(customer) {
            customer.addresses = [];
            _activity.addressContact.forEach(function (address) {
                if (address.mainContact) {
                    customer.addresses.unshift(address.address);
                } else {
                    customer.addresses.push(address.address);
                }
            });
            return customer;
        }
        
        ///////////////////////////////////
	    
	    if (!getInstanceControlProperty('AF_CONTACT_DATA_CONFIRMED_AND_UPDATED')) {
        	
        	if (!_activity.phoneContact || !_activity.phoneContact.length) {
        		throw 'É necessário ter um telefone celular cadastrado';
        	} else {
        	    _activity.phoneContact.some(function (phone) {
                    if (phone.mainContact && phone.type) {
                        if (phone.type.type === 'Celular') {
                            if (!crm.phone.validatePhoneNumber({ "number": phone.number, "isCellPhone": true }).success) {
                                throw "O celular é inválido. Formato: DDD + 9 números";
                            }
                        } else {
                            throw "Telefone de serviço deve ser 'Celular'.";
                        }
                    }
                });
        	}
        	if (!_activity.emailContact || !_activity.emailContact.length) {
        		throw 'É necessário ter um e-mail cadastrado';
        	}
        	if (!_activity.addressContact || !_activity.addressContact.length) {
        		throw 'É necessário ter um endereço cadastrado';
        	}
        	
        	
            var customer = crm.contact._get(_activity.customer);
            customer = updateCustomerEmail(customer);
            customer = updateCustomerPhone(customer);
            customer = updateCustomerAddress(customer);

            var errors = _process.attendance.current.customerDataValidation({
        		customer: customer
        	}).errors;
        	
            if (errors && errors.length > 0) {
            	throw errors.join('\n');
            }
            
            _activity.customer = crm.contact._update(customer);
	        setInstanceControlProperty('AF_CONTACT_DATA_CONFIRMED_AND_UPDATED', true);
	    }
	}
    
    ////////////////////////////////	
	
	if (!_activity.antifraudContactDataConfirmation) {
	    antifraudContactDataDraft();
	} else {
	    checkContactDataConfirmation();
	    saleDraft();
	}
}

function performSaleEndCheck() {
	return true;
}

function performSaleSequence() {
	return finalizeActivity("PERFORM_SALE");
}

function performSaleEnd() {
	function checkDependentData() {
		if (_activity.discount && _activity.coupon !== null && _activity.coupon !== "") {
			_activity.saleContract.productData[0].discount = [];
			_activity.saleContract.productData[0].discount.push(_activity.discount);
		}

		////////////
		/// Checar se dependentes possuem os dados corretos
		///////////

		var dCPFs = [];
		var buyerCPF = getContactCPF(_activity.saleContract.buyer._id);
		if (_activity.saleContract && _activity.saleContract.productData && _activity.saleContract.productData[0] &&
			_activity.saleContract.productData[0].provisioningParameters &&
			_activity.saleContract.productData[0].provisioningParameters.dependentsData &&
			_activity.saleContract.productData[0].provisioningParameters.dependentsData.length) {
			_activity.saleContract.productData[0].provisioningParameters.dependentsData.forEach(
				function (dependentData) {
				var dependent = getDependentInput(dependentData.dependent);

				if (buyerCPF === dependent.cpf) {
					throw "Comprador não pode ser dependente do contrato.";
				}
				if (dCPFs.indexOf(dependent.cpf) >= 0) {
					throw "Dependentes informados devem possuir CPF diferentes.";
				}
				dCPFs.push(dependent.cpf);
				dependent.validationExceptions = [];
				dependent.validationExceptions.push("validateRequiredEmailConfirmation");
				dependent.validationExceptions.push("validateRequiredAddress");

				try {
					crm.contact.validateSerasaAntiFraude(dependent);
					billing.contract.validateSaleAntifraudeForContact({
						"cpf": dependent.cpf,
						"sellerId": "59c41cb7ce02720a6579490f" // Serasa Experian
					});
				} catch (e) {
					throw dependent.fullName + " - " + e.message.split("Exception: ")[1];
				}
			});
		}

		//////////////
		function getDependentInput(dependent) {
			var fullName = dependent.name;
			var cpf = getContactCPF(dependent._id);
			var birthDate = null;
			if (dependent.dateOfBirth) {
				var date = dependent.dateOfBirth;
				birthDate = "" + pad(date.getDate()) + pad(date.getMonth() + 1) + date.getFullYear();
			}
			var mobileNumber = null;
			if (dependent.phones) {
				mobileNumber = dependent.phones.find(
						function (phone) {
						return phone.type.type == "Celular";
					});
				mobileNumber = mobileNumber.number ? mobileNumber.number : mobileNumber;
			}
			var email = null;
			if (dependent.emails) {
				email = dependent.emails[0].address;
			}
			return {
				"fullName": fullName,
				"cpf": cpf,
				"birthDate": birthDate,
				"mobileNumber": mobileNumber,
				"email": email
			};
		}

		function getContactCPF(contactId) {
			var cpf = null;
			var contact = crm.contact._get({
					"_id": contactId
				});
			if (contact.documents) {
				cpf = contact.documents.find(
						function (doc) {
						return doc.type.type === 'CPF';
					});
				return cpf.number ? cpf.number : cpf;
			} else {
				return null;
			}
		}

	}
	
	function performSale() {
		var lockString = "sellViaProcess" + (_activity.cpf ? _activity.cpf : "");
        var lock = _utils.resource.lock(lockString);
		try {
			function sendMail(contract) {
				try {
					var customer = contract.buyer;
					var date = new Date();
					var dateStr = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
					var queueItem = {
						sender: contract,
						subject: serasa.servicesConfigs.sendMail,
						body: {
							"email": contract.productData[0].provisioningParameters.email.address,
							"template": "serasa-email-v1-link-pagamento-cartao",
							"parameters": {
								"nomeCliente": customer.name,
								"dataPedido": dateStr,
								"numeroPedido": contract.code,
								"plano": contract.product.name,
								"valorDoPlano": "R$" + _activity.value.toFixed(2).replace(".", ","),
								"linkDadosCartao": getCreditCardLink(contract),
								"X_PARCELAS": getNumberOfInstallments(contract)
							}
						},
						schedulingDate: null,
						tryNow: true
					};
					_system._queueMessage.addItem(queueItem);
				} catch (err) {
					var error = err.message ? err.message : err;
					_utils.log("Erro no envio de e-mail de vendas.")
					_utils.log("ERROR: " + error)
				}
			}

			function getNumberOfInstallments(contract) {
				if (contract.paymentCondition) {
					if (contract.paymentCondition.installments) {
						return contract.paymentCondition.installments;
					}
				}
				return 1;
			}

			function getCreditCardLink(contract) {
				var servicesConfigs = serasa.servicesConfigs._get({"_id": "59dd445ace02720a06cde961"});
				var baseUrl = servicesConfigs.sydleOneBaseURL;
				return baseUrl + "creditCardManager/new-register/" + contract._id + "/" + encodeURIComponent(buildHash(contract));
			}

			function buildHash(contract) {
				var contractId = contract._id;
				var clientId = contract.buyer._id;

				var stringToHash = contractId + '_' + clientId + '_cadastroCc';

				return _utils.crypto.standardEncrypt(stringToHash);
			}

			function sendSMS(contract) {
				try {
					var customer = contract.buyer;
					var servicesConfigs = serasa.servicesConfigs._get({"_id": "59dd445ace02720a06cde961"});
					var contentType = "application/json";
					var headers = {
						"X-Api-Key": servicesConfigs.antifraudeServicesToken,
						"Authorization": servicesConfigs.antiFraudeAuthorizationToken
					};
					var target = servicesConfigs.antifraudeServicesBaseURL + "notifications/send-sms-template";
					var webTarget = _utils.connector.web(target, contentType, headers);
					var phone = "";
					phone = getPhone(customer);
					var request = {
						"partnerId": 1,
						"product": "antifraude",
						"template": 169,
						"phoneNumber": Number(phone),
						"parameters": {
							"link_max50carac": getUrlShortened(getCreditCardLink(contract))
						}
					};
					var response = webTarget.post(request);
				} catch (err) {
					// Ignorar
				}
			}

			function getPhone(customer) {
				for (var i = 0; i < customer.phones.length; i++) {
					if (customer.phones[i].type._id == '59124071116a1f2ac0ac1e9b') {
						var ddi = customer.phones[i].ddi.replace("+", "");
						return ddi + customer.phones[i].number;
					}
				}
			}

			function getUrlShortened(url) {
				var contentType = "application/json";
				var target = "https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyApvCd0D6lu0Yjn7JS8divdTwhOCZTb8sc";
				var webTarget = _utils.connector.web(target, contentType, {});
				var response = webTarget.post({
						"longUrl": url
					});
				if (response && response.id) {
					return response.id;
				} else {
					throw "Erro ao encurtar URL.";
				}
			}

			function addRemittanceError(remittance) {
				if (remittance.remittanceReturnCode) {
					throw remittance.remittanceReturnCode.name;
				} else {
					throw remittance.ReturnMessage;
				}
			}

			function activateTrial(contract) {
				// Espera que a indexação do contrato ocorra, no caso da venda e primeira cobrança ser online (cartão de crédito)
				var schedulingDate = new Date();
				schedulingDate.setMinutes(schedulingDate.getMinutes() + 2);
				// Ativa o serviço
				_system._queueMessage.addItem({
					sender: contract,
					subject: billing.contract.activateService,
					body: contract,
					schedulingDate: schedulingDate,
					tryNow: false
				});
			}

			// Pega o valor com desconto para enviar email de trial
			function getValue(contract) {
				var product = contract.product;
				var discount = null;
				if (contract.productData[0].discount) {
					discount = contract.productData[0].discount[0];
				}
				var discountedValue = product.deal.value;
				if (discount) {
					var discountDeal = discount.deal;
					if (discountDeal && discountDeal.type == 'Simples') {
						discountedValue = product.deal.value - discountDeal.value;
					} else if (discountDeal && discountDeal.type == 'Percentual') {
						discountedValue = product.deal.value * ((100 - discountDeal.value) / 100);
					} else if (discountDeal && discountDeal.type == "Normal") {
						var value = 0;
						// Para o tipo Normal, é gerada uma tarifa para cada linha da tabela de preço
						discountDeal.ratingRules.forEach(function (ratingRule) {
							if (checkParcel(discountDeal, ratingRule)) {
								if (ratingRule.type == "Valor fixo") {
									// Tipo de rating sem calculo de pró-rata
									value += ratingRule.value;
								} else if (ratingRule.type == "Percentual") {
									value += (ratingRule.value / 100) * product.deal.value;
								}
							}
						});
						discountedValue = product.deal.value - value;
					}

					if (discountedValue < 0) {
						discountedValue = 0;
					}
				}
				return (discountedValue >= 0) ? discountedValue.toFixed(2) : discountedValue;
			}

			function checkParcel(deal, ratingRule) {
				try {
					var beginServiceState = new Date();
					var month = beginServiceState.getMonth() + 1;
					var parcelValues = ratingRule.expression.split("-");
					var beginParcel = Number(parcelValues[0]);
					var endParcel = parcelValues[1] != "*" ? Number(parcelValues[1]) : Number.MAX_SAFE_INTEGER;
					var periodicityValue = billing.ratingRule.getPeriodicityValue({
							"periodicity": ratingRule.periodicity
						}).value;
					var beginDate = new Date(beginServiceState.getFullYear(), month - (deal.prepaid ? 1 : (1 + periodicityValue)), beginServiceState.getDate());
					var endDate = new Date(beginDate.getTime());
					endDate.setMonth(endDate.getMonth() + periodicityValue);
					endDate.setSeconds(-1);
					var months = getMonthDifference(beginServiceState, beginDate);
					// Somente 1a fatura no caso do pre-pago
					if (deal.prepaid && (beginParcel === 0 || beginParcel == 1)) {
						return true;
					}
					// O mês bate com a periodicidade
					if (months >= 0 && months % (periodicityValue) === 0) {
						if (deal.type == "Por uso") {
							return true;
						}
						var actualParcel = ((months / periodicityValue) + 1).toFixed(0);
						if (actualParcel >= beginParcel && actualParcel <= endParcel) {
							return true;
						}
					}
				} catch (err) {
					_utils.log(JSON.stringify(err));
					throw "Expressão de regra de preço não implementada: '" + ratingRule.expression + "'.";
				}
				return false;
			}

            function performCourtesySale(contract) {
                
                //-------------FATURAR ------------//
                var month = new Date().getMonth() + 1;
                var invoice = billing.contract.invoice({
                    "_id": contract._id,
                    "referenceMonth": (month < 10 ? '0' : '') + month + '/' + new Date().getFullYear(),
                    "firstInvoice": true
                });
                if (!invoice || !invoice._id) {      
                  lock.release();
                  throw "Erro ao faturar contrato.";
                }
    
                //-------------ARRECADAR ------------//    
                var remittance = billing.invoice.collect({"_id": invoice._id});
                if (!remittance || !remittance._id) {
                    lock.release();
                    throw "Erro ao arrecadar fatura.";
                }  
                
                if (remittance.state) {
                    if (remittance.state && remittance.state._id == "5977c11cce027227c4269e2e") {
                        //Erro de cobrança
                        addRemittanceError(remittance);
                    }
                } else if (remittance.invoiceState && remittance.invoiceState._id == "5977bfa7ce027227c4269e24") {
                    //Status success
                }
            }

			var embeddedContract = _activity.saleContract;
			var saleData = {};

			if (embeddedContract && embeddedContract._id) {
				var contract = billing.contract._create(embeddedContract);
				_activity.contract = contract;

				if (contract.paymentMethod.identifier == 'BANK_BILLET') {

					var month = new Date().getMonth() + 1;

					var invoice = billing.contract.invoice({
							"_id": contract._id,
							"referenceMonth": (month < 10 ? '0' : '') + month + '/' + new Date().getFullYear(),
							"firstInvoice": true
						});

					if (invoice && invoice._id) {

						var remittance = billing.invoice.collect({
								"_id": invoice._id,
							});

						if (remittance && remittance._id) {

							if (contract.paymentMethod.identifier == 'BANK_BILLET') {
								_output = {
									"status": "success",
									"identifier": contract.code,
									"paid": false,
									"billetInformation": {
										"value": remittance.value,
										"currency": contract.product.deal.currency.iso,
										"expiration": remittance.expirationDate,
									}
								};

								if (remittance.serasaSantanderBankSlip) {

									if (remittance.serasaSantanderBankSlip.LineDig) {
										_output.billetInformation.number = remittance.serasaSantanderBankSlip.LineDig;
									}
									if (remittance.serasaSantanderBankSlip.BankSlipPath) {
										saleData.bankSlipPath = remittance.serasaSantanderBankSlip.BankSlipPath;
										_output.billetInformation.pdf = remittance.serasaSantanderBankSlip.BankSlipPath;
									}
								}
							} else if (contract.paymentMethod.identifier == 'COURTESY') {
								if (remittance.state) {
									if (remittance.state && remittance.state._id == "5977c11cce027227c4269e2e") {
										//Erro de cobrança
										addRemittanceError(remittance);
									}
								} else if (remittance.invoiceState && remittance.invoiceState._id == "5977bfa7ce027227c4269e24") {
									_output = {
										"status": "success",
										"identifier": contract.code,
										"paid": true
									};
								}
							}

						} else {
							_utils.log("sellViaProcess: Erro ao arrecadar fatura.");
						}

					} else {
						_utils.log("sellViaProcess: Erro ao faturar contrato.");
					}
				} else if (contract.productData[0].discount && contract.productData[0].discount[0].discountType.identifier == 'TRIAL') {
					activateTrial(contract);
				} else if (contract.paymentMethod.identifier == 'COURTESY') {
				    performCourtesySale(contract);
				} else {
					sendMail(contract);
					sendSMS(contract);
				}

				saleData.contractCode = contract.code;
				saleData.product = contract.productData[0].product;
				_activity.saleData = billing.saleData._create(saleData);

			} else {
				_utils.log("sellViaProcess: Erro ao processar venda: contrato não encontrado");
			}
		} catch (e) {
			log("Erro ao realizar a venda.");
			throw e;
		}
		finally {
			lock.release();
		}

	}
	
	function setSubSubReason() {
		try {
			if (_activity.contract) {
				var product = _activity.contract.product;
				var paymentMethod = _activity.contract.paymentMethod;

				var tab = getTab(product, paymentMethod);
				_activity.saleSubSubReason = tab;
			} else {
				throw 'ERRO: Processo de Venda não encontrado.';
			}
		} catch (e) {
			var error = e.message ? e.message : e;
			_activity.errorsLog.push("Erro na função 'setSubSubReason' de Vendas: " + error);
		}

		//////////////////
		function getTab(product, paymentMethod) {

			var productIdentifier = null;
			var paymentMethodIdentifier = null;

			if (product.identifier === "ANTIFRAUDE_ANUAL" || product.identifier === "ANTIFRAUDE_ANUAL_PLANO_FAMILIA") {
				productIdentifier = "anual";
			} else if (product.identifier === "ANTIFRAUDE_MENSAL" || product.identifier === "ANTIFRAUDE_MENSAL_PLANO_FAMILIA") {
				productIdentifier = "mensal";
			} else if (product.identifier === "ANTIFRAUDE_TRIMESTRAL" || product.identifier === "ANTIFRAUDE_TRIMESTRAL_PLANO_FAMILIA") {
				productIdentifier = "trimestral";
			} else if (product.identifier === "ANTIFRAUDE_SEMESTRAL_PLANO_FAMILIA") {
				return null;
			} else {
				throw "Produto vendido não reconhecido";
			}

			if (paymentMethod.identifier === "CREDIT_CARD") {
				paymentMethodIdentifier = "cartao";
			} else if (paymentMethod.identifier === "BANK_BILLET") {
				paymentMethodIdentifier = "boleto";
			} else {
				throw "Forma de pagamento não reconhecida";
			}

			var defaultTabPath = "serasaantifraude.oferta.venda.";
			var tabIdentifier = defaultTabPath + productIdentifier + paymentMethodIdentifier;

			return crm.customerServiceTab.findByIdentifier({
				identifier: tabIdentifier
			});
		}
	}

    if (_activity.saleSubReason && _activity.saleSubReason.identifier === 'serasaantifraude.oferta.venda') {
    	checkDependentData();
    	performSale();
    	setSubSubReason();
    }
    
	_activity.isSolved = "sim";
	_activity.attendanceUser = _user;

	_activity.attendanceTab = [{
			prodServ: _activity.prodServ,
			reason: _activity.reason,
			subReason: _activity.saleSubReason,
			subSubReason: _activity.saleSubSubReason
		}
	];
}

//////////////////////////////////////////////

function terminalStateStart() {}

function terminalStateOnState() {
	_activity.canFinish = true;
}

function terminalStateEndCheck() {
	return true;
}

function terminalStateSequence() {
	return "TERMINAL_STATE";
}

function terminalStateEnd() {}

///////////////////////////////////////////

_output = _activity;
 