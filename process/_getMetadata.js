if (!_object) {
    return;
}

var showFieldIfValue = ["tabType", "URAOrigin", "URAPhone", "attendanceTab", "customerServiceAction", 
                        "order", "paymentMethod", "availablePlans", "refundAmount", "newValue",
                        "labels", "saleData", "slaData", "subject",
                        "contract", "invoices", "attendanceGroup", "cancelationData"];

                        
showFieldIfValue.forEach(function (field) {
    _metadata.fields[field].hidden = (_object[field] === null || 
                                      _object[field] === undefined || 
                                      _object[field].length === 0) ?
                                     true : false;
});


if (!_object.openAttendence) {
    _metadata.fields.attendanceClassification.hidden = false;
    _metadata.fields.description.hidden = false;
    _metadata.fields.priority.hidden = false;
    _metadata.fields.isSolved.hidden = false;
    _metadata.fields.files.hidden = false;
} else {
    _metadata.fields.openAttendence.hidden = false;
    _metadata.fields.SOSPriority.hidden = false;
}

// if (_object.prodServ && _object.prodServ.identifier === "cadastropositivo") {
//     _metadata.fields.ombudsman.hidden = false;
// }

if (_object.inconsistentAttendance) {
    _metadata.fields.inconsistentAttendance.hidden = false;
    _metadata.fields.inconsistencyTable.hidden = false;
}

if(_object.bankSlipsPaths && _object.bankSlipsPaths.length) {
    _metadata.fields.bankSlipsPaths.hidden = false;
}

if (_user && _user.aclUserGroups && _user.aclUserGroups.length) {
    var isAdministrador = false;
    _user.aclUserGroups.forEach(function (userGroup){
        if (userGroup.name == "Administrador"){
            isAdministrador = true;
        }
    });

    if (isAdministrador === true) {
    _metadata.fields.expiredSLA.hidden = false;
    } else {
        _metadata.fields.expiredSLA.hidden = true;
    }
}

if (_object.cpf) {
    _metadata.fields.responsible.hidden = true;
}

if(_object.attendanceData){
    
    if(_object.attendanceData.contract){
        _metadata.fields.contract.hidden = true;
    }
    if(_object.attendanceData.order){
        _metadata.fields.order.hidden = true;
    }
    if(_object.attendanceData.attendanceAction){
        _metadata.fields.customerServiceAction.hidden = true;
    }
    
    
    if(_object.attendanceData.newPaymentMethod){
        _metadata.fields.paymentMethod.hidden = true;
    } else if(_object.attendanceData.newPlan){
        _metadata.fields.availablePlans.hidden = true;
    } else if(_object.attendanceData.refundValue !== null){
        _metadata.fields.refundAmount.hidden = true;
    } else if(_object.attendanceData.newEmailSerasaConsumidor){
        _metadata.fields.newValue.hidden = true;
    }

}

if(_object.newValue && ['Informações Positivas','Fontes', 'Consulentes', 'Histórico'].indexOf(_object.newValue) >= 0){
    _metadata.fields.newValue.hidden = true;
}

if((_object.customerServiceAction && _object.customerServiceAction.identifier === "changePlan" && !_object.order) || 
(_object.customerServiceAction && _object.customerServiceAction.identifier !== "changePlan")){
    
    _metadata.fields.orderDetails.hidden = true;
}