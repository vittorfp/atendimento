    
function capitalize (name) {
    return name.toLowerCase().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
}
//masked cep
function maskedCep(cep){
    return cep.slice(0,2) + "." + cep.slice(2,5) + "-" + cep.slice(5,8);
}
    
//metodo reOptIn gerar arquivo
var body = null;
if(_object.cpf){
    body = {"cpf": _object.cpf};
}else if(_object.cnpj){
    body = {"cnpj": _object.cnpj};   
}

if(body && _object.attendanceData.signature){
    
    var response = serasa.cadastroPositivoIntegration.reOptIn(body);
    var response = "";
    if(response && response.success){
        var rg = "";
        _object.attendanceData.terms = [];
        var month_mapper = {
            1: 'Janeiro',
            2: 'Fevereiro',
            3: 'Março',
            4: 'Abril',
            5: 'Maio',
            6: 'Junho',
            7: 'Julho',
            8: 'Agosto',
            9: 'Setembro',
            10: 'Outubro',
            11: 'Novembro',
            12: 'Dezembro'
        }
         
        var template;
        if(body.cpf){
            template = serasa.cadastroPositivoTemplates._get({_id: "5d724fb2dd498345ac091289"}).template;
            for(var i = 0; i < _object.customer.documents.length; i++){
                if( _object.customer.documents.type === "RG")
                    rg = _object.customer.documents.number;
            }
            template = template.replace(/#NOME#/g, capitalize(_object.customer.name));
            template = template.replace('#RUA#', _object.customer.addresses[0].street);
            template = template.replace('#BAIRRO#', _object.customer.addresses[0].quarter);
            template = template.replace('#CIDADE#', _object.customer.addresses[0].city);
            template = template.replace('#UF#', _object.customer.addresses[0].state);
            template = template.replace('#CEP#', maskedCep(_object.customer.addresses[0].zipcode));
            template = template.replace('#RG#', rg);
            template = template.replace(/#CPF#/g, _object.customer.maskedCpfCnpj);
        }
        else if(body.cnpj){
            template = serasa.cadastroPositivoTemplates._get({_id: "5d724fefdd498345ac091298"}).template;
            for(var i = 0; i < _object.responsible.documents.length; i++){
                if( _object.customer.documents.type === "RG")
                    rg = _object.customer.documents.number;
            }
            template = template.replace(/#NOME#/g, capitalize(_object.responsible.name));
            template = template.replace('#RUA#', _object.responsible.addresses[0].street);
            template = template.replace('#BAIRRO#', _object.responsible.addresses[0].quarter);
            template = template.replace('#CIDADE#', _object.responsible.addresses[0].city);
            template = template.replace('#UF#', _object.responsible.addresses[0].state);
            template = template.replace('#CEP#', maskedCep(_object.responsible.addresses[0].zipcode));
            template = template.replace('#RG#', rg);
            template = template.replace(/#CPF#/g, _object.responsible.maskedCpfCnpj);
            template = template.replace('#NOME_EMPRESA#', _object.customer.companyName);
            template = template.replace('#CNPJ#', _object.customer.maskedCpfCnpj);
        }
        
        template = template.replace('#LOCAL#', "São Paulo");
        template = template.replace('#DIA#', _object._creationDate.getDate());
        template = template.replace('#MES#', month_mapper[_object._creationDate.getMonth()]);
        template = template.replace('#ANO#', _object._creationDate.getFullYear());
        template = template.replace('#ASSINATURA#', _object.attendanceData.signature);

        let arqHTML = _util._fileHelper.create({
            content: template,
            orientation: "landscape",
            targetFileName: "TermoDeReabertura.html",
            targetContentType: "text/html"
        });
        
        // conversão do html para o pdf
        _object.attendanceData.terms.push(_util._fileHelper.convert({
            file: arqHTML,
            targetFileName: "TermoDeReabertura.pdf"
        }));

        _output.attendanceData = _object.attendanceData;
        _utils.addInfoMessage("O pedido de Opt In foi inserido na fila e será executado dentro de 48h.");
    }else{
        throw('Erro OptIn');
    }
     
}else if(!_object.attendanceData.signature){
    throw('Consumidor não assinou o documento');
}
