﻿namespace pocSerasaSydleTests.Model
{
    public class OptInResponse
    {
        public bool optin { get; set; }
    }

    public class ConsultaStatusCadastroPositivoResponse
    {
        public bool success { get; set; }
        public OptInResponse response { get; set; }
    }
}
