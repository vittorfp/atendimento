using Flurl;
using Flurl.Http;
using pocSerasaSydleTests.Model;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace pocSerasaSydleTests
{
    public class EnderecoEmailTests
    {
        #region requisicao
        public async Task<ValidacaoEmailResponse> RequisicaoSerasaSydleOne(string rota, string body)
        {
            ValidacaoEmailResponse result = await "https://serasa-ecs.hom.sydle.one/api/1/main"
                .AppendPathSegment(rota)
                .WithBasicAuth("C81151A", "Lm3#kpuj")
                .WithHeader("X-Explorer-Account-Token", "serasa-dev")
                .WithHeader("Content-Type", "application/json")
                .PostStringAsync(body)
                .ReceiveJson<ValidacaoEmailResponse>();

            return result;
        }

        public async Task<FlurlHttpException> RequisicaoSerasaSydleOneStatusCode(string rota, string body, string login)
        {
            try
            {
                var result = await "https://serasa-ecs.hom.sydle.one/api/1/main"
                    .AppendPathSegment(rota)
                    .WithBasicAuth(login, "Lm3#kpuj")
                    .WithHeader("X-Explorer-Account-Token", "serasa-dev")
                    .WithHeader("Content-Type", "application/json")
                    .PostStringAsync(body);
            }
            catch (FlurlHttpException e)
            {
                return e;
            }

            return null;
        }

        #endregion

        [Fact]
        public async void SerasaSydle_ValidarEnderecoEmail_Falha401()
        {
            string rota = "_sysm/emailAddress/validateEmailAddress";
            string body = "{\"address\":\"oloquinho.meu\"}";
            string login = "C8115";
            FlurlHttpException e = await RequisicaoSerasaSydleOneStatusCode(rota, body, login);
            Assert.Equal(HttpStatusCode.Unauthorized, e.Call.Response.StatusCode);
        }

        [Fact]
        public async void SerasaSydle_ValidarEnderecoEmail_Falha404()
        {
            string rota = "_sysm/emailAddress/validateEmailAddress";
            string body = "{\"address\":\"oloquinho.meu\"}";
            string login = "C81151A";
            FlurlHttpException e = await RequisicaoSerasaSydleOneStatusCode(rota, body, login);
            Assert.Equal(HttpStatusCode.NotFound, e.Call.Response.StatusCode);
        }

        [Fact]
        public async void SerasaSydle_ValidarEnderecoEmail_Sucesso_EmailFalse()
        {
            string rota = "_system/emailAddress/validateEmailAddress";
            string body = "{\"address\":\"oloquinho.meu\"}";
            var response = await RequisicaoSerasaSydleOne(rota, body);
            Assert.False(response.success);
        }

        [Fact]
        public async void SerasaSydle_ValidarEnderecoEmail_Sucesso_EmailTrue()
        {
            string rota = "_system/emailAddress/validateEmailAddress";
            string body = "{\"address\":\"oloquinho@meu.com\"}";
            var response = await RequisicaoSerasaSydleOne(rota, body);
            Assert.True(response.success);
        }

    }
}
