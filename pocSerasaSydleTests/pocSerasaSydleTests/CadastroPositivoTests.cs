using Flurl;
using Flurl.Http;
using pocSerasaSydleTests.Model;
using System.Threading.Tasks;
using Xunit;

namespace pocSerasaSydleTests
{
    public class CadastroPositivoTests
    {
        #region requisicao
        public async Task<ConsultaStatusCadastroPositivoResponse> RequisicaoSerasaSydleOne(string rota, string body)
        {
            ConsultaStatusCadastroPositivoResponse result = await "https://serasa-ecs.hom.sydle.one/api/1/main/serasa"
                .AppendPathSegment(rota)
                .WithBasicAuth("C81151A", "Lm3#kpuj")
                .WithHeader("X-Explorer-Account-Token", "serasa-dev")
                .WithHeader("Content-Type", "application/json")
                .PostStringAsync(body)
                .ReceiveJson<ConsultaStatusCadastroPositivoResponse>();

            return result;
        }
        #endregion

        [Fact]
        public async void SerasaSydle_ConsultaStatusCadastroPositivo_Falha()
        {
            string rota = "cadastroPositivoIntegration/searchStatusInCadastroPositivo";
            string body = "{\"cpf\": \"45\"}";
            var restultado = await RequisicaoSerasaSydleOne(rota, body);
            Assert.False(restultado.success);
        }

        [Fact]
        public async void SerasaSydle_ConsultaStatusCadastroPositivo_Sucesso_OptIn_True()
        {
            string rota = "cadastroPositivoIntegration/searchStatusInCadastroPositivo";
            string body = "{\"cpf\": \"13003299638\"}";            
            var resultado = await RequisicaoSerasaSydleOne(rota, body);            
            Assert.True(resultado.success);
            Assert.True(resultado.response.optin);
        }

        [Fact]
        public async void SerasaSydle_ConsultaStatusCadastroPositivo_Sucesso_OptIn_False()
        {
            string rota = "cadastroPositivoIntegration/searchStatusInCadastroPositivo";
            string body = "{\"cpf\": \"88115781401\"}";            
            var resultado = await RequisicaoSerasaSydleOne(rota, body);            
            Assert.True(resultado.success);
            Assert.False(resultado.response.optin);
        }

        [Fact]
        public async void SerasaSydle_ConsultaStatusCadastroPositivo_Sucesso_OptIn_Null()
        {
            string rota = "cadastroPositivoIntegration/searchStatusInCadastroPositivo";
            string body = "{\"cpf\": \"90547657048\"}";            
            var resultado = await RequisicaoSerasaSydleOne(rota, body);
            Assert.True(resultado.success);
            Assert.Null(resultado.response);
        }

    }
}
