# Atendimento Serasa Consumidor

Esse repositório contem os scripts utilizados na automação do processo de Atendimento do Serasa. O objetivo é evitar perdas de código e sincronização entre varias pessoas utilizando o mesmo código.

Fluxo basico a ser seguido a cada inicio de desenvolvimento:

1. Atualizar sua versão com as alterações da origem:

```
git pull
```

2. Realizar as alterações necessárias.
3. Subrir sempre que possivel todas as alterações:

```
git commit -m 'MENSAGEM DE COMMIT'
git pull  # Busca novamente as atualizações antes de subir
git push
```

### JavaScript Lint no vscode

Para ativar o linter no vscode instale a extensão **ESLint** e utilize os seguintes comandos:

```
npm install
eslint --init
```