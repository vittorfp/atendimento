validateInput();

var viewInvoices = [];
var viewRemittances = [];
var refundAmount = 0;

// para cada pedido cancelar fatura correspondente
for (let i = 0; i < _object.ordersToCancel.length; i++) {

    //obtem faturas
    var invoicesList = billing.invoice._search({
        "query": {
            "match": {
                "order._id": _object.ordersToCancel[i]._id
            }
        }
    }).hits.hits;

    var invoices = invoicesList.map((hit) => {
        return hit._source;
    });

    // obtem remessas
    var remittances = [];
    invoices.map((invoice) => {
        var remittanceList = billing.invoice.getCancellableRemittances(invoice).remittances;
        remittanceList.map(remittance => remittances.push(remittance));
    });

    var error = false;
    var refundedFromInvoice = 0;
    // Estorna pagamento no cielo
    remittances.map((remittance) => {
        try {
            billing.remittance.cancel({
                _id: remittance._id
            });

            refundedFromInvoice += remittance.paidValue;
            viewRemittances.push(remittance);
        } catch (err) {
            error = true;
            _utils.addErrorMessage("Fatura não cancelada, falha no reembolso da remessa " + remittance._id + ": " + err);
        }
    });

    if (!error) {
        // Cancela fatura
        invoices.map((invoice) => {
            invoice = billing.invoice.cancel({
                _id: invoice._id,
                refundedAmount: refundedFromInvoice
            });
            viewInvoices.push(invoice);
        });

        // cancela pedido
        billing.order.cancel({
            _id: _object.ordersToCancel[i]._id
        });
    }
    refundAmount += refundedFromInvoice;
}

_output.cancelationData = crm.cancelationData._create({
    "orders": _object.ordersToCancel,
    "invoices": viewInvoices,
    "remittances": viewRemittances,
    "refundAmount": refundAmount
});

//////////////////////////////////////////////////////

function validateInput() {
    if (!_object.ordersToCancel)
        throw "Parâmetro obrigatório ordersToCancel não foi enviado.";

    if (!_object.ordersToCancel.length)
        throw "Parâmetro obrigatório ordersToCancel está vazio.";
}