//js
if (_pin.customerServiceAction && _pin.customerServiceAction.identifier) {
    var methodStr = "_process.attendance.current." + _pin.customerServiceAction.identifier;
    var method = eval(methodStr);
    try {
        let result = method({_id: _pin._id});
        _pin.statusAction = "sucesso";
        
        if (result.bankSlipsPaths && result.bankSlipsPaths.length) {
            _pin.attendanceData.bankSlipPath = result.bankSlipsPaths;
        } else if(result.retentionData){
            _pin.attendanceData.retentionData = result.retentionData;
        } else if(result.scheduleSuspensionDate){
            _pin.attendanceData.schedulingDate = result.scheduleSuspensionDate;
        } else if(result.cancelationData){
            _pin.cancelationData = result.cancelationData;
        } else if(result.attendanceData){
            _pin.attendanceData = result.attendanceData;
        }
    } catch (e) {
        if (e.message) {
            _pin.statusAction = "erro";
            _pin.errorsLog.push("Erro na ação: " + _pin.customerServiceAction.identifier + ". Msg: " + e.message);
        }
    }
}
