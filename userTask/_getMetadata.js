/***
 *
 * Os getMetadatas foram separados por etapa do atendimento.
 *
 * Por padrão todos os objetos devem ser ocultos e quais devem ser exibidos por etapa
 * devem ser informados no mapa de variáveis do estado
 *
 * @author Juliano Ladeira
 */

if (!(_object && _object.instanceControl)) {
	return;
}

// Mapa contendo os identificadores dos campos que devem ser exibidos por fluxo do processo.
var stateVariableMap = {
	// Variáveis comuns para todos os estados.
	__global__: {
		stateFields: ["protocol"],
		stateMethod: globalVariableCustomMetadata
	},
	PROCESS_START: {
		stateFields: [],
		stateMethod: processStartCustomMetadata
	},
	PARSE_CLIENT: {
		stateFields: ["anonymousClient", "cpf", "cnpj", "serasaConsumidorEmail"],
		stateMethod: parseClientCustomMetadata
	},
	SEEKING_CLIENT: {
		stateFields: [],
		stateMethod: seekingClientCustomMetadata
	},
	REGISTER_CUSTOMER: {
		stateFields: ["personToCreate", "createPersonTrigger"],
		stateMethod: registerCustomerCustomMetadata
	},
	REGISTER_ENTERPRISE: {
		stateFields: ["companyToCreate", "createCompanyTrigger"],
		stateMethod: registerCompanyCustomMetadata
	},
	CREATE_CUSTOMER: {
		stateFields: [],
		stateMethod: createCustomerCustomMetadata
	},
	CREATE_ENTERPRISE: {
		stateFields: [],
		stateMethod: createCompanyCustomMetadata
	},
	UPDATE_CUSTOMER: {
		stateFields: [],
		stateMethod: updateCustomerCustomMetadata
	},
	UPDATE_ENTERPRISE: {
		stateFields: [],
		stateMethod: updateCompanyCustomMetadata
	},
	ATENDIMENTO_ESCRITORIOS: {
		stateFields: ["customer", "cnpj", "third", "continue"],
		stateMethod: atendimentoEscritoriosCustomMetadata
	},
	CUSTOMER_PROCURATION: {
		stateFields: [],
		stateMethod: customerProcurationCustomMetadata
	},
	CREDIT_RATING: {
		stateFields: [],
		stateMethod: creditRatingCustomMetadata
	},
	POSITIVE_IDENTIFICATION: {
	    stateFields: ((_object.responsibleCPF || _object.responsible) ? ["responsible", "emailContact", "phoneContact", "contactDataConfirmation"] : ["customer", "emailContact", "phoneContact", "contactDataConfirmation"]),
		stateMethod: positiveIdentificationCustomMetadata
	},
	REGISTER_PHONE_CONTACT: {
		stateFields: ["customer", "openAttendanceHistory", "phoneContactForOpenAttendence", "goToAttendanceTrigger"],
		stateMethod: registerPhoneContactCustomMetadata
	},
	PARSE_RESPONSIBLE: {
	    stateFields: ["responsibleCPF"],
		stateMethod: parseResponsibleCustomMetadata
	},
	CHOOSE_RESPONSIBLE: {
		stateFields: ["createResponsibleTrigger"],
		stateMethod: chooseResponsibleCustomMetadata
	},
	PERFORM_ATTENDANCE: {
		stateFields: ["customer", "attendanceClassification", "description", "attendanceTab", "showEmailHistory", "showZendeskHistory", "files"],
		stateMethod: performAttendanceCustomMetadata
	},
	DOCUMENT_AUTENTICATION: {
		stateFields: ["documentType", "documentNumber", "pfDocument", "documentWithoutCpf"],
		stateMethod: documentAutenticationCustomMetadata
	},
	PERFORM_RETENTION: {
		stateFields: ["customer", "prodServ", "reason", "contract", "argumentation", "isWithheld"],
		stateMethod: performRetentionCustomMetadata
	},
	PERFORM_SALE: {
		stateFields: [],
		stateMethod: performSaleCustomMetadata
	},
	// DEPRECATED
	TERMINAL_STATE: {
		stateFields: ["customer"],
		stateMethod: terminalStateCustomMetadata
	}
};

var state = JSON.parse(_object.instanceControl).state;

showVariables(state);

/**************************************************/

function showVariables(state) {

	try {
		var stateVariables = stateVariableMap[state];

		if (!stateVariables) {
			throw "Variáveis do estado " + state + " não encontradas.";
		}

		stateVariables.stateFields = stateVariables.stateFields.concat(stateVariableMap.__global__.stateFields);

		stateVariables.stateFields.forEach(
			function (fieldIdentifier) {
			_metadata.fields[fieldIdentifier].hidden = false;
		});

		stateVariables.stateMethod();
	} catch (e) {
		var error = e.message ? e.message : e;
		throw error;
		/// FOR DEBUG
		//throw 'Erro ao processar metadata de estado ' + state + ': ' + error + ': ' + e.lineNumber;
	}
}

/**************************************************/

function globalVariableCustomMetadata() {}

function processStartCustomMetadata() {}

function parseClientCustomMetadata() {
    
    function userIsAtendimentoEscritorios() {
        return _user.aclUserGroups.some( (group) => {
            return (group.identifier === "officeAttendance");
        });
    }
    
    if (userIsAtendimentoEscritorios()) {
        _metadata.fields.cpf.required = true;
        _metadata.fields.cnpj.required = false;
        _metadata.fields.cnpj.hidden = true;
        _metadata.fields.serasaConsumidorEmail.required = false;
        _metadata.fields.serasaConsumidorEmail.hidden = true;
        _metadata.fields.anonymousClient.required = false;
        _metadata.fields.anonymousClient.hidden = true;
        _metadata.fields.password.hidden = false;
        
    } else {
        _metadata.fields.cpf.required = true;
        _metadata.fields.cnpj.required = true;
        _metadata.fields.serasaConsumidorEmail.required = true;
    }
	
	if (_object.anonymousClient) {
		_metadata.fields.cpf.hidden = true;
		_metadata.fields.cpf.required = false;
		_metadata.fields.cnpj.hidden = true;
        _metadata.fields.cnpj.required = false;
        _metadata.fields.serasaConsumidorEmail.hidden = true;
        _metadata.fields.serasaConsumidorEmail.required = false;
		_metadata.fields.phoneContactForOpenAttendence.hidden = true;
	}

	if (_object.cpf !== null) {
		_metadata.fields.cnpj.hidden = true;
        _metadata.fields.cnpj.required = false;
        _metadata.fields.serasaConsumidorEmail.hidden = true;
        _metadata.fields.serasaConsumidorEmail.required = false;
	}

	if (_object.cnpj !== null) {
		_metadata.fields.cpf.hidden = true;
        _metadata.fields.cpf.required = false;
        _metadata.fields.serasaConsumidorEmail.hidden = true;
        _metadata.fields.serasaConsumidorEmail.required = false;
    }
    
    if (_object.serasaConsumidorEmail !== null) {
		_metadata.fields.cnpj.hidden = true;
        _metadata.fields.cnpj.required = false;
        _metadata.fields.cpf.hidden = true;
        _metadata.fields.cpf.required = false;
    }

	if (_object.phoneContactForOpenAttendence) {
		_metadata.fields.anonymousClient.hidden = true;
	}

    var help = "<html>Solicite o <b>CPF</b> ou o <b>CNPJ</b> do consumidor para iniciar o atendimento.<br>" +
        "Caso o cliente esteja cadastrado no Serasa Consumidor, o mesmo poderá se identificar com o seu <b>endereço de email</b><br>"+
		"Caso ele não queira se identificar, marque a opção <b>'Contato não identificado'</b>.<br>" +
		"Se solicitado, informe o número do protocolo desse atendimento: <b>" + _pin.protocol + "</b>";
	if (_object.URAOrigin) {
		_metadata.editHelp = help + "<br>(O CPF informado pela URA não é válido)</html>";
	} else {
		_metadata.editHelp = help + "</html>";
	}
}

function seekingClientCustomMetadata() {
	parseClientCustomMetadata();

	stateVariableMap.SEEKING_CLIENT.stateFields.forEach(
		function (fieldIdentifier) {
		_metadata.fields[fieldIdentifier].readOnly = true;
	});

	_metadata.editHelp = 'Buscando dados do cliente...';
}

function registerCustomerCustomMetadata() {
    
    function getCustomerCreationError() {
    //Não está em uso pois faltam exemplos testes - @Danilo Silveira
        var errors = [''];
        if (_object.errorsLog && _object.errorsLog.length) {
            errors = _object.errorsLog.map(error => {
                var serasaError = error.indexOf("Serasa Consumidor");
                var receitaError = error.indexOf("Receita Federal");
                if (serasaError !== -1) {
                    return "Erro na criação/atualização do contato: " + error.slice(serasaError + 19);
                } else if (receitaError !== -1) {
                    return "Erro na criação/atualização do contato: " + error.slice(receitaError + 17);
                } else {
                    return null;
                }
            }).filter( x => {return x;});
            if (errors.length) {
                errors[0] = '<br>' + errors[0];
            }
        }
        return '<html>' + errors.join('\n') + '</html>';
    }
    
    let info = "<html><b>Documento: " + _object.cpf + "</b><br>";
    let successReceitaFederal = true;
    let sucessSerasaConsumidor = true;
    let errorCreateContactWithSerasaConsumidorSite = false;
    let serasaConsumidorUser = JSON.parse(_object.serasaConsumidorResponse);
    
    if (_object.errorsLog && _object.errorsLog.length){
        successReceitaFederal = _object.errorsLog.every( error => {
            return (error.indexOf("Erro na consulta de CPF/CNPJ pela Receita Federal:") === -1);
        });
        
        sucessSerasaConsumidor = _object.errorsLog.every( error => {
            return (error.indexOf("Erro na busca em Serasa Consumidor:") === -1);
        });
        
        errorCreateContactWithSerasaConsumidorSite = _object.errorsLog.some( error => { //Erro no DQ
            return (error.indexOf("Erro na criação de contato pelo Serasa Consumidor:") > -1);
        });
    }
    
    if(successReceitaFederal && !_object.RFWSResponse && sucessSerasaConsumidor && !_object.serasaConsumidorResponse){
        info += "<b>CPF não encontrado na Receita Federal.</b><br>"
        info += "<b>Não foi encontrado cadastro no site Serasa Consumidor com o CPF informado.</b><br>"
        
    }else if (successReceitaFederal && !_object.RFWSResponse && errorCreateContactWithSerasaConsumidorSite){
        info += "<b>CPF não encontrado na Receita Federal.</b><br>"
        info += "<b>Dados do site não OK pelo Data Quality:</b><br>"
        if (serasaConsumidorUser.name){
            info += "Nome: <b>" + serasaConsumidorUser.name + "</b><br>"   
        }
        if (serasaConsumidorUser.birth_date){
            info += "Data de nascimento: <b>" + serasaConsumidorUser.birth_date? '' + serasaConsumidorUser.birth_date.substr(8) + '/' + serasaConsumidorUser.birth_date.substr(5,2) + '/' + serasaConsumidorUser.birth_date.substr(0,4) : '' + "</b><br>"   
        }
        info += "Status: <b>" + serasaConsumidorUser.status + "</b><br>"
        
    } else if(!successReceitaFederal && errorCreateContactWithSerasaConsumidorSite){
        info += "<b>Serviço de consulta da Receita Federal INDISPONÍVEL</b><br>"
        info += "<b>Dados do site não OK pelo Data Quality:</b><br>"
        if(serasaConsumidorUser.name){
            info += "Nome: <b>" + serasaConsumidorUser.name + "</b><br>"    
        }
        if(serasaConsumidorUser.birth_date){
            info += "Data de nascimento: <b>" + serasaConsumidorUser.birth_date? '' + serasaConsumidorUser.birth_date.substr(8) + '/' + serasaConsumidorUser.birth_date.substr(5,2) + '/' + serasaConsumidorUser.birth_date.substr(0,4) : '' + "</b><br>"   
        }
        info += "Status: <b>" + serasaConsumidorUser.status + "</b><br>"
        
    } else if (successReceitaFederal && !_object.RFWSResponse && !sucessSerasaConsumidor){
        info += "<b>CPF não encontrado na Receita Federal.</b><br>"
        info += "<b>Serviço de consulta do Serasa Consumidor (site) INDISPONÍVEL</b><br>"
        
    } else if (!successReceitaFederal && !sucessSerasaConsumidor){
        info += "<b>Serviço de consulta da Receita Federal INDISPONÍVEL</b><br>"
        info += "<b>Serviço de consulta do Serasa Consumidor (site) INDISPONÍVEL</b><br>"
    } else if (!successReceitaFederal && sucessSerasaConsumidor && !_object.serasaConsumidorResponse){
        info += "<b> Serviço de consulta da Receita Federal INDISPONÍVEL</b><br>"
        info += "<b>Não foi encontrado cadastro no site Serasa Consumidor com o CPF informado.</b><br>"
    }
    
    _metadata.editHelp = info + "Realize o <b>cadastro</b> do consumidor.</html>";
    
	_metadata.fields.personToCreate.required = true;
}

function registerCompanyCustomMetadata() {
    
    function getCustomerCreationError() {
    //Não está em uso pois faltam exemplos testes - @Danilo Silveira
        var errors = [''];
        if (_object.errorsLog && _object.errorsLog.length) {
            errors = _object.errorsLog.map(error => {
                var serasaError = error.indexOf("Serasa Consumidor");
                var receitaError = error.indexOf("Receita Federal");
                if (serasaError !== -1) {
                    return "Erro na criação/atualização do contato: " + error.slice(serasaError + 19);
                } else if (receitaError !== -1) {
                    return "Erro na criação/atualização do contato: " + error.slice(receitaError + 17);
                } else {
                    return null;
                }
            }).filter( x => {return x;});
            if (errors.length) {
                errors[0] = '<br>' + errors[0];
            }
        }
        return '<html>' + errors.join('\n') + '</html>';
    }
    
    
	_metadata.editHelp = "<html>Realize o <b>cadastro</b> da empresa.</html>";

	_metadata.fields.companyToCreate.required = true;
}

function createCustomerCustomMetadata() {}

function createCompanyCustomMetadata() {}

function updateCustomerCustomMetadata() {}

function updateCompanyCustomMetadata() {}

function atendimentoEscritoriosCustomMetadata() {
    if (_object.third === true) {
        _metadata.fields.outorganteCPF.hidden = false;
        _metadata.fields.outorganteCPF.required = true;
    } else {
        _metadata.fields.outorganteCPF.hidden = true;
        _metadata.fields.outorganteCPF.required = false;
    }
}

function customerProcurationCustomMetadata() {}

function creditRatingCustomMetadata() {}

function positiveIdentificationCustomMetadata() {
    
    var contactIdentification = "";
    var contactEmailPhone = "";
    if (_object.cpf) {
    	contactIdentification = "<html>Realize a identificação positiva, confirmando o nome completo e o CPF do consumidor:<br>" +
    		"Nome: <b>" + (_object.customer.name ? _object.customer.name : "(Não encontrado)") + "</b><br>" +
    		"CPF: <b>" + (_object.cpf ? _object.cpf : "(Não encontrado)") + "</b></html>";

        
        if (!_object.customer.emails || !_object.customer.emails.length) {
            if (!_object.customer.phones || !_object.customer.phones.length) {
                contactEmailPhone += "O consumidor <b>não</b> possui dados de contato no CRM.<br>";
            } else {
                contactEmailPhone += "O consumidor <b>não</b> possui e-mail de contato no CRM.<br>";
            }
        } else if (!_object.customer.phones || !_object.customer.phones.length) {
            contactEmailPhone += "O consumidor <b>não</b> possui telefone de contato no CRM.<br>";
        }
		contactEmailPhone += "Confirme os <b>dados de contato</b> do consumidor. Informe-o que será encaminhado um e-mail com o protocolo em seu contato <b>principal</b> ao final do atendimento.</html>";
		contactEmailPhone = "<html>" + contactEmailPhone;
		
    }
    
     if (_object.cnpj) {
         
        if (_object.responsibleCPF || _object.responsible) {
        	contactIdentification = "<html>Realize a identificação positiva, confirmando o nome completo e o CPF do contato:<br>" +
        		"Nome: <b>" + (_object.responsible.name ? _object.responsible.name : "(Não encontrado)") + "</b><br>" +
        		"CPF: <b>" + (_object.responsible.maskedCpfCnpj ? _object.responsible.maskedCpfCnpj : "(Não encontrado)") + "</b></html>";
    
            
            if (!_object.responsible.emails || !_object.responsible.emails.length) {
                if (!_object.responsible.phones || !_object.responsible.phones.length) {
                    contactEmailPhone += "O consumidor <b>não</b> possui dados de contato no CRM.<br>";
                } else {
                    contactEmailPhone += "O consumidor <b>não</b> possui e-mail de contato no CRM.<br>";
                }
            } else if (!_object.responsible.phones || !_object.responsible.phones.length) {
                contactEmailPhone += "O consumidor <b>não</b> possui telefone de contato no CRM.<br>";
            }
    		contactEmailPhone += "Confirme os <b>dados de contato</b>. Informe que será encaminhado um e-mail com o protocolo em seu contato <b>principal</b> ao final do atendimento.</html>";
    		contactEmailPhone = "<html>" + contactEmailPhone;
        }
		
    }
    
    _metadata.editHelp = (contactIdentification ? contactIdentification + "<html><br><br><html>" : "") + 
                         (contactEmailPhone ? contactEmailPhone : "");
                         
}

function registerPhoneContactCustomMetadata() {

	function treatRegisterPhoneContactEvolution() {
        
        var openAttendanceNumber = _object.openAttendanceHistory.length;
        if (_object.openAttendanceHistory.length === 1) {
            openAttendanceNumber = "<html>Esse consumidor possui <b>" + openAttendanceNumber + "</b> atendimento em aberto, verifique se a ligação é referente a ele. " + 
                               "Se sim, marque a opção <b>“Contato para atendimento em aberto”</b>.<br>";
        } else {
            openAttendanceNumber = "<html>Esse consumidor possui <b>" + openAttendanceNumber + "</b> atendimentos em aberto, verifique se a ligação é referente a eles. " + 
                               "Se sim, marque a opção <b>“Contato para atendimento em aberto”</b>.<br>";
        }
        
		_metadata.editHelp = openAttendanceNumber +
			                "<br>Caso contrário, marque a opção <b>“Seguir para novo atendimento”</b>.</html>";

		if (_object.phoneContactForOpenAttendence) {
			_metadata.editHelp = "Selecione o atendimento desejado no campo <b>“Atendimento em aberto”</b>.";

			_metadata.fields.openAttendence.hidden = false;
			_metadata.fields.openAttendence.required = true;
			_metadata.fields.openAttendence.query = {
				"bool": {
					"must": [{
							"nested": {
								"path": "customer.documents",
								"query": {
									"term": {
										"customer.documents.number.keyword": _object.cpf
									}
								}
							}

						}, {
							"term": {
								"_status.identifier.keyword": "open"
							}
						}
					],
					"must_not": {
						"term": {
							"_id": _pin._id
						}
					}
				}
			};
            
			if (_object.openAttendence) {
			    var expiredSLA = false;
			    if (_object.openAttendence && _object.openAttendence.slaData && _object.openAttendence.slaData.length && _object.openAttendence.slaData[0] && _object.openAttendence.slaData[0].finalDate) {
            		var attendSLA = new Date (_object.openAttendence.slaData[0].finalDate);
            		expiredSLA = attendSLA < new Date() ? true : false;
            	}
            	
				_metadata.editHelp = "Digite as observações relacionadas ao contato do consumidor no campo <b>“Resolução”</b> e conclua o atendimento através do botão <b>“Concluir”</b>.";
				_metadata.fields.subject.hidden = false;
				_metadata.fields.subject.required = true;
				_metadata.fields.description.hidden = _object.description ? false : true;
				_metadata.fields.description.readOnly = true;
				_metadata.fields.contactType.hidden = false;
				_metadata.fields.contactType.required = true;
				
				if (_object.openAttendence && expiredSLA == true){
					_metadata.fields.SOSPriority.hidden = false;
					if (_object.openAttendence.priority === "SOS") {
						_metadata.fields.SOSPriority.readOnly = true;
						_metadata.fields.SOSPriority.name = "Atendimento já é SOS";
					}
				}
			}
		}
	}

	function treatRegisterPhoneContactDetails() {

		_metadata.fields.URAOrigin.hidden = _object.URAOrigin ? false : true;
		_metadata.fields.URAPhone.hidden = _object.URAPhone ? false : true;

		_metadata.fields.goToAttendanceTrigger.hidden = _object.phoneContactForOpenAttendence ? true : false;

		var readOnlyFields = ["customer", "receitaFederalStatus", "URAOrigin", "URAPhone"];
		readOnlyFields.forEach(function (field) {
			_metadata.fields[field].readOnly = true;
		});

		_metadata.fields.SOSPriority.required = true;

	}

	treatRegisterPhoneContactEvolution();
	treatRegisterPhoneContactDetails();
}

function parseResponsibleCustomMetadata() {
    _metadata.fields.responsibleCPF.required = true;
    
    var help = "<html><b>Dados do atendimento:</b><br>"+
        "<b>Razão Social:</b> "+_object.customer.companyName+
        "<br><b>CNPJ:</b> "+_object.customer.maskedCpfCnpj+
        "<br><br>Solicite o <b>CPF</b> do contato para iniciar o atendimento.<br>" +
        "Se solicitado, informe o número do protocolo desse atendimento: <b>" + _pin.protocol + "</b>";
	if (_object.URAOrigin) {
		_metadata.editHelp = help + "<br>(O CPF informado pela URA não é válido)</html>";
	} else {
		_metadata.editHelp = help + "</html>";
	}
	
}

function chooseResponsibleCustomMetadata() {
            
    _metadata.editHelp = "<html><b>Dados do atendimento:</b><br>"+
        "<b>Razão Social:</b> "+_object.customer.companyName+
        "<br><b>CNPJ:</b> "+_object.customer.maskedCpfCnpj+
        "<br><br>Solicite o <b>NOME</b> do contato para iniciar o atendimento.<br>" +
        "Se o contato não for cadastrado, deverá ser realizado o cadastro do mesmo.<br>"+
        "Se solicitado, informe o número do protocolo desse atendimento: <b>" + _pin.protocol + "</b></html>";
        
        
    var company = crm.enterprise.findByCNPJ({
        cnpj: _object.cnpj
    });
        
    if (company && company._id) {
        let connections = crm.connection._search({
            "query": {
                "match": {
                    "enterprise._id": company._id
                }
            }
        }).hits.hits;
        
        if (connections && connections.length ) {
            
            _metadata.fields.responsibleIdentification.required = true;
            _metadata.fields.responsibleIdentification.hidden = false;
            
            connections.forEach((conn) => {
                conn = conn._source;
                let contact = conn.contacts[0];
            
                _metadata.fields.responsibleIdentification.valueOptions.push({
                    identifier: contact._id,
                    value: contact.name
                });
                
            });
        }
                
    }
}

function performAttendanceCustomMetadata() {

	function treatServiceTabOptions() {
		//treatCustomerServiceTabOptions();
		var tabOn = _object.subSubReason ? _object.subSubReason :
			_object.subReason ? _object.subReason :
			_object.reason ? _object.reason : null;

		var isTerminal = _object.reason && _object.reason.identifier === "serasaantifraude.cancelamento";
		if (tabOn && !isTerminal) {
			checkCompleteTabulation(tabOn);
		}

		//////////////////////////////
		/* Localizado agora na classe "completeAttendanceTab - _getMetadata"
		function treatCustomerServiceTabOptions() {
		}
		 */

		function checkForNextLevel(tab) {
			var currentLevel = _object.attendanceLevel.name;
			return tab.customerServiceData.some(function (cSD) {
				if ((currentLevel === "N1" && cSD.level.name === "N2") ||
					(currentLevel === "N2" && cSD.level.name === "N3") ||
					(currentLevel === "N3" && cSD.level.name === "N4")) {

					//Impedir que grupo possa encaminhar caso o próprio atenda o próximo nível
					if (_pin.responsibleGroup && cSD.userGroups && cSD.userGroups.length) {
						return cSD.userGroups.every(
							function (uG) {
							    return _pin.responsibleGroup._id !== uG._id;
						});
					} else {
						return true;
					}
				}
			});
		}

		function checkCompleteTabulation(tab) {
			var tabCanFinish = false;
			var isThereNextLevel = false;

			if (tab.customerServiceData && tab.customerServiceData.length) {
				tabCanFinish = true;
				isThereNextLevel = checkForNextLevel(tab);
			}

			if (tabCanFinish) {
				if (tab.customerServiceAction) {
                    if (!(_object.anonymousClient || _object.backOfficeSolving)) {
						_metadata.fields.customerServiceAction.hidden = false;
					}

					var cSAIds = [];
					tab.customerServiceAction.forEach(function (action) {
						cSAIds.push(action._id);
					});
				
				
        			var search = {
    					"bool": {
    						"must": [{
    								"terms": {
    									"_id": cSAIds
    								}
    							}
    						],
    						"must_not": []
    					}
    				};
    
    				var user = JSON.parse(_object.serasaConsumidorResponse);
    			    if (user && user.status) {
    					    if(user.status === 'revalidate_blocked' || user.status === 'needs_revalidation'){
    					        search.bool.must_not.push({ "term": { "_id": "5ba246b0b3163932e01bee6b"  } }); // Ação de atendimento: Alterar email
    				            search.bool.must_not.push({ "term": { "_id": "5ba246f6b3163932e01bef3a"  } }); // Ação de atendimento: Alterar celular
    					    }
    				}
    				
    				//Trata ações do Re-OptIn/Re-OptOut
                    if(_object.prodServ.identifier === 'cadastropositivo'){
                        
                        var response = (JSON.parse(_object.cadastroPositivoResponse)).response;
                        
                        if (response && response.optin === true){
                            search.bool.must_not.push({ "term": { "_id": "5d29069dff79ab3d7949c826"  } }); // Ação de atendimento: Re-OptIn
                        }
                        if (response && response.optin === false){
                            search.bool.must_not.push({ "term": { "_id": "5d276e0dff79ab3d7947e24b"  } }); // Ação de atendimento: Re-OptOut
                        }
                        
                    }
                    
                    if(!_object.RFWSResponse && _object.attendanceLevel.name === "N1" ){
                        search.bool.must_not.push({ "term": { "_id": "5ba247bab3163932e01bf13b"  } }); // Ação de atendimento: Liberar 48 horas
                    }
                    
    				_metadata.fields.customerServiceAction.query = search;
					
					treatSolved(isThereNextLevel);
				} else {
					_metadata.fields.customerServiceAction.hidden = true;
					_metadata.fields.customerServiceAction.required = false;
					treatSolved(isThereNextLevel);
				}
			}

		}

		function treatSolved(isThereNextLevel) {
			treatValueOptions(isThereNextLevel);

			_metadata.fields.isSolved.required = true;
			_metadata.fields.isSolved.hidden = false;

			if (_object.isSolved && (_object.isSolved === "encaminhar" || _object.isSolved === "retornar")) {
            	getResponsibleGroups();
            }
		}

		function treatValueOptions(isThereNextLevel) {
// 			if (_object.attendanceLevel.name == "N1" || _object.attendanceLevel.name == "N2" || _object.backOfficeSolving) {
// 				_metadata.fields.isSolved.valueOptions.push({
// 					identifier: "sim",
// 					value: "Sim"
// 				});
// 			} else {
// 				_metadata.fields.isSolved.valueOptions.push({
// 					identifier: "simBackOffice",
// 					value: "Sim"
// 				});
// 			}
            //////////////////////////////
            var tabOn = _object.subSubReason ? _object.subSubReason :
            			_object.subReason ? _object.subReason :
                        _object.reason ? _object.reason : null;
            
            var canBackOffice = false;
            
            if (tabOn && tabOn._id && tabOn.customerServiceData && tabOn.customerServiceData.length){
                tabOn.customerServiceData.forEach(function (serviceData){
                    if(serviceData.level._id == _object.attendanceLevel._id){
                        canBackOffice = serviceData.canBackOffice;
                    }
                }); 
            }
            
            if (!canBackOffice || _object.backOfficeSolving) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "sim",
                    value: "Sim"
                });
            } else {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "simBackOffice",
                    value: "Sim"
                });
            }
            
            /////////////////////////////
			if (isThereNextLevel && !_object.backOfficeSolving && (_object.cnpj || _object.cpf)) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "encaminhar",
                    value: "Não"
                });
            } else if (_object.backOfficeSolving && (_object.cnpj || _object.cpf)) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "retornar",
                    value: "Não"
                });
            }

			if (_object.isSolved) { //if para garantir que na primeira execução do getMetadata não dê problema caso isSolved não exista nas opções.
				var exist = false;
				for (var i = 0; i < _metadata.fields.isSolved.valueOptions.length(); ++i) {
					if (_metadata.fields.isSolved.valueOptions[i].identifier == _object.isSolved) {
						exist = true;
					}
				}
				if (!exist) {
					_metadata.fields.isSolved.valueOptions.push({
						identifier: _object.isSolved,
						value: _object.isSolved
					});
				}
			}
		}

		function getResponsibleGroups() {
			var tabOn = _object.subSubReason ? _object.subSubReason :
				_object.subReason ? _object.subReason : _object.reason;

            if (_object.isSolved === "encaminhar") {
    			if (!(_object.attendanceLevel.nextLevel && tabOn.customerServiceData)) {
    				return;
    			}
            }
            
            if (_object.isSolved === "retornar") {
    			if (!(_pin.previousAttendanceLevel && tabOn.customerServiceData)) {
    				return;
    			}
            }
            
            var nextLevel;
            if (_object.isSolved === "retornar") {
                nextLevel = _pin.previousAttendanceLevel;
            } else {
                nextLevel = _object.attendanceLevel.nextLevel;
            }
			var groupOptionsId = [];
			tabOn.customerServiceData.some(
				function (cSD) {
				if (cSD.level.name == nextLevel.name) {
					if (cSD.userGroups.length > 1) {
						cSD.userGroups.forEach(function (ug) {
							groupOptionsId.push(ug._id);
						});
					}
					return true;
				}
			});
			if (groupOptionsId.length > 0) {
				_metadata.fields.responsibleGroup.query = {
					"terms": {
						"_id": groupOptionsId
					}
				};
				_metadata.fields.responsibleGroup.hidden = false;
				_metadata.fields.responsibleGroup.required = true;
				_metadata.fields.responsibleGroup.readOnly = false;
			}
		}

	}

	function treatAttendanceActions() {
		if (!_object.anonymousClient && _object.prodServ && _object.prodServ.identifier) {
			treatSeeNotificationsHistory();
			if (_object.customerServiceAction && _object.customerServiceAction.identifier) {
				if (_object.prodServ.identifier === "serasaantifraude") {
					treatAntifraude();
				} else if (_object.prodServ.identifier === "voceconsultaempresas" || _object.prodServ.identifier === "voceconsultapessoas") {
					treatVCPE();
				} else if (_object.prodServ.identifier === "serasaconsumidorsite") {
					treatSerasaConsumidor();
				} else if (_object.prodServ.identifier === "cadastropositivo"){
				    treatCadastroPositivo();
				}
			}
			if (_object.reason && _object.reason.identifier) {
				treatCancellation();
			}
		}

		////////////////////////////////////////////
		function treatAntifraude() {
			treatSendAntiFraudeTDU();
			treatResendCreditCardPaymentMail();
			treatChangePaymentMethod();
			treatRecollectOnlineBankBillet();
			treatActivateService();
			treatSendAntifraudeReport();
			treatChangeEmailAndPhoneAntiFraude();
			treatChangePlan();
			treatCancelCharge();
			/// Plano Família:
			treatIncludeDependent();
			treatRemoveDependent();
			treatSuspendDependent();
			treatResendEmail();
		}

		function treatCancellation() {
			if (_object.reason.identifier === "serasaantifraude.cancelamento") {
				showAndRequiredContractsByStates(["ACTIVE", "PENDING"]);
			}
		}

		function treatSendAntiFraudeTDU() {
			if (_object.customerServiceAction.identifier === "sendAntiFraudeTDU") {
				showAndRequiredContractsByStates(["ACTIVE", "PENDING", "SUSPENDED", "DEFAULTING_SUSPENDED"]);
			}
		}

		function treatResendCreditCardPaymentMail() {
			if (_object.customerServiceAction.identifier === "resendCreditCardPaymentMail") {
				showAndRequiredContractsByStates(["PENDING"]);
				_metadata.fields.contract.query.bool.must_not = {
					"term": {
						"product._id": "5b90189fb316393b1303b09e"
					}
				};
				_metadata.fields.contract.query.bool.must.push({
					"term": {
						"paymentMethod._id": '59c41f90ce02720a65794af9'
					}
				});
			}
		}

		function treatChangePaymentMethod() {
			if (_object.customerServiceAction.identifier === "changePaymentMethod") {
				showAndRequiredContractsByStates(["ACTIVE", "PENDING"]);
				_metadata.fields.contract.query.bool.must.push({
					"terms": {
						"product._id": ["59c41dd8ce02720a65794a0e", "59c41eefce02720a65794ab9", "5a6f61b5ce0272638fe8f894", "59c41e3ece02720a65794a57", "5b901629b316393b1303a9b7", "5b90134cb316393b13039d61", "5b9014f3b316393b1303a6c1", "5b901419b316393b1303a162", "5c4604b3b316395737280a18", "5c507ca9b316395737382e2a"]
					}
				});
				if (_object.contract && _object.contract.paymentMethod) {
					_metadata.fields.paymentMethod.hidden = false;
					_metadata.fields.paymentMethod.required = true;
					var availablePaymentMethods = _object.contract.product.paymentMethod.map(function (pm) {
							return pm.identifier;
						}).filter(function (identifier) {
							if (_object.contract.paymentMethod.identifier != identifier && identifier !== "COURTESY") {
								return true;
							}
						});

					_metadata.fields.paymentMethod.query = {
						"bool": {
							"must_not": {
								"match": {
									"identifier": _object.contract.paymentMethod.identifier
								}
							},
							"must": {
								"terms": {
									"identifier.keyword": availablePaymentMethods
								}
							}
						}
					};
				}
			}
		}

		function treatRecollectOnlineBankBillet() {
			if (_object.customerServiceAction.identifier === "recollectOnlineBankBillet") {
				showAndRequiredContractsByStates(["ACTIVE", "PENDING", "SUSPENDED", "DEFAULTING_SUSPENDED"]);
				if (_object.contract) {
					_metadata.fields.invoices.query = {
						"bool": {
							"must": [{
									"nested": {
										"path": "invoiceItem",
										"query": {
											"match": {
												"invoiceItem.contract._id": _object.contract._id
											}
										}
									}
								}, {
									"terms": {
										"invoiceState.identifier.keyword": ["EMITTED", "BILLING_ERROR"]
									}
								}
							]
						}
					};
					_metadata.fields.invoices.hidden = false;
					_metadata.fields.invoices.required = true;
				}
			}
		}

		function treatActivateService() {
			if (_object.customerServiceAction.identifier === "activateService") {
				_metadata.fields.contract.query = {
					"bool": {
						"must": [{
								"term": {
									"buyer._id": _object.customer._id
								}
							}, {
								"bool": {
									"should": [{
											"term": {
												"contractStates.serviceState.identifier.keyword": "SUSPENDED"
											}
										}, {
											"exists": {
												"field": "scheduleSuspensionDate"
											}
										}
									]
								}
							}
						]
					}
				};
				_metadata.fields.contract.hidden = false;
				_metadata.fields.contract.required = true;
			}
		}

		function treatSeeNotificationsHistory() {
			if (tabHasSeeNotificationHistory()) {
				_metadata.fields.showAntifraudeHistory.hidden = false;
				_metadata.fields.showAntifraudeHistory.required = true;
				_metadata.fields.showAntifraudeHistory.name = "Histórico de envios do relatório Antifraude";
				showAndRequiredContractsByStates(["ACTIVE"]);
				if (_object.antifraudeEmailHistory || _object.antifraudeSMSHistory) {
					_metadata.fields.showAntifraudeHistory.hidden = true;
					_metadata.fields.antifraudeEmailHistory.hidden = _object.antifraudeEmailHistory ? false : true;
					_metadata.fields.antifraudeSMSHistory.hidden = _object.antifraudeSMSHistory ? false : true;
					if (_object.contract && !_object.contract.productData[0].provisioningParameters.identifier) {
						throw 'Contrato selecionado não está provisionado.';
					}
				} else {
					_metadata.fields.showAntifraudeHistory.valueOptions.push({
						identifier: "show",
						value: "Visualizar"
					});
				}
			}
		}

		function treatSendAntifraudeReport() {
			if (_object.customerServiceAction.identifier === "sendAntifraudeReport") {
				showAndRequiredContractsByStates(["ACTIVE"]);
			}
		}

		function treatChangeEmailAndPhoneAntiFraude() {
			if (_object.customerServiceAction.identifier === "changeEmailAndPhoneAntiFraude") {
				showAndRequiredContractsByStates(["ACTIVE", "PENDING"]);
				if (_object.contract) {
					if (_object.subSubReason && _object.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.emaildoservico") {
						_metadata.fields.emailAntifraude.hidden = false;
						_metadata.fields.emailAntifraude.required = true;
					} else if (_object.subSubReason && _object.subSubReason.identifier === "serasaantifraude.dadoscadastrais.alteracaodedados.telefonedoservico") {
						_metadata.fields.phoneAntifraude.hidden = false;
						_metadata.fields.phoneAntifraude.required = true;
					}
				}
			}
		}

		function treatChangePlan() {
		    
		    if (_object.customerServiceAction.identifier === "changePlan") {
		        showAndRequiredContractsByStates(["ACTIVE"]);
		        var availables = [];
		        if (_object.contract) {
		            _metadata.fields.availablePlans.hidden = false;
					_metadata.fields.availablePlans.required = true;
					_object.contract.productData[0].product.productsAvailableMigration.forEach(function (p){
					    availables.push(p._id);
					});
					_metadata.fields.availablePlans.query = {
						"bool": {
							"must": [{
									"term": {
										"active": true
									}
								}, {
									"term": {
										"saleEnable": true
									}
								},
								{
                					"terms": {
                						"_id": availables
                					}
								}
							]
						}
					};
		        }
		    }
		}
		
		function treatCancelCharge() {
		    
		    if (_object.customerServiceAction.identifier === "cancelCharge" ) {
		        var query = billing.contract._search({
                	"query": {
                        "bool":{
                        	"must": [
                        	{
                	        	"term": {
                	            	"buyer._id": _object.customer._id
                	        	}
                        	},
                        	{
                        		"term": {
                	            	"paymentMethod._id": "59c41f90ce02720a65794af9"
                	        	}
                        	}]
                        }
                    }
    		    });
    		    if (query.hits.total > 0)
		            showAndRequiredContractsByStates(["ACTIVE"]);		        
		    }
		}

		function treatVCPE() {
			if (_object.customerServiceAction.identifier === "resendVCPEMail") {
				_metadata.fields.order.query = {
					"bool": {
						"must": [{
								"term": {
									"buyer._id": _object.customer._id
								}
							}, {
								"term": {
									"orderFinancialState._id": "5a180f4fce0272241492ae39"
								}
							}
						],
						"must_not": {
							"term": {
								"orderState._id": "5a180f9bce0272241492b2b9"
							}
						}
					}
				};
				_metadata.fields.order.required = true;
				_metadata.fields.order.hidden = false;

				if (_object.order) {
					_metadata.fields.emailAddresses.hidden = false;
					_metadata.fields.emailAddresses.minMultiplicity = 1;
					_metadata.fields.emailAddresses.maxMultiplicity = 3;
				}
			}
		}

		function treatIncludeDependent() {
			if (_object.customerServiceAction.identifier === "includeDependent") {
				showAndRequiredContractsByStates(["ACTIVE"]);
				_metadata.fields.contract.query.bool.must.push({
					"terms": {
						"product._id": ["5b901629b316393b1303a9b7", "5b90134cb316393b13039d61", "5b9014f3b316393b1303a6c1", "5b901419b316393b1303a162"]
					}
				});
				if (_object.contract && _object.contract._id != null) {
					_metadata.fields.dependentData.hidden = false;
					_metadata.fields.dependentData.required = true;
				}
			}
		}

		function treatRemoveDependent() {
			if (_object.customerServiceAction.identifier === "removeDependent") {
				showAndRequiredContractsByStates(["ACTIVE"]);
				_metadata.fields.contract.query.bool.must.push({
					"terms": {
						"product._id": ["5b901629b316393b1303a9b7", "5b90134cb316393b13039d61", "5b9014f3b316393b1303a6c1", "5b901419b316393b1303a162"]
					}
				});
				if (_object.contract && _object.contract._id != null) {
					showDependentsByHolderContract(_object.contract._id);
				}
			}

		}

		function treatSuspendDependent() {
			if (_object.customerServiceAction.identifier === "suspendDependentContract") {
				showAndRequiredContractsByStates(["ACTIVE"]);
				_metadata.fields.contract.query.bool.must.push({
					"terms": {
						"product._id": ["5b901629b316393b1303a9b7", "5b90134cb316393b13039d61", "5b9014f3b316393b1303a6c1", "5b901419b316393b1303a162"]
					}
				});
				if (_object.contract && _object.contract._id != null) {
					showDependentsContractsByContract(_object.contract._id);
				}
			}
		}
		
		function treatResendEmail() {
		    if (_object.customerServiceAction.identifier === "resendDependentInvite") {
				showAndRequiredContractsByStates(["ACTIVE"]);
				_metadata.fields.contract.query.bool.must.push({
					"terms": {
						"product._id": ["5b901629b316393b1303a9b7", "5b90134cb316393b13039d61", "5b9014f3b316393b1303a6c1", "5b901419b316393b1303a162"]   /// Plano família
					}
				});
				if (_object.contract && _object.contract._id != null) {
					if (_object.contract.productData &&
					    _object.contract.productData.length && 
					    _object.contract.productData[0].provisioningParameters && 
					    _object.contract.productData[0].provisioningParameters.dependentsData) {
					        var nonConfirmedDependents = [];
					        _object.contract.productData[0].provisioningParameters.dependentsData.forEach(
					            function (data) {
					                if (data.inviteAccepted !== true && data.serviceSuspended !== true) {
					                    nonConfirmedDependents.push(data.dependent._id);
					                }
					            }
				            );
					    }
					    
				        _metadata.fields.dependent.hidden = false;
		                _metadata.fields.dependent.required = true;
		                _metadata.fields.dependent.query = {
            				"bool": {
            					"must": [{
        							"terms": {
        								"_id": nonConfirmedDependents
        							}
        						}]
            				}
            			};
            			if (_object.dependent) {
            			    _metadata.fields.newValue.name = 'E-mail para envio';
            			    _metadata.fields.newValue.required = true;
            			    _metadata.fields.newValue.hidden = false;
            			}
				}
			}
		}

		function showAndRequiredContractsByStates(states) {
			_metadata.fields.contract.query = {
				"bool": {
					"must": [{
							"term": {
								"buyer._id": _object.customer._id
							}
						}, {
							"terms": {
								"contractStates.serviceState.identifier.keyword": states
							}
						}
					]
				}
			};
			_metadata.fields.contract.hidden = false;
			_metadata.fields.contract.required = true;
		}

		function showDependentsByHolderContract(holderContractId) {
			var dependentsIds = [];

			var holderContract = billing.contract._get({
					"_id": holderContractId
				});

			if (holderContract.productData[0].provisioningParameters.dependentsData) {
				holderContract.productData[0].provisioningParameters.dependentsData.forEach(function (data) {
					var searchQuery = {
						"query": {
							"bool": {
								"must": [{
										"term": {
											"buyer._id": data.dependent._id
										}
									}, {
										"term": {
											"originContract._id": holderContract._id
										}
									}
								]
							}
						},
						"size": 100
					};
					if (billing.contract._search(searchQuery).hits.total === 0) {
						dependentsIds.push(data.dependent._id);
					}
				});
			}

			_metadata.fields.dependent.query = {
				"bool": {
					"must": [{
							"terms": {
								"_id": dependentsIds
							}
						}
					]
				}
			};
			_metadata.fields.dependent.hidden = false;
			_metadata.fields.dependent.required = true;
		}

		function showDependentsContractsByContract(holderContractId) {
			_metadata.fields.dependentContract.query = {
				"bool": {
					"must": [{
							"term": {
								"originContract._id": holderContractId
							}
						}, {
							"term": {
								"contractStates.serviceState._id": "59726354ce027221e44534fb"   ///ATIVO
							}
						}
					]
				}
			};
			_metadata.fields.dependentContract.hidden = false;
			_metadata.fields.dependentContract.required = true;
		}

		function treatSerasaConsumidor() {
			treatEditEmail();
			treatEditCellphone();
		}

		function treatEditEmail() {
			if (_object.customerServiceAction.identifier === "serasaConsumidorEditEmail") {
				_metadata.fields.newValue.name = 'Novo e-mail';
				_metadata.fields.newValue.hidden = false;
				_metadata.fields.newValue.required = true;
			}

		}

		function treatEditCellphone() {
			if (_object.customerServiceAction.identifier === "serasaConsumidorEditCellphone") {
			    _metadata.fields.phoneAntifraude.name = 'Novo número de celular';
				_metadata.fields.phoneAntifraude.hidden = false;
				_metadata.fields.phoneAntifraude.required = true;
			}
		}

		function tabHasSeeNotificationHistory() {
			var tabOn = _object.subSubReason ? _object.subSubReason :
				_object.subReason ? _object.subReason :
				_object.reason ? _object.reason : null;
			if (tabOn && tabOn.customerServiceAction) {
				return tabOn.customerServiceAction.some(function (cSA) {
					if (cSA.identifier === "seeNotificationsHistory") {
						return true;
					}
					return false;
				});
			}
		}
		
		function treatCadastroPositivo(){
		    
		    function treatGenerateReport(){
    	       if (_object.customerServiceAction.identifier === "cadastroPositivoGenerateReport"){
    	    	    _metadata.fields.newValue.required = true;
                    _metadata.fields.newValue.name = "Tipo do Relatório"
    
                    if(!(_object.attendanceData && _object.attendanceData.positiveInformationsReport)){
                        _metadata.fields.newValue.valueOptions.push(
            	        {
        					identifier: "Informações Positivas",
        					value: "Informações Positivas"
        				});
        				_metadata.fields.newValue.hidden = false;
                    }else {
                        _metadata.fields.positiveInformations.hidden = false;
                        _metadata.fields.positiveInformations.maxMultiplicity = _object.positiveInformations.length;
                        _metadata.fields.positiveInformations.minMultiplicity = _object.positiveInformations.length;
                    }
                    if(!(_object.attendanceData && _object.attendanceData.fontsReport)){
        	    	    _metadata.fields.newValue.valueOptions.push(
        	    	        {
        						identifier: "Fontes",
        						value: "Fontes"
        					});
        					_metadata.fields.newValue.hidden = false;
                    }
                    if(! (_object.attendanceData && _object.attendanceData.consulentsReport)){
                        _metadata.fields.newValue.valueOptions.push(
        	    	        {
        						identifier: "Consulentes",
        						value: "Consulentes"
        					});
        					_metadata.fields.newValue.hidden = false;
                    }
                    if(! (_object.attendanceData && _object.attendanceData.historyReport)){
        				_metadata.fields.newValue.valueOptions.push(
        	    	        {
        						identifier: "Histórico",
        						value: "Histórico"
        					});
        					_metadata.fields.newValue.hidden = false;
                    }
                    
                    if(_object.report && _object.report.length && _object.report.length > 0){
    				    _metadata.fields.newValue.required = false;
    				    _metadata.fields.extracts.hidden = false;
    				    _metadata.fields.extracts.name = 'Exratos';
    				}

    				if(_object.report && _object.report.length && _object.report.length > 0){
    				    _metadata.fields.newValue.required = false;
    				    _metadata.fields.report.hidden = false;
    				    _metadata.fields.report.name = 'Relatórios';
    				}
    				
    				if(_object.protocols && _object.protocols.length && _object.protocols.length > 0){
    				    _metadata.fields.newValue.required = false;
    				    _metadata.fields.protocols.hidden = false;
    				    _metadata.fields.protocols.name = 'Protocolos';
    				}
    	    	}
            }
            
            treatGenerateReport();
            
		}
		
	}

	function treatAttendanceEvolution() {

        function formatDate(date) {
		    return date.toISOString().slice(0, 10).split('-').reverse().join('/');
		}
		
    	function putNewLine(array) {
    	    return array.filter(function(elem) {
    	        return elem;
    	    }).map(function(elem){
    	        return "<html>" + elem + "<br></html>";
    	    }).join('');
    	}
    	
		// Usada em N1 e N2
		function setTabInformation() {
            
            function serasaConsumidorSiteTabulation(){
                if(_object.attendanceLevel.name === "N1"){
                    if(!_object.RFWSResponse){
                        help = "<html><b> Cadastro do consumidor não confirmado na Receita Federal, alterações indisponíveis." +
                                "<br> Como não foi possível confirmar o cadastro do consumidor na Receita Federal, a solicitação" +
                                " deverá ser encaminhada ao BKO para atualização. </b><br><br></html>";
                    
                        _metadata.fields.customerServiceAction.hidden = true;
                    } 
                    
                    if(!_object.serasaConsumidorResponse){
                        help = "<html><b> Cliente não cadastrado no Serasa Consumidor. Ação Indisponível! </b><br><br></html>";
                    
                        _metadata.fields.customerServiceAction.hidden = true;
                    } 
                    
                    if(_object.RFWSResponse && _object.serasaConsumidorResponse){
                        let responseRF = JSON.parse(_object.RFWSResponse);
                        let responseSC = JSON.parse(_object.serasaConsumidorResponse);
                        if((responseRF.NomeCliente != responseSC.name) || (responseRF.DataNascimento != responseSC.birth_date)){
                            help = "<html><b> Os dados do cliente estão diferentes nas bases da Receita Federal e Serasa Consumidor. Selecione a ação \"Atualizar Cadastro\" para sincronizar as bases. </b><br><br></html>";
                        } else {
                            help = "<html><b> Os dados do cliente estão em concordância nas bases da Receita Federal e Serasa Consumidor. Ação de atendimento indisponível. </b><br><br></html>";
                            _metadata.fields.customerServiceAction.hidden = true;
                        }
                    }
                } else if (_object.attendanceLevel.name === "N2"){
                    if(!_object.serasaConsumidorResponse){
                        help = "<html><b> Cliente não cadastrado no Serasa Consumidor. Ação Indisponível! </b><br><br></html>";
                        _metadata.fields.customerServiceAction.hidden = true;
                    } else {
                        help = "<html><b> Selecione a ação \"Atualizar Cadastro\" e insira os novos dados para o nome e data de nascimento do consumidor. <br> Caso queira manter o nome ou a data de nascimento com o valor atual apenas deixe o campo em branco. </b><br><br></html>";
                        _metadata.fields.birthDate.hidden = true;
                        if(_object.customerServiceAction && _object.customerServiceAction.identifier === "updateUserRegistration"){
                            _metadata.fields.newValue.hidden = false;
                            _metadata.fields.newValue.name = "Novo nome";
                            
                            _metadata.fields.birthDate.hidden = false;
                            _metadata.fields.birthDate.name = "Nova data de nascimento";
                        }
                    }
                }
			}

            function serasaConsumidorTabulation() {
                var user = JSON.parse(_object.serasaConsumidorResponse);
                var RFResponse = JSON.parse(_object.RFWSResponse);
            
                let info;
                let sucessSerasaConsumidor = true;
                
                if (_object.errorsLog && _object.errorsLog.length){ 
                    sucessSerasaConsumidor = _object.errorsLog.every( error => {
                        return (error.indexOf("Erro na busca em Serasa Consumidor:") === -1);
                    });
                }
            
                if (!sucessSerasaConsumidor){
                    info = "<b>Serviço de consulta do Serasa Consumidor (site) INDISPONÍVEL</b><br>"
                } else if(sucessSerasaConsumidor && !_object.serasaConsumidorResponse){
                    info = "<b>Consumidor não cadastrado no Serasa Consumidor (site)</b><br>"
                } else {
                    
                    info = "<b>Dados do cadastro (site):</b><br>"
                    if(!RFResponse || (user.name !== RFResponse.NomeCliente)){
                        if (user.name){
                            info += "Nome: <b>" + user.name + "</b><br>"   
                        }
                    }
            
                    let birthDate = (RFResponse && RFResponse.DataNascimento)? RFResponse.DataNascimento : null;
                    if (birthDate){
                        if(birthDate.length === 7){
                            birthDate = '0' + birthDate;
                        }
                        birthDate = birthDate.substring(4) + '-' + birthDate.substring(2,4) + '-' +  birthDate.substring(0,2);   
                    }
                    if (!birthDate || (birthDate !== user.birth_date)){

                        if (user.birth_date){
                            info += "Data de nascimento: <b>" + (user.birth_date? '' + user.birth_date.substr(8) + '/' + user.birth_date.substr(5,2) + '/' + user.birth_date.substr(0,4) : '') + "</b><br>"    
                        }
                    }
                    if(user.email){
                        info += "E-mail: <b>" + user.email + "</b><br>"
                    }
                    if(user.cellphone){
                        info += "Telefone: <b>" + user.cellphone + "</b><br>"
                    }
                    info += ("Status: " + returnStatus(user));
            
                }
                
                if(_object.attendanceLevel.name === "N1" && _object.subSubReason && _object.subSubReason.identifier === "serasaconsumidorsite.acesso.dificuldadedeacesso.erro48horas" && !_object.RFWSResponse){
                    info += '<br><b>Cadastro do consumidor não confirmado na Receita Federal, ação de desbloqueio indisponível.</b><br>';
                    _metadata.fields.customerServiceAction.hidden = true;
                }
                
                help += '<html>' + info + '</html>';
            
                function returnStatus (user){
                    switch (user.status) {
                        case 'active':
                            if (!user.is_blocked_by) {
                                return "<b>ativo</b><br>";
                            } else if (user.is_blocked_by === 'login') {
                                return "<b>bloqueado temporariamente (tentativas fracassadas de login)</b><br>";
                            } else if (user.is_blocked_by === 'account_recovery') {
                                return "<b>bloqueado temporariamente (falha no fluxo de recuperação de login)</b><br>";
                            }
                            break;
                        case 'needs_approved':
                            return "<b>bloqueio de 48 horas</b><br>";
                        case 'light_signup':
                            return "<b>excluído</b><br>";
                        case 'pending':
                            return "<b>pendente</b><br>";
                        case 'revalidate_blocked':
                            return "<b>bloqueado (cadastro rejeitado)</b><br>Orientar o consumidor a entrar na Central de Ajuda no artigo <b> Houve um problema no seu Cadastro.</b><br>";
                        case 'needs_revalidation':
                            return "<b>bloqueado (precisa ser revalidado)</b><br>Orientar o consumidor a realizar o login no site para revalidação.<br>";
                        default:
                            return user.status;
                    }
                }
            }
			
			function antifraudeContractTabulation() {
                var afContract = _object.antifraudeContract;
                if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'SUSPENDED') {
                    help = "<html>Identificamos que o consumidor possui um contrato <b>suspenso</b> do <b>Antifraude</b>.<br>" + 
            	                     "Aproveite e explique as vantagens em <b>reativar</b> o serviço.<br></html>";
                } else if (afContract.paymentMethod && afContract.paymentMethod.identifier === 'BANK_BILLET') {
                    var remittance = _object.afRemittance;
                    if (remittance) {
                        if (remittance.state && remittance.state.identifier === "EMITTED") {
                    	    if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'ACTIVE') {
                    	        if (remittance.expirationDate > new Date()) {
                    	            help = "<html>Identificamos que o consumidor é um cliente <b>Antifraude</b> e tem um boleto de " + 
                    	                             "<b>R$" + remittance.value.toFixed(2) + "</b> com vencimento em <b>" + formatDate(remittance.expirationDate) + "</b> em aberto.<br></html>";
                    	        } else {
                                    help = "<html>Identificamos que o consumidor é um cliente <b>Antifraude</b> e tem um boleto de " + 
                    	                             "<b>R$" + remittance.value.toFixed(2) + "</b> <u>vencido</u> em <b>" + formatDate(remittance.expirationDate) + "</b> em aberto.<br></html>";                	            
                    	        }
                    		} else if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'PENDING') {
                    		    if (remittance.expirationDate > new Date()) {
                    	            help = "<html>Identificamos que o consumidor realizou uma compra do <b>Antifraude</b> e está com um boleto de " + 
                    	                             "<b>R$" + remittance.value.toFixed(2) + "</b> com vencimento em <b>" + formatDate(remittance.expirationDate) + "</b> em aberto.<br></html>";
                    	        } else {
                                    help = "<html>Identificamos que o consumidor realizou uma compra do <b>Antifraude</b> e está com um boleto de " + 
                    	                             "<b>R$" + remittance.value.toFixed(2) + "</b> <u>vencido</u> em <b>" + formatDate(remittance.expirationDate) + "</b> em aberto.<br></html>";
                    	        }
                    		}
                        } else if (remittance.state && remittance.state.identifier === "PAID") {
                            if (remittance.value > remittance.paidValue) {
                                help = "<html>Identificamos que o consumidor é um cliente <b>Antifraude</b> e realizou um pagamento no valor de " + 
                    	                             "<b>R$" + remittance.paidValue.toFixed(2) + "</b>, menor que o valor do boleto de <b>R$" + remittance.value.toFixed(2) + "</b>.<br></html>";
                            } else if (remittance.value < remittance.paidValue) {
                                help = "<html>Identificamos que o consumidor é um cliente <b>Antifraude</b> e realizou um pagamento no valor de " + 
                    	                             "<b>R$" + remittance.paidValue.toFixed(2) + "</b>, maior que o valor do boleto de <b>R$" + remittance.value.toFixed(2) + "</b>.<br></html>";
                            }
                        }
                    }
            	} else if (afContract.paymentMethod && afContract.paymentMethod.identifier === 'CREDIT_CARD') {
            	    if (afContract.contractStates && afContract.contractStates.serviceState.identifier === 'PENDING') {
            	        help = "<html>Identificamos que o consumidor realizou uma compra do <b>Antifraude</b>, porém ainda não informou os dados do <b>cartão de crédito</b>.<br></html>";
            	    }
            	} 
        	    var serviceStateIdentifier = afContract.contractStates && afContract.contractStates.serviceState && afContract.contractStates.serviceState.identifier;
            	if (serviceStateIdentifier === 'ACTIVE' || serviceStateIdentifier === 'PENDING') {
            	    var provParams = afContract.productData && afContract.productData.length && afContract.productData[0].provisioningParameters;
            	    help = help ? help : "<html>Identificamos que o consumidor é um cliente <b>Antifraude</b>. </html>";
            	    help += "<html>O e-mail e o telefone cadastrados no serviço são:<br>" + 
    	                             "E-mail: <b>" + (provParams.email && provParams.email.address ? provParams.email.address : "(Não encontrado)") + "</b><br>" + 
    	                             "Telefone: <b>" + (provParams.phone && provParams.phone.number ? provParams.phone.number : "(Não encontrado)") + "</b><br></html>";
    	            
					if (_object.contract && 
                        _object.availablePlans && 
                        _object.customerServiceAction && 
                        _object.customerServiceAction.identifier === "changePlan"){
                        
                        var balance = null;
                        try {
                            balance = billing.contract.newPlanBalanceCalculation({
                                "contract": _object.contract,
                                "product": _object.availablePlans
                            });
                        } catch (e) {
                            balance = null;
                        }
                        
                        if (balance){
                    
                            let contractPeriodicy;
                            let productPeriodicy;
                            
                            if (_object.contract.productData && _object.contract.productData.length && _object.contract.productData[0].deal && _object.contract.productData[0].deal.ratingRules &&
                                _object.contract.productData[0].deal.ratingRules.length && _object.contract.productData[0].deal.ratingRules[0].periodicity) {
                                contractPeriodicy = _object.contract.productData[0].deal.ratingRules[0].periodicity;
                            }
                            
                            if (_object.availablePlans.deal && _object.availablePlans.deal.ratingRules && _object.availablePlans.deal.ratingRules.length && _object.availablePlans.deal.ratingRules[0].periodicity) {
                                productPeriodicy = _object.availablePlans.deal.ratingRules[0].periodicity;
                            }
                            
                            /////////////////////////
                            var periodEnd = new Date(_object.contract.paidPeriodEnd);
                            if (_object.contract.paidPeriodEnd && periodEnd < new Date()){
                        
                                if (_object.contract.paymentMethod && _object.contract.paymentMethod._id == "59c41f90ce02720a65794af9"){ // se a forma de pagamento for cartão de crédito
                                    let creditNumber;
                                    if (_object.contract.accountPaymentData && _object.contract.accountPaymentData.accountCreditCardData && _object.contract.accountPaymentData.accountCreditCardData.number) {
                                        creditNumber = _object.contract.accountPaymentData.accountCreditCardData.number.maskedValue;
                                    } else {
                                        creditNumber = "(não encontrado)";
                                    }
                        
                                    help += "<html><br> Informar para o consumidor que será lançado para pagamento o valor de <b>R$"+balance.remainingBalance.toFixed(2)+
                                    "</b> referente ao proporcional do novo plano. Este valor será cobrado de uma unica vez (não parcelado) no cartão atual do cliente final "+creditNumber+
                                    ". A migração para o plano <b>"+_object.availablePlans.name+"</b> acontecerá assim que confirmado o pagamento pela operadora de cartão de crédito.</html>";
                                } else if (_object.contract.paymentMethod && _object.contract.paymentMethod._id == "59c41fadce02720a65794b10"){ // se a forma de pagamento for boleto eletronico
                                    
                                    let expirationDate = new Date();
                                    expirationDate.setDate(expirationDate.getDate()+6);
                        
                                    help += "<html><br> Informar para o consumidor que será lançado para pagamento por boleto o valor de <b>R$"+balance.remainingBalance.toFixed(2)+
                                    "</b> referente ao proporcional do novo plano, com vencimento em <b>"+formatDate(expirationDate)+"</b>. A migração para o plano <b>"+_object.availablePlans.name+
                                    "</b> acontecerá assim que confirmado o pagamento do boleto.</html>";
                                }
                        
                            } else if (contractPeriodicy == "Mensal" || productPeriodicy == "Mensal") {
                        
                                let nextBD;
                                let chagePlanDate = new Date();
                                if (chagePlanDate.getDate() < _object.contract.billingDays.billingDay) {
                                    nextBD = new Date (chagePlanDate.getFullYear(), chagePlanDate.getUTCMonth(), _object.contract.billingDays.billingDay, 0 , 0, 0);
                                } else {
                                    nextBD = new Date (chagePlanDate.getFullYear(), (chagePlanDate.getUTCMonth()+1), _object.contract.billingDays.billingDay, 0 , 0, 0);
                                }
                        
                                if(balance.remainingBalance > 0){
                                    if (balance.remainingBalance > _object.availablePlans.deal.value) {
                                        help += "<html><br> Informar para o consumidor que a migração de plano acontecerá neste momento e será concedido um crédito no valor total de <b>R$"+balance.remainingBalance.toFixed(2)+
                                        "</b> devido ao plano atual. Nos meses seguintes esse saldo credor será abatido com a mensalidade do plano atual e quando o saldo for totalmente consumido, "+
                                        "as demais já virão com o valor do novo plano <b>"+_object.availablePlans.name+"</b>, no valor de <b>R$"+_object.availablePlans.deal.value.toFixed(2)+"</b>.</html>";
                                    } else {
                                        help += "<html><br> Informar para o consumidor que a migração de plano acontecerá neste momento e será concedido um desconto de <b>R$"+balance.remainingBalance.toFixed(2)+"</b> em <b>"+formatDate(nextBD)+
                                        "</b> (próxima fatura do cliente) devido ao crédito remanescente do plano atual. As demais faturas já serão emitidas com o valor do novo plano <b>"+_object.availablePlans.name+
                                        "</b>, no valor de <b>R$"+_object.availablePlans.deal.value.toFixed(2)+"</b>.</html>";
                                    }
                                } else if(balance.remainingBalance < 0){
                                    balance.remainingBalance = - balance.remainingBalance;
                                    help += "<html><br> Informar para o consumidor que a migração de plano acontecerá neste momento e será lançado para pagamento o valor de <b>R$" + 
                                    balance.remainingBalance.toFixed(2) + "</b> em <b>"+formatDate(nextBD)+"</b> (próxima fatura do cliente) referente ao valor proporcional do novo plano <b>"+
                                    _object.availablePlans.name+"</b>, no valor de <b>R$"+_object.availablePlans.deal.value.toFixed(2)+"</b>. </html>";
                                }
                        
                            } else {
                        
                                if(balance.remainingBalance > 0){
                                    if (balance.remainingBalance > _object.availablePlans.deal.value) {
                                        help += "<html><br> Informar para o consumidor que a migração de plano acontecerá neste momento e será concedido um crédito no valor total de <b>R$"
                                        +balance.remainingBalance.toFixed(2)+"</b> devido ao plano atual. Nas renovações seguintes esse saldo credor será abatido e quando totalmente consumido, as demais já virão com o valor do novo plano <b>"
                                        +_object.availablePlans.name+"</b>, no valor de <b>R$"+_object.availablePlans.deal.value.toFixed(2)+"</b>.</html>";
                                    } else {
                                        let nextInvoiceDay;
                                        if (_object.contract.paidPeriodEnd){
                                            nextInvoiceDay = new Date(_object.contract.paidPeriodEnd);
                                            nextInvoiceDay.setUTCSeconds(nextInvoiceDay.getUTCSeconds()+1);
                                            nextInvoiceDay = formatDate(nextInvoiceDay);
                                        } else {
                                            nextInvoiceDay = "(não definido)";
                                        }
                                        help += "<html><br> Informar para o consumidor que a migração de plano acontecerá neste momento e será concedido um desconto de <b>R$"+balance.remainingBalance.toFixed(2)+
                                        "</b> em <b>"+nextInvoiceDay+"</b> (próxima renovação do consumidor) devido ao crédito remanescente do plano atual. As demais faturas já serão emitidas com o valor do novo plano <b>"+
                                        _object.availablePlans.name+"</b>.</html>";
                                    }
                                } else if(balance.remainingBalance < 0){
                                    balance.remainingBalance = - balance.remainingBalance;
                        
                                    if (_object.contract.paymentMethod && _object.contract.paymentMethod._id == "59c41f90ce02720a65794af9"){ // se a forma de pagamento for cartão de crédito
                                        let creditNumber;
                                        if (_object.contract.accountPaymentData && _object.contract.accountPaymentData.accountCreditCardData && _object.contract.accountPaymentData.accountCreditCardData.number) {
                                            creditNumber = _object.contract.accountPaymentData.accountCreditCardData.number.maskedValue;
                                        } else {
                                            creditNumber = "(não encontrado)";
                                        }
                        
                                        help += "<html><br> Informar para o consumidor que será lançado para pagamento o valor de <b>R$"+balance.remainingBalance.toFixed(2)+
                                        "</b> referente ao proporcional do novo plano. Este valor será cobrado de uma unica vez (não parcelado) no cartão atual do cliente final "+creditNumber+
                                        ". A migração para o plano <b>"+_object.availablePlans.name+"</b> acontecerá assim que confirmado o pagamento pela operadora de cartão de crédito.</html>";
                                    } else if (_object.contract.paymentMethod && _object.contract.paymentMethod._id == "59c41fadce02720a65794b10"){ // se a forma de pagamento for boleto eletronico
                                        
                                        let expirationDate = new Date();
                                        expirationDate.setDate(expirationDate.getDate()+6);
                        
                                        help += "<html><br> Informar para o consumidor que será lançado para pagamento por boleto o valor de <b>R$"+balance.remainingBalance.toFixed(2)+
                                        "</b> referente ao proporcional do novo plano, com vencimento em <b>"+formatDate(expirationDate)+"</b>. A migração para o plano <b>"+_object.availablePlans.name+
                                        "</b> acontecerá assim que confirmado o pagamento do boleto.</html>";
                                    }
                                }
                            }
                        }
                    }
                    
                    var prdAtend = _object.prodServ && _object.prodServ.identifier;
                    if (prdAtend === "serasaantifraude") {
                        if (!_object.antifraudeContract  || _object.antifraudeContract.contractStates.serviceState.identifier != "ACTIVE") {
                            help += "<html> <br>Consumidor não possui contrato <b>Antifraude</b> ativo<br> </html>";
                        } else {
                            if (_object.customerServiceAction && _object.customerServiceAction.identifier === "cancelCharge"){
                                var query = buscarContratoCartaoCredito();
                		        if (query.hits.total > 0) {
        		                    help = "<html> <br> <b> Selecione o contrato do cliente </b> <br></html>" ;
        		                    if(_object.contract){
        		                        help = "<html> <br> <b> Selecione a cobrança para cancelamento </b> <br></html>" ;
        		                        
                                        _metadata.fields.invoices.hidden = false;
                                        _metadata.fields.invoices.required = true;
                                        _metadata.fields.invoices.maxMultiplicity = 1;
                                        
        		                        _metadata.fields.invoices.query = {
                                            "query": {
                                                "bool": {
                                                    "must": [
                                                        {
                                                            "nested": {
                                                                "path": "invoiceItem",
                                                                "query": {
                                                                    "term": {
                                                                        "invoiceItem.contract.code": Number(_object.contract.code)
                                                                    }
                                                                }
                                                            }
                                                        },
                                                        {
                                                            "term": {
                                                                "invoiceState._id": "5977c246ce027227c4269e3a"
                                                            }
                                                        }
                                                    ]
                                                }
                                            },
                                            "sort": {
                                                "_creationDate": "desc"
                                            }
                                        };
                                        
                                        if(_object.invoices){
                                            help = "<html> <br> <b> Fatura "+ _object.invoices[0].code + 
                                                    " de " + _object.invoices[0].referenceMonth + " paga em " +
                                                     formatarData(_object.invoices[0].paymentDate) + " no valor de R$ " + 
                                                     _object.invoices[0].value.toFixed(2) + " </b> <br></html> ";
                                            
                                            _metadata.fields.refundAmount.hidden = false;
                                            _metadata.fields.refundAmount.required = true;
                                            
                                            if(!_object.refundAmount){                                                     
                                                 help += "<html> <br> <b> Insira o valor a ser estornado </b> <br></html>" ;
                                            } else if(_object.refundAmount > _object.invoices[0].value){
                                                help = "<html> <br> <b> O valor do estorno deve ser de até R$ " + _object.invoices[0].value.toFixed(2) + " </b> <br> </html>"; 
                                            } else if(_object.refundAmount && _object.refundAmount <= 0){
                                                help = "<html> <br> <b> O valor do estorno deve ser maior que R$ 0 </b> <br> </html>"; 
                                            }
                                        }
                                    }
                                } else {
                                    help = "<html> <br> Cliente <b>NÃO</b> possui contrato na forma de pagamento <b>CARTÃO DE CRÉDITO</b></html>"; 
                    		    }
                		    }
                        }
                    }

                    if (afContract.scheduleSuspensionDate){
                        var date = afContract.scheduleSuspensionDate;
                        var day, mounth, year;
                        day = date.getDate();
                        mounth = date.getMonth()+1;
                        year = date.getFullYear();
                        if(day<10){
                            day="0"+day;
                        }
                        if(mounth<10){
                            mounth="0"+mounth;
                        }
                        help += "<html><br>Cliente possui um contrato Serasa antifraude com suspensão agendada para: <b>"+day+"/"+mounth+"/"+year+".</b><br></html>";
                    }
            	}
			}
			
			function montarQueryBuscaPedidosVCPE(cliente, produto) {
			    return {
                    "query": {
                        "bool": {
                            "must": [
                                {
                                    "match": {
                                        "buyer._id": cliente
                                    }
                                },
                                {
                                    "match": {
                                        "orderFinancialState.identifier": "PAID"
                                    }
                                },
                                {
                                    "nested": {
                                        "path": "orderItems",
                                        "query": {
                                            "match": {
                                                "orderItems.product.identifier": produto[1]
                                            }
                                        }
                                    }
                                }
                            ]
                        }
                    }
                };
			}
			
			function buscaPedidosVCPE(cliente, produto) {
			    var query = montarQueryBuscaPedidosVCPE(cliente, produto);
			    var retornoPedido = billing.order._search(query).hits;
                
                return retornoPedido;
			}
			
			function validarOpcaoVCPE(productTab){
			    var product = [];
                if(productTab === "voceconsultapessoas"){
                    product[0] = "Você Consulta Pessoas";
                    product[1] = "VOCE_CONSULTA_PESSOA_ECOMMERCE";
                } else if (productTab === "voceconsultaempresas"){
                    product[0] = "Você Consulta Empresas";
                    product[1] = "VOCE_CONSULTA_EMPRESA_ECOMMERCE";
                }
                
                return product;
			}
			
			function voceConsultaTabulation() {
				
				// se atendente selecionar o produto vce ou vcp
                help += "<html><b> Você Consulta Pessoa/Empresa </b>: Após selecionar a tabulação, insira a <b> Ação do Atendimento </b>. <br></html>";
                
                if (_object.customerServiceAction && _object.customerServiceAction.identifier === "cancelOrder"){
    				
    				var productTab = _object.prodServ && _object.prodServ.identifier;
                    var product = validarOpcaoVCPE(productTab);
                    var order = buscaPedidosVCPE(_object.customer._id, product);
    				_metadata.fields.ordersToCancel.query = montarQueryBuscaPedidosVCPE(_object.customer._id, product);

    				//se houver pedidos falar com atendente e inserir no campo ordersToCancel
    				if(order.total > 0){
    					help = "<html>Identificamos que o consumidor possui <b>" + order.total + "</b> pedidos do <b> " + product[0] + " </b>." + 
    					"<br><b> Selecione os pedidos que deseja cancelar </b> <br> </html>";
    					
    					_metadata.fields.ordersToCancel.hidden = false;
    				    _metadata.fields.ordersToCancel.required = true;

    				} else {
    				    help = "<html>Identificamos que o consumidor <b> NÃO </b>possui pedido(s) pago(s) do produto <b> " + product[0] + "</b>.</html>";
    				    
    				    _metadata.fields.ordersToCancel.hidden = false;
    				    _metadata.fields.ordersToCancel.required = true;
    				   
    				}    
                }
			}
		    
		    function buscarContratoCartaoCredito(){
                var query = billing.contract._search({
                	"query": {
                        "bool":{
                        	"must": [
                        	{
                	        	"term": {
                	            	"buyer._id": _object.customer._id
                	        	}
                        	},
                        	{
                        		"term": {
                	            	"paymentMethod._id": "59c41f90ce02720a65794af9"
                	        	}
                        	}]
                        }
                    }
    		    });
    		    
    		    return query;
            }
                    
            function formatarData(dataSemFormatar){
                var data = dataSemFormatar;
                
                var dia = data.getDate().toString(),
                    diaFinal = (dia.length == 1) ? "0" + dia : dia,
                    mes = (data.getMonth() + 1).toString(),
                    mesFinal = (mes.length == 1) ? "0" + mes : mes,
                    ano = data.getFullYear().toString();
                
                var dataFinal = diaFinal + "/" + mesFinal + "/" + ano;
                
                return dataFinal;
            }
		    
		    function lnoTabulation() {
		        var lnoNegotiationNumber = _object.negotiations.reduce(function (acc, negotiation) {
                    if (negotiation.productCategory && negotiation.productCategory.some(function (category) {
                        if (category._id === "5bbceb7fb3163958ca0a5338") {   /// LNO
                            return true;
                        }
                    })) {
                        return acc + 1;
                    } else {
                        return acc;
                    }
                }, 0);
                if (lnoNegotiationNumber === 1) {
                    help = "<html>Identificamos que o consumidor possui " + lnoNegotiationNumber + " negociação no <b>Serasa Limpa Nome</b>, localizado abaixo da tabulação.<br></html>";
                } else {
                    help = "<html>Identificamos que o consumidor possui " + lnoNegotiationNumber + " negociações no <b>Serasa Limpa Nome</b>, localizados abaixo da tabulação.<br></html>";
                }
		    }
		    
		    function eCredTabulation() {
		        var eCredNegotiationNumber = _object.negotiations.reduce(function (acc, negotiation) {
                    if (negotiation.productCategory && negotiation.productCategory.some(function (category) {
                        if (category._id === "5bbceb6db3163958ca0a52dc") {   /// eCred
                            return true;
                        }
                    })) {
                        return acc + 1;
                    } else {
                        return acc;
                    }
                }, 0);
                if (eCredNegotiationNumber === 1) {
                    help = "<html>Identificamos que o consumidor possui " + eCredNegotiationNumber + " negociação no <b>Serasa eCred</b>, localizado abaixo da tabulação.<br></html>";
                } else {
                    help = "<html>Identificamos que o consumidor possui " + eCredNegotiationNumber + " negociações no <b>Serasa eCred</b>, localizados abaixo da tabulação.<br></html>";
                }
		    }
		    
		    var help = '';
		    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                var prodServIden = _object.prodServ && _object.prodServ.identifier;
                
                if(prodServIden === "serasaconsumidorsite" && _object.subSubReason && _object.subSubReason.identifier == "serasaconsumidorsite.acesso.cadastrosite.datadenascimentoincorreta"){
                    serasaConsumidorSiteTabulation();
                }

    			if (prodServIden === "serasaconsumidorsite" && _object.customer /*&& _object.personFoundinSerasaConsumidor*/) {
                    serasaConsumidorTabulation();
    			} else if (prodServIden === "serasaantifraude") {
    			    if (_object.antifraudeContract) {
                	    antifraudeContractTabulation();
    			    } else if (_object.attendanceLevel.name === "N1" && _object.cpf){
                        help = "<html>Consumidor não possui <b>Antifraude</b>, fale dos benefícios e explique o produto.<br>" + 
                         "Caso selecione o motivo <b>'Oferta'</b>, o atendimento seguirá para a atividade de <b>Vendas</b>.<br></html>";
    			    }
                } else if (_object.negotiations && _object.negotiations.length) {
                    if (prodServIden === "serasalimpanome") {
                        lnoTabulation();
                    } else if (prodServIden === "serasaecred") {
                        eCredTabulation();
                    }
                } else if (prodServIden === "cadastropositivo" && _object.customer){
                    cadastroPositivoTabulation();
                    if (_object.cnpj && _object.responsible) {
                        help += "<html><br><b>Dados do atendimento:</b><br>"+
                        "<b>Razão Social:</b> "+_object.customer.companyName+
                        "<br><b>CNPJ:</b> "+_object.customer.maskedCpfCnpj+
                        "<br><br><b>Contato:</b>"+
                        "<br><b>Nome:</b> "+_object.responsible.name+
                        "<br><b>CPF:</b> "+_object.responsible.maskedCpfCnpj+
                        "<br><b>Email:</b> "+_object.responsible.emails[0].address+
                        "<br><b>Telefone:</b> "+_object.responsible.phones[0].number+
                        "<br><br>No término deste atendimento será encaminhando um email com o protocolo de atendimento para o contato no endereço de email acima.<br></html>";
                    }
                } else if (prodServIden == "voceconsultapessoas" || prodServIden == "voceconsultaempresas"){
					voceConsultaTabulation();
				}
		    }
		    
		    function isEmpty(obj) {
                for(var prop in obj) {
                    if(obj.hasOwnProperty(prop))
                        return false;
                }
                return true;
            }
		    
		    function editHelpOptOut(optin){
				if(optin == false){
					help += "<html><b> Oriente o consumidor que o status do cadastro positivo está INATIVO </b><br></html>";
				} else {
					help += "<html><b> Ao concluir o atendimento o pedido do consumidor será inserido na fila e será executado em 48 horas.</b><br></html>";
				}
			}
			
		    function editHelpReOptIn(optin){
				if(optin == true){
					help += "<html><b> Oriente o consumidor que o status do cadastro positivo está ATIVO </b><br></html>";
				} else {
					help += "<html><b> Ao concluir o atendimento o pedido do consumidor será inserido na fila e será executado em 48 horas.</b><br></html>";
				}
		    }
		    
		    function geraEditHelpCadastroPositivo(retorno){
		        if (isEmpty(retorno)){
		            if(_object.cpf){
        		        help += "<html><b> Pessoa não incluída no Cadastro Positivo. </b> <br></html>";    
        		    }else if(_object.cnpj){
        		        help += "<html><b> Empresa não incluída no Cadastro Positivo. </b> <br></html>";    
					}
					if((_object.reason) && (_object.reason.identifier == "cadastropositivo.reabertura" || _object.reason.identifier == "cadastropositivo.cancelamento")){
						help += "<html><b> Ao concluir o atendimento o pedido do consumidor será inserido na fila e será executado em 48 horas.</b><br></html>";
					}
		        }else if (retorno.optin != null && retorno.optin == true){
					help += "<html><b> Status do Cadastro Positivo: </b> Ativo <br></html>";
					if(_object.reason && _object.reason.identifier == "cadastropositivo.reabertura"){
						editHelpReOptIn(retorno.optin);
					}else if (_object.reason && _object.reason.identifier == "cadastropositivo.cancelamento"){
						editHelpOptOut(retorno.optin);
					}   
	            }else if (retorno.optin != null && retorno.optin == false){
	                help += "<html><b> Status do Cadastro Positivo: </b> Inativo <br></html>";
	                if(_object.reason && _object.reason.identifier == "cadastropositivo.reabertura"){
						editHelpReOptIn(retorno.optin);
					}else if (_object.reason && _object.reason.identifier == "cadastropositivo.cancelamento"){
						editHelpOptOut(retorno.optin);
					}   
				}
			}
		    
		    function msgErroAPICadastroPostitivo(){
		        if(_object.cpf){
    		        help += "<html><b> Não foi possível consultar o status do cadastro positivo do consumidor. </b> <br></html>";		        
    		    }else if(_object.cnpj){
    		        help += "<html><b> Não foi possível consultar o status do cadastro positivo da empresa. </b> <br></html>";
    		    }
		    }
		    
		    function cadastroPositivoTabulation() {
		        let retorno = JSON.parse(_object.cadastroPositivoResponse);
		        if(retorno.success == true){
		            geraEditHelpCadastroPositivo(retorno.response);
		        } else {
		            msgErroAPICadastroPostitivo();
		        }
    		}
		    
		    if (_object.isSolved == "sim" && _object.responsibleGroup && _object.responsibleGroup._id == "59ef9311ce02722cda742b89"){
                help = "<html>Informe no campo resolução a resposta ao consumidor. <br><b>IMPORTANTE:</b> Toda informação contida no campo irá ser enviada por e-mail para o consumidor.<br></html>"
            }
            
            function getInfoFromRF(){
                let info = '';
                let successReceitaFederal = true;
                if (_object.errorsLog && _object.errorsLog.length){
                    successReceitaFederal = _object.errorsLog.every( error => {
                        return (error.indexOf("Erro na consulta de CPF/CNPJ pela Receita Federal:") === -1);
                    });
                }
                
                if (_object.customer.receitaFederal && _object.personFoundinRFWS){
                    info += "Nome: <b>" + _object.customer.name + "</b><br>"
                    info += "CPF: <b>" + _object.cpf + "</b><br>"
                    let birthDate = (_object.customer && _object.customer.dateOfBirth)? _object.customer.dateOfBirth : null;
                    if(birthDate){
                        info += "Data de nascimento: <b>" + returnStringDate(birthDate) + "</b><br><br>"
                    }
                } else if (_object.customer.receitaFederal && !successReceitaFederal && _object.personFoundinSystem){
                    let lastQueryRFDate = _object.customer.receitaFederal.rfLastConsultationDate;
                    info += "<b>Falha ao consultar o cadastro na Receita Federal, dados da última consulta realizada em " + returnStringDate(lastQueryRFDate) +":</b><br>"
                    info += "Nome: <b>" + _object.customer.name + "</b><br>"
                    info += "CPF: <b>" + _object.cpf + "</b><br>"
                    let birthDate = (_object.customer && _object.customer.dateOfBirth)? _object.customer.dateOfBirth : null;
                    if(birthDate){
                        info += "Data de nascimento: <b>" + returnStringDate(birthDate) + "</b><br><br>"
                    }
                } else if(!_object.customer.receitaFederal){
                    info = "<b>Dados do consumidor não confirmados pela Receita Federal</b><br><br>"
                }
                
                return info;
                
                function returnStringDate(date){
                    let day = (('' + date.getDate()).length === 1)? '0' + date.getDate(): + date.getDate()
                    let month = (('' + date.getMonth()).length === 1)? '0' + (date.getMonth()+1): + (date.getMonth()+1)
                    return day + '/' + month + '/' + date.getFullYear();
                }
            }
              
            return '<html>' + getInfoFromRF() + '</html>' + help;
		}
		
		
        
        function treatN1AttendanceProgression() {
    
    		function isVariableDefined(variable) {
    			function attendanceTab() {
    				var tabOn = _object.subSubReason ? _object.subSubReason :
    					_object.subReason ? _object.subReason :
    					_object.reason ? _object.reason : null;
    				if (tabOn && tabOn.customerServiceData && tabOn.customerServiceData.length) {
    				    return true;
    				} else {
    				    return false;
    				}
    			}
    
    			function isSolved() {
    				return _object.isSolved === 'sim' || _object.isSolved === 'encaminhar';
    			}
    
    			function attendanceClassification() {
    				var attClass = _object.attendanceClassification;
    				return attClass && attClass.length && attClass[0].source && attClass[0].type;
    			}
    
    			////////////////////
    
    			var variableHasFunction = ["attendanceClassification", "attendanceTab", "isSolved"];
    			if (variableHasFunction.indexOf(variable) > -1) {
    				return eval(variable)();
    			} else {
    				if (_object[variable] || _object[variable] === 0) {
    					return true;
    				} else {
    					return false;
    				}
    			}
    		}
    
    		function buildEditHelpText() {
    
    			function setActionError() {
                    var help = '';
                    if (_object.statusAction === 'erro' && _object.customerServiceAction) {
                        var errorReason; 
                        var errorReasons = [];
                        var searhException;
                        
                        if (_pin.errorsLog && _pin.errorsLog.length) {
                        
                            searhException = _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException");
                            
                            if (searhException >= 0) {
                                errorReason = "Serviço indisponível.";
                            } else {
                                errorReasons = _pin.errorsLog[_pin.errorsLog.length - 1].split(":");
                                if (errorReasons && errorReasons.length) {
                                    errorReason = errorReasons[errorReasons.length -1];
                                    errorReason = errorReason.replace("}", '');
                                    errorReason = errorReason.replace("{", '');
                                    errorReason = errorReason.replace("\\u00e3", "ã");
                                    errorReason = errorReason.replace("\\u00e1", "á");
                                    errorReason = errorReason.replace("\\u00e9", "é");
                                    errorReason = errorReason.replace("\\u00ed", "í");
                                    errorReason = errorReason.replace("\\u00f3", "ó");
                                    errorReason = errorReason.replace("\\u00fa", "ú");
                                    errorReason = errorReason.replace("\\u00e7", "ç");
                                    //errorReason = JSON.parse(JSON.stringify(errorReason));
                                    //decodeURIComponent(encodeURIComponent(errorReason));
                                }
                            }
                        }
                        
                        if (errorReason){
                            if (errorReason === " Email já cadastrado."){
                                help = "<html>Não foi realizada troca do e-mail, pois o e-mail informado <b>"+_object.newValue+"</b> já está cadastrado no Serasa Consumidor para outro usuário."+
                                "<br><br>Favor realizar nova tentativa com outro e-mail."+
                                "<br><br>Caso o consumidor não tenha outro e-mail para alteração, orienta-lo para criação de novo e-mail e após retornar ao atendimento "+
                                "(nesta situação será necessário desmarcar ação para finalizar o atendimento).<br></html>";
                            } else if(errorReason === ' Erro OptIn'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt In do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else if(errorReason === ' Erro OptOut'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt Out do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else {
                                help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "': "+errorReason+" <br><br>Favor tentar novamente ou encaminhar para o próximo nível.</b><br></html>";
                            }
                        } else {
                            help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "'. <br><br>Favor tentar novamente ou encaminhar para o próximo nível.</b></html>";
                        }
        
                    
                        
                    }
                    return help;
                }
    			
    			function setActionText() {
    			    var help = '';
    			    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
            			var tabOn = _object.subSubReason ? _object.subSubReason :
            				_object.subReason ? _object.subReason :
            				_object.reason ? _object.reason : null;
            				
        				if (tabOn && tabOn.customerServiceAction) {
        				    help = "<html>Caso necessário, selecione a ação a ser realizada no campo <b>“Ação de atendimento”</b>.<br></html>";
        				}
    			    }
    			
    		       return help;
    			}
    			
                function setFillTabInformation() {
                    var help;
                    
                    if (_object.reason && _object.reason.identifier === 'serasaantifraude.cancelamento') {
                        help = "<html>Para prosseguir com o cancelamento, selecione o <b>contrato</b> do cliente.<br>" + 
                        "Após selecionar o contrato o atendimento seguirá para <b>retenção/cancelamento</b>.</html>";
                    } else {
                        if (_object.cnpj) {
                            help = "<html>Realize a <b>tabulação do atendimento</b> do <b>Cadastro Positivo</b> escolhendo <b>Motivo</b> do atendimento.<br>" + 
                            "Caso necessário, selecione também o <b>SubMotivo</b> e <b>SubSubMotivo</b>.</html>";
                        } else {
                            help = "<html>Realize a <b>tabulação do atendimento</b> escolhendo primeiro o <b>Produto/Serviço</b> e depois o <b>Motivo</b> do atendimento.<br>" + 
                            "Caso necessário, selecione também o <b>SubMotivo</b> e <b>SubSubMotivo</b>.</html>";
                        }
                    }
                    return help;
                }
    			
    			function setSolvedText() {
    			    var help = '';
        			if (_object.isSolved && _object.isSolved === 'sim') {
        				help = "<html>Termine o atendimento através do botão <b>“Concluir”</b>. " + 
        				       "Se necessário <b>anexe</b> os arquivos com as evidências geradas.</html>";
        			} else if (_object.isSolved && _object.isSolved === 'encaminhar') {
        				var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
        				help = "<html>Baseado na urgência do problema, o priorize selecionando a opção adequada no campo <b>“Prioridade”</b>.<br>" +
        					"Se necessário <b>anexe</b> os arquivos com as evidências geradas para o atendimento.<br><br>" + 
        					"O SLA deste atendimento é de <b>" + (slaData && slaData.sla ? slaData.sla : "(Não encontrado)") + "</b>; " + 
        					"informe ao consumidor que a previsão de atendimento é até <b>" + (slaData && slaData.finalDate ? formatDate(slaData.finalDate) : "(Não encontrado)") + "</b>.<br><br>" + 
        					"Termine o atendimento através do botão <b>“Concluir”</b></html>.";
        			}
        			return help;
    			}
    			
    			function setZendeskCampaignText() {
    			    var help = '';
    			    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
        			    if (_metadata.fields.showEmailHistory.hidden === false || _metadata.fields.showEmailHistory.hidden === false) {
                            help = "<html>Mais informações podem ser obtidas selecionando as <b>opções de consulta</b> no final da página.<br></html>";
        			    }
    			    }
    			    return help;
    			}
    			
    			function setLabelText() {
    			    var help = '';
    			    if (_object.customer && _object.customer.labels && _object.customer.labels.length) {
    			        var labels = _object.customer.labels.map(function(label){ return '<b>' + label.name + '</b>';}).join(', ');
    			        help = "<html>Esse consumidor foi marcado como: " + labels + ".</html>";
    			    }
    			    
    			    return help;
    			}
    			
    			var tabInformation = setTabInformation();           /// Setar editHelp baseado na tabulação
    			var fillTabInformation = setFillTabInformation();   /// Informação padrão sobre preenchimento da tabulação
    			var actionText = setActionText();                   /// Tabulação com ação
    			var actionError = setActionError();                 /// Erro na ação
    			var labelText = setLabelText();                     /// Rótulos do consumidor
    			var zendeskCampaignText = setZendeskCampaignText(); /// Mensagem sobre possibilidade de consulta
    			var solvedText = setSolvedText();                   /// Informação para resolução
            	
    
    			////// Evolução completa do editHelp
    			
    			return [
        			putNewLine([actionError, tabInformation, fillTabInformation, labelText]),
        
                    putNewLine([actionError, tabInformation, "Preencha o campo <b>“Observações”</b> com as informações relevantes ao atendimento.", labelText]),
        
                    putNewLine([actionError, actionText, zendeskCampaignText]) + 
        			//"<html>Informe se o atendimento está <b>resolvido</b>, se não será encaminhado para o <b>próximo nível</b>.</html>" + 
        			((_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0) ? "" : "<html>Informe se o atendimento está <b>resolvido</b>, se não será encaminhado para o <b>próximo nível</b>.</html>")+
        			(labelText ? '<html><br><br><html>' + labelText : '' ),
        
        			solvedText
        		];
    			
    
    		}
    
    		////////////////////////////////////
    		
    		var progression = [    // Quais variáveis devem estar preenchidas para as próximas aparecerem
    			["attendanceClassification", "attendanceTab"],
    			["description"],
    			["isSolved"],
    			["files"]
    		];
    		
    		var isLevelDone = [];  // Exemplo: [true, true, false] significa que não cumpriu o terceiro nível
    
    		progression.reduce(
    			function (previousLevelAttended, phaseVariables) {
    
    			var allFieldsFilled = previousLevelAttended;
    			var hideCurrentLevelFields = !previousLevelAttended;
    
    			phaseVariables.forEach(
    				function (variable) {
        				allFieldsFilled = allFieldsFilled && isVariableDefined(variable);
        				_metadata.fields[variable].hidden = hideCurrentLevelFields;
    			});
    
    			isLevelDone.push(allFieldsFilled);
    			return allFieldsFilled;
    		}, true);
            
    		var lastLevelDone = 0;
            isLevelDone.some(
                function (level, index) {
                    lastLevelDone = index;
                    if (level !== true) {
                        return true;
                    }
                }
            );
    		var editHelp = buildEditHelpText();
    		_metadata.editHelp = editHelp[lastLevelDone];    		
    		
        }
        
        function treatN2AttendanceProgression() {
            function buildEditHelpText() {
    
    			function setActionError() {
    			    var help = '';
            	    if (_object.statusAction === 'erro' && _object.customerServiceAction) {
                        var errorReason; 
                        var errorReasons = [];
                        var searhException;
                        
                        if (_pin.errorsLog && _pin.errorsLog.length) {
                        
                            searhException = _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException");
                            
                            if (searhException >= 0) {
                                errorReason = "Serviço indisponível.";
                            } else {
                                errorReasons = _pin.errorsLog[_pin.errorsLog.length - 1].split(":");
                                if (errorReasons && errorReasons.length) {
                                    errorReason = errorReasons[errorReasons.length -1];
                                    errorReason = errorReason.replace("}", '');
                                    errorReason = errorReason.replace("{", '');
                                    errorReason = errorReason.replace("\\u00e3", "ã");
                                    errorReason = errorReason.replace("\\u00e1", "á");
                                    errorReason = errorReason.replace("\\u00e9", "é");
                                    errorReason = errorReason.replace("\\u00ed", "í");
                                    errorReason = errorReason.replace("\\u00f3", "ó");
                                    errorReason = errorReason.replace("\\u00fa", "ú");
                                    errorReason = errorReason.replace("\\u00e7", "ç");
                                    //errorReason = JSON.parse(JSON.stringify(errorReason));
                                    //decodeURIComponent(encodeURIComponent(errorReason));
                                }
                            }
                        }
                        
                        if (errorReason){
                            if (errorReason === " Email já cadastrado."){
                                help = "<html>Não foi realizada troca do e-mail, pois o e-mail informado <b>"+_object.newValue+"</b> já está cadastrado no Serasa Consumidor para outro usuário."+
                                "<br><br>Favor realizar nova tentativa com outro e-mail."+
                                "<br><br>Caso o consumidor não tenha outro e-mail para alteração, orienta-lo para criação de novo e-mail e após retornar ao atendimento "+
                                "(nesta situação será necessário desmarcar ação para finalizar o atendimento).<br></html>";
                            } else if(errorReason === ' Erro OptIn'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt In do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else if(errorReason === ' Erro OptOut'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt Out do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else {
                                help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "': "+errorReason+" <br><br>Favor tentar novamente ou encaminhar para o próximo nível.</b><br></html>";
                            }
                        } else {
                            help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "'. <br><br>Favor tentar novamente ou abrir um chamado para a Sydle.</b></html>";
                        }
        
                    
                        
                    }
    			    return help;
    			}
    			
                function setUserBasicInfo() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        help = "<html>Protocolo: <b>" + _object.protocol + "</b><br>" +
        		            "Nome: <b>" + (_object.customer.name ? _object.customer.name : "(Não encontrado)") + "</b><br>" +
        		            "CPF: <b>" + (_object.cpf ? _object.cpf : "(Não encontrado)") + "</b><br></html>";
                    }
    		        
    		        return help;
                }
                
                function setObservationText() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        help = "<html>Verifique as informações relevantes em <b>Justificativa da atribuição</b>.<br><html>";
                    }
                    
                    return help;
                }
    
    
                function setSlaInfo() {
                    var help = '';
                    
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
                        if (slaData) {
                            help = "O SLA deste atendimento é de <b>" + (slaData && slaData.sla ? slaData.sla : "(Não encontrado)") + "</b> " + 
                					"e sua data final é <b>" + (slaData && slaData.finalDate ? formatDate(slaData.finalDate) : "(Não encontrado)") + "</b>.</html>";
                        }
                    }
        			
        			return help;
                }
    
    			var actionError = setActionError();                 /// Erro na ação
                var userBasicInfo = setUserBasicInfo();
                var tabInformation = setTabInformation();           /// Setar editHelp baseado na tabulação
                var observationText = setObservationText();
                var slaInfo = setSlaInfo();
    
    			_metadata.editHelp = putNewLine([actionError, userBasicInfo, tabInformation, observationText, slaInfo]);
    				
    		}
        
            ////////////////////////////////////
            
            buildEditHelpText();
        }
        
        function treatN3N4AttendanceProgression() {
            function buildEditHelpText() {
    
    			function setActionError() {
    			    var help = '';
            	    if (_object.statusAction === 'erro' && _object.customerServiceAction) {
                        var errorReason; 
                        var errorReasons = [];
                        var searhException;
                        
                        if (_pin.errorsLog && _pin.errorsLog.length) {
                        
                            searhException = _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException");
                            
                            if (searhException >= 0) {
                                errorReason = "Serviço indisponível.";
                            } else {
                                errorReasons = _pin.errorsLog[_pin.errorsLog.length - 1].split(":");
                                if (errorReasons && errorReasons.length) {
                                    errorReason = errorReasons[errorReasons.length -1];
                                    errorReason = errorReason.replace("}", '');
                                    errorReason = errorReason.replace("{", '');
                                    errorReason = errorReason.replace("\\u00e3", "ã");
                                    errorReason = errorReason.replace("\\u00e1", "á");
                                    errorReason = errorReason.replace("\\u00e9", "é");
                                    errorReason = errorReason.replace("\\u00ed", "í");
                                    errorReason = errorReason.replace("\\u00f3", "ó");
                                    errorReason = errorReason.replace("\\u00fa", "ú");
                                    errorReason = errorReason.replace("\\u00e7", "ç");
                                    //errorReason = JSON.parse(JSON.stringify(errorReason));
                                    //decodeURIComponent(encodeURIComponent(errorReason));
                                }
                            }
                        }
                        
                        if (errorReason){
                            if (errorReason === " Email já cadastrado."){
                                help = "<html>Não foi realizada troca do e-mail, pois o e-mail informado <b>"+_object.newValue+"</b> já está cadastrado no Serasa Consumidor para outro usuário."+
                                "<br><br>Favor realizar nova tentativa com outro e-mail."+
                                "<br><br>Caso o consumidor não tenha outro e-mail para alteração, orienta-lo para criação de novo e-mail e após retornar ao atendimento "+
                                "(nesta situação será necessário desmarcar ação para finalizar o atendimento).<br></html>";
                            } else if(errorReason === ' Erro OptIn'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt In do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else if(errorReason === ' Erro OptOut'){
                                help = '<html> ' +
                                    '<b>Falha ao criar o pedido de Opt Out do consumidor.</b><br>' +
                                    '<b>Orientar o consumidor e atribuir caso ao Back Office.</b>'+
                                '</br></html>';
                            } else {
                                help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "': "+errorReason+" <br><br>Favor tentar novamente ou encaminhar para o próximo nível.</b><br></html>";
                            }
                        } else {
                            help = "<html><b>Ocorreu um <b>erro</b> em '" + _object.customerServiceAction.name + "'. <br><br>Favor tentar novamente ou abrir um chamado para a Sydle.</b></html>";
                        }
        
                    
                        
                    }
    			    return help;
    			}
    			
                function setUserBasicInfo() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        help = "<html>Protocolo: <b>" + _object.protocol + "</b><br>" +
            		        "Nome: <b>" + (_object.customer.name ? _object.customer.name : "(Não encontrado)") + "</b><br>" +
            		        "CPF: <b>" + (_object.cpf ? _object.cpf : "(Não encontrado)") + "</b><br></html>";
                    }
    		        
    		        return help;
                }
                
                function setObservationText() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        help = "<html>Verifique as informações relevantes em <b>Justificativa da atribuição</b>.<br><html>";
                    }
                    
                    return help;
                }
    
    
                function setSlaInfo() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
                        help = "O SLA deste atendimento é de <b>" + (slaData && slaData.sla ? slaData.sla : "(Não encontrado)") + "</b> " + 
            					"e sua data final é <b>" + (slaData && slaData.finalDate ? formatDate(slaData.finalDate) : "(Não encontrado)") + "</b>.</html>";
                    }
        			return help;
                }
    
                function setPriorityText() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        if (_object.priority && (_object.priority === 'Alta' || _object.priority === 'SOS')) {
                            help = "<html>Esse atendimento está com prioridade <b>" + _object.priority + "</b>.</html>";
                        }
                    }
                    
                    return help;
                }
    
                function setTabTypeText() {
                    var help = '';
                    if (!(_object.statusAction == "erro" && _pin.errorsLog && _pin.errorsLog.length && _pin.errorsLog[_pin.errorsLog.length - 1].indexOf("UnknownHostException") >= 0)){
                        if (_object.tabType) {
                            help = "<html>A tabulação desse atendimento é do tipo <b>" + (_object.tabType && _object.tabType.type ? _object.tabType.type : "(Não encontrado)") + 
                            "</b> e subtipo <b>" + (_object.tabType && _object.tabType.subtype ? _object.tabType.subtype : "(Não encontrado)") + "</b>.<br></html>";
                            
                        }
                    }
                    
                    return help;
                }
    
    			var actionError = setActionError();                 /// Erro na ação
                var userBasicInfo = setUserBasicInfo();
                var observationText = setObservationText();
                var slaInfo = setSlaInfo();
                var priorityText = setPriorityText();
                var tabTypeText = setTabTypeText();
    
    			_metadata.editHelp = putNewLine([actionError, userBasicInfo, tabTypeText, observationText, slaInfo, priorityText]);
    				
    		}
        
            ////////////////////////////////////
            
            buildEditHelpText();
        }

		////////////////////////////////////

		if (_object.attendanceLevel.name === "N1") {
			treatN1AttendanceProgression();
		} else if (_object.attendanceLevel.name === "N2") {
		    treatN2AttendanceProgression();
		} else {
		    treatN3N4AttendanceProgression();
		}

	}

	function treatAttendanceDetails() {

		if (_object.URAOrigin) {
			_metadata.fields.URAOrigin.hidden = false;
		}

		_metadata.fields.URAPhone.hidden = _object.URAPhone ? false : true;
		_metadata.fields.digitalCertificate.hidden = _object.digitalCertificate ? false : true;

		_metadata.fields.attendanceTab.minMultiplicity = 1;
		_metadata.fields.attendanceTab.maxMultiplicity = 1;

		_metadata.fields.attendanceClassification.minMultiplicity = 1;
		_metadata.fields.attendanceClassification.maxMultiplicity = 1;

		treatFieldsEdition(); ///Trata metadata de detalhes gerais
		treatMessagesAndExceptions();
		treatAnonymousCustomer();
		treatContactData();
		treatNegotiations();
		treatEmailZendeskHistory();
		treatInconsistencyAttendance(); ///Inconsistência de atendimento para o BackOffice
		treatRequiredShowFields();

		/////////////////////////////////

		function treatFieldsEdition() {
			if (_object.attendanceLevel.name !== "N1") {

				_metadata.fields.ombudsman.readOnly = true;
				_metadata.fields.assignmentJustification.hidden = false;

		        _metadata.fields.antifraudeContract.hidden = _object.antifraudeContract ? false : true;
        		_metadata.fields.labels.hidden = _object.labels ? false : true;
			}

			if (_object.attendanceLevel.name === "N1" || _object.attendanceLevel.name === "N2") {
				_metadata.fields.attendanceHistory.hidden = false;
			} else {
				_metadata.fields.attendanceClassification.hidden = true;
			}

            //Não exibir o campo ouvidoria para o atendimento
			/*if (_object.prodServ && _object.prodServ.identifier && _object.prodServ.identifier === "cadastropositivo") {
				_metadata.fields.ombudsman.hidden = false;
			}*/

			var levelName = _object.attendanceLevel.name;
			if (_object.isSolved === 'encaminhar' && (levelName === "N1" || levelName === "N2")) {
				_metadata.fields.priority.required = true;
				_metadata.fields.priority.hidden = false;
			}
			
			if(levelName === "N2" && _object.customerServiceAction && _object.customerServiceAction.identifier === "serasaConsumidorUnlock48Hours"){
			    _metadata.fields.birthDate.hidden = false;
			    _metadata.fields.newValue.name = "Novo E-mail";
			    _metadata.fields.newValue.hidden = false;
			}
			
			if (_object.isSolved == "sim" && _object.responsibleGroup && _object.responsibleGroup._id == "59ef9311ce02722cda742b89"){
			    _metadata.fields.resolution.hidden = false;
			    _metadata.fields.resolution.required = true;
			} else {
			    _metadata.fields.resolution.hidden = true;
			    _metadata.fields.resolution.required = false;
			}

			/* Localizado agora na classe "completeAttendanceTab"
			if (_object.isntBackOfficeN2 || _object.attendanceLevel.name == "N3" || _object.attendanceLevel.name === "N4" || _object.backOfficeSolving) {
    			_metadata.fields.prodServ.readOnly = true;
    			_metadata.fields.reason.readOnly = true;
    			_metadata.fields.subReason.readOnly = true;
    			_metadata.fields.subSubReason.readOnly = true;
			}*/
		}

		function treatMessagesAndExceptions() {
			/// Mensagem de erro na ação para > N1
			if (!_metadata.editHelp && _object.statusAction === 'erro' && _object.customerServiceAction) {
				_metadata.editHelp = "<html>Ocorreu um erro em '" + _object.customerServiceAction.name + "'. Favor tentar novamente ou encaminhar para o próximo nível.<br></html>";
			}

			if (_object.customerServiceAction && _object.isSolved && _object.isSolved === 'encaminhar') {
				throw "Caso não possa resolver o atendimento, não é possível escolher uma ação.";
			}
		}

		function treatAnonymousCustomer() {
			if (_object.anonymousClient) {
				_metadata.fields.customer.hidden = true;
				_metadata.fields.showAntifraudeHistory.hidden = true;
				_metadata.fields.attendanceHistory.hidden = true;
				_metadata.fields.showEmailHistory.hidden = true;
				_metadata.fields.showZendeskHistory.hidden = true;
			}
		}

		function treatContactData() {
			///Dados do contato
			if (_object.attendanceLevel.name === 'N2' && _object.customer && !_object.backOfficeSolving) {
				
				_metadata.fields.clientContactDataTable.hidden = false;
				_metadata.fields.clientContactDataTable.minMultiplicity = 1;
				_metadata.fields.clientContactDataTable.maxMultiplicity = 1;
				
			}

			///Dados Serasa Consumidor
			if (_object.attendanceLevel.name !== 'N1' && _object.personFoundinSerasaConsumidor && _object.customer) {
				_metadata.fields.showSerasaConsumidorClientData.hidden = false;
				_metadata.fields.showSerasaConsumidorClientData.required = true;
				_metadata.fields.showSerasaConsumidorClientData.name = "Dados de cadastro no Serasa Consumidor";
				if (_object.serasaConsumidorClientData) {
					_metadata.fields.serasaConsumidorClientData.hidden = false;
					_metadata.fields.serasaConsumidorClientData.readOnly = true;
					_metadata.fields.showSerasaConsumidorClientData.hidden = true;
				} else {
					_metadata.fields.showSerasaConsumidorClientData.valueOptions.push({
						identifier: "show",
						value: "Visualizar"
					});
				}
			}
		}

		function treatNegotiations() {
		    var eCredOrLNO = _object.prodServ && (_object.prodServ.identifier === 'serasalimpanome' || _object.prodServ.identifier === 'serasaecred');
            if (_object.negotiations && _object.negotiations.length) {
                if (_object.attendanceLevel.name !== 'N1') {
                    
        			_metadata.fields.showNegotiations.hidden = false;
        			_metadata.fields.showNegotiations.required = true;
        			_metadata.fields.showNegotiations.name = "Informações de negociações LNO e eCred";
        			if (_object.showNegotiations === 'show') {
        				_metadata.fields.showNegotiations.hidden = true;
        				_metadata.fields.negotiations.hidden = false;
        				_metadata.fields.negotiations.maxMultiplicity = _object.negotiations.length;
        				_metadata.fields.negotiations.minMultiplicity = _object.negotiations.length;
        			} else {
        				_metadata.fields.showNegotiations.valueOptions.push({
        					identifier: "show",
        					value: "Visualizar"
        				});
        			}
                    
                } else if (eCredOrLNO) {
                    _metadata.fields.negotiations.hidden = false;
    				_metadata.fields.negotiations.maxMultiplicity = _object.negotiations.length;
    				_metadata.fields.negotiations.minMultiplicity = _object.negotiations.length;
    				if (_object.prodServ.identifier === 'serasalimpanome') {
    				    _metadata.fields.negotiations.name = 'Negociações LNO';
    				} else if (_object.prodServ.identifier === 'serasaecred') {
    				    _metadata.fields.negotiations.name = 'Negociações eCred';
    				}
                }
            }
		}

		function treatEmailZendeskHistory() {
			/// Histórico de e-mails Serasa
			_metadata.fields.showEmailHistory.required = true;
			_metadata.fields.showEmailHistory.name = "Histórico de disparo de e-mails na Campaign Monitor";
			if (_object.emailHistory === null) {
				_metadata.fields.showEmailHistory.valueOptions.push({
					identifier: "show",
					value: "Visualizar"
				});
			} else if (_object.emailHistory.length !== 0) {
				_metadata.fields.showEmailHistory.hidden = true;
				_metadata.fields.emailHistory.hidden = false;
				_metadata.fields.emailHistory.maxMultiplicity = _object.emailHistory.length;
				_metadata.fields.emailHistory.minMultiplicity = _object.emailHistory.length;
			} else if (_object.emailHistory.length === 0) {
				_metadata.fields.showEmailHistory.hidden = true;
				if (_object.noInfoEmailHistory) {
					_metadata.fields.noInfoEmailHistory.hidden = false;
					_metadata.fields.noInfoEmailHistory.name = "Histórico de disparo de e-mails na Campaign Monitor";
				}
			}

			/// Histórico Zendesk (redes sociais e chat)
			_metadata.fields.showZendeskHistory.required = true;
			_metadata.fields.showZendeskHistory.name = "Histórico de atendimentos na Zendesk";
			if (_object.zendeskHistory === null) {
				_metadata.fields.showZendeskHistory.valueOptions.push({
					identifier: "show",
					value: "Visualizar"
				});
			} else if (_object.zendeskHistory.length !== 0) {
				_metadata.fields.showZendeskHistory.hidden = true;
				_metadata.fields.zendeskHistory.hidden = false;
				_metadata.fields.zendeskHistory.maxMultiplicity = _object.zendeskHistory.length;
				_metadata.fields.zendeskHistory.minMultiplicity = _object.zendeskHistory.length;
			} else if (_object.zendeskHistory.length === 0) {
				_metadata.fields.showZendeskHistory.hidden = true;
				if (_object.noInfoZendeskHistory) {
					_metadata.fields.noInfoZendeskHistory.hidden = false;
					_metadata.fields.noInfoZendeskHistory.name = "Informações de atendimentos realizados via chat ou redes sociais (Zendesk)";
				}
			}

		}

		function treatInconsistencyAttendance() {
			if (_object.attendanceLevel.name === "N2" && !_object.isntBackOfficeN2 && !_object.backOfficeSolving) {
				_metadata.fields.inconsistentAttendance.hidden = false;
				_metadata.fields.inconsistentAttendance.required = true;
				if (_object.inconsistentAttendance) {
					_metadata.fields.inconsistencyTable.name = "Detalhes";
					_metadata.fields.inconsistencyTable.hidden = false;
					_metadata.fields.inconsistencyTable.hidden.required = true;
					_metadata.fields.inconsistencyTable.minMultiplicity = 1;
					_metadata.fields.inconsistencyTable.maxMultiplicity = 1;
				}
			}
		}

		function treatRequiredShowFields() {
			if (_object.isSolved) {
				var fields = [
					"showSerasaConsumidorClientData",
					"showNegotiations",
					"showEmailHistory",
					"showZendeskHistory",
					"showAntifraudeHistory"
				];

				fields.forEach(
					function (field) {
					_metadata.fields[field].required = false;
				});
			}
		}

	}

	function treatAttendanceType() {
		///Importante ser a última função para seu metadata sobresair
		var attendanceTypes = ["ligacaoPerdida", "engano"];
		var attClass = _object.attendanceClassification;
		if (attClass && attClass.length && attClass[0].type && attendanceTypes.indexOf(attClass[0].type) >= 0) {

			_metadata.fields.description.hidden = false;
			_metadata.fields.description.required = true;

			_metadata.fields.priority.hidden = true;
			_metadata.fields.priority.required = false;

			_metadata.fields.attendanceTab.minMultiplicity = 0;
			_metadata.fields.attendanceTab.hidden = true;

			_metadata.fields.isSolved.hidden = false;
			_metadata.fields.isSolved.required = true;
			_metadata.fields.isSolved.valueOptions.push({
				identifier: "sim",
				value: "Sim"
			});
			
		}
	}
    
    if (_object.responsible) {
        _metadata.fields.responsible.required = true;
        _metadata.fields.responsible.hidden = false;
    }
    
	var readOnlyFields = ["tabType", "customer", "responsible", "receitaFederalStatus", "labels", "antifraudeContract",
		"digitalCertificate", "negotiations", "attendanceLevel", "URAOrigin", "URAPhone",
		"responsibleGroup", "antifraudeEmailHistory", "antifraudeSMSHistory",
		"attendanceHistory"];
		
	var requiredFields = ["showSerasaConsumidorClientData",  "showNegotiations",
		"ombudsman", "description", "showZendeskHistory"];
	
	readOnlyFields.forEach(function (field) {
		_metadata.fields[field].readOnly = true;
	});
	requiredFields.forEach(function (field) {
		_metadata.fields[field].required = true;
	});
    
	treatServiceTabOptions();
	treatAttendanceActions();
	treatAttendanceDetails();
	treatAttendanceEvolution(); ///Fluxo de atendimento passo-a-passo (configurando editHelp)
	treatAttendanceType();      ///'Tipos de atendimentos' que não precisam ser atendidos

}

function documentAutenticationCustomMetadata() {
	if(_object.customerServiceAction && (_object.customerServiceAction.identifier == "optOut" || _object.customerServiceAction.identifier == "reOptIn" || _object.customerServiceAction.identifier == "cadastroPositivoGenerateReport")) {	
		_metadata.fields.documentType.required = true;
		_metadata.fields.documentNumber.required = true;
		_metadata.fields.pfDocument.required = true;
		_metadata.fields.pfDocument.required = true;

		if(_object.cnpj) {
			_metadata.fields.pjDocument.required = true;
			_metadata.fields.pjDocument.hidden = false;
		}
		if(_object.documentWithoutCpf == true) {
			_metadata.fields.cpfDocument.required = true;
			_metadata.fields.cpfDocument.hidden = false;
		}
	}
}

function performRetentionCustomMetadata() {

	function treatAttendanceEvolution() {
	    
		if (_object.isWithheld == "nao") {
		    if (!_object.argumentation) {
		        _metadata.fields.argumentation.required = false;
     			_metadata.fields.argumentation.hidden = true;
		    }
			
			var retentionDiscounts = billing.discount.getValidRetentionDiscounts({
					"contract": _object.contract
				}).retentionDiscounts;
			var discountsIds = [];
			retentionDiscounts.forEach(function (discount) {
				discountsIds.push(discount._id);
			});
			_metadata.fields.retentionDiscount.query = {
				"bool": {
					"must": {
						"terms": {
							"_id": discountsIds
						}
					}
				}
			};
			if (retentionDiscounts.length > 0) {
				_metadata.fields.isWithheldDiscount.hidden = false;
				_metadata.fields.isWithheldDiscount.required = true;
				
				_metadata.fields.isWithheldDiscount.valueOptions.push({
                    identifier: "sim",
                    value: "Sim"
                });
                _metadata.fields.isWithheldDiscount.valueOptions.push({
                    identifier: "nao",
                    value: "Não"
                });

				if (_object.isWithheldDiscount == "nao") {
					treatSubReasonWithheldOptions();
				} else {
					_metadata.fields.retentionDiscount.hidden = false;
					_metadata.fields.retentionDiscount.required = true;
					treatRetention();
				}
			} else {
				treatSubReasonWithheldOptions();
			}
		} else {
			treatRetention();
		}

		function treatSubReasonWithheldOptions() {
			/*_metadata.fields.retentionSubReason.query = {
				"bool": {
					"must": [{
							"term": {
								"active": true
							}
						}, {
							"term": {
								"parentTab._id": _object.reason._id
							}
						}
					]
				}
			};*/
			
			_metadata.fields.retentionSubReason.hidden = false;
			_metadata.fields.retentionSubReason.required = true;
			if (_object.retentionSubReason && _object.retentionSubReason._id) {
			    _metadata.fields.retentionSubReason.readOnly = true;
			    
				var tabsToIgnore = ["serasaantifraude.cancelamento.cartaodecredito.reversaocomargumentacao",
					"serasaantifraude.cancelamento.cartaodecredito.reversaocomdesconto",
					"serasaantifraude.cancelamento.boleto.reversaocomargumentacao",
					"serasaantifraude.cancelamento.boleto.reversaocomdesconto"
				];
				_metadata.fields.retentionSubSubReason.query = {
					"bool": {
						"must": [{
								"term": {
									"active": true
								}
							}, {
								"term": {
									"parentTab._id": _object.retentionSubReason._id
								}
							}
						],
						"must_not": [{
								"terms": {
									"identifier.keyword": tabsToIgnore
								}
							}
						]
					}
				};
				var sResult = crm.customerServiceTab._search({
						"query": _metadata.fields.retentionSubSubReason.query,
						"size": 0
					});
				if (sResult && sResult.hits && sResult.hits.total > 0) {
					_metadata.fields.retentionSubSubReason.hidden = false;
					_metadata.fields.retentionSubSubReason.required = true;
				}

				if (sResult && sResult.hits && sResult.hits.total > 0 && _object.retentionSubSubReason) {
					getActions(_object.retentionSubSubReason);
					treatIsSolved();
				} else {
					getActions(_object.retentionSubReason);
					treatIsSolved();
				}
				if (_object.retentionCustomerServiceAction) {
					treatActions();
				}
			}
		}

		function getActions(tab) {
			var sResult = crm.customerServiceTab._search({
					"query": {
						"term": {
							"identifier.keyword": tab.identifier
						}
					}
				});
			if (sResult && sResult.hits && sResult.hits.total > 0) {
				var customerServiceAction = sResult.hits.hits[0]._source.customerServiceAction;
				if (customerServiceAction) {
					if (!_object.backOfficeSolving) {
						_metadata.fields.retentionCustomerServiceAction.hidden = false;
					}
					var isRetention = (_object.isWithheld == "sim") && _object.isWithheldDiscount == "sim";
					var isSolved = _object.isSolved && _object.isSolved === "sim";
					if (_object.attendanceLevel.name === "N1" && isRetention && isSolved) {
						_metadata.fields.retentionCustomerServiceAction.required = true;
					}

					var actionsID = [];
					customerServiceAction.forEach(function (action) {
						actionsID.push(action._id);
					});
					_metadata.fields.retentionCustomerServiceAction.query = {
						"terms": {
							"_id": actionsID
						}
					};
				}
			}
		}

		function treatActions() {
			
			function userHasBackOfficeGroup() {
                return _user.aclUserGroups.some(
                    function (userGroup) {
                        return userGroup && userGroup._id === "59ef9311ce02722cda742b89";
                    }    
                );
            }
			
			if (_object.retentionCustomerServiceAction.identifier === "scheduleSuspensionService") {
				//Nada
			} else if (_object.retentionCustomerServiceAction.identifier === "suspendService") {
				// _metadata.fields.openCancelEBS.hidden = false;
				// _metadata.fields.openCancelEBS.required = true;
				if (!_object.backOfficeSolving && (_object.attendanceLevel.name !== 'N1' || userHasBackOfficeGroup())) {
					_metadata.fields.totalRefundCancellation.hidden = false;
					_metadata.fields.totalRefundCancellation.required = true;
				}
				var prvProductAntiFraudeId = "59d669f5ce02723b15ac40ea";
				var prvProductAntiFraudeFamiliaId = "5b9000d4b316393b1303593f";
				if (_object.contract && _object.contract.product && _object.contract.product.prvProduct && 
				(_object.contract.product.prvProduct._id === prvProductAntiFraudeId || _object.contract.product.prvProduct._id === prvProductAntiFraudeFamiliaId)) {
				// 	_metadata.fields.refundAmount.hidden = false;
					var refund = billing.contract.calculateRefundAmount({
							"contractId": _object.contract._id,
							"is100percent": _object.totalRefundCancellation
						});
					if (refund.withinSevenDaysOfPurchase) {
						_metadata.fields.totalRefundCancellation.readOnly = true;
					} else {
						_metadata.fields.totalRefundCancellation.readOnly = false;
					}
				} else {
					_metadata.fields.refundAmount.hidden = true;
				}
			}
		}

		function treatRetention() {
			if ((_object.isWithheld == "sim" && _object.argumentation) || (_object.isWithheldDiscount == "sim" && _object.retentionDiscount)) {
				_metadata.fields.retentionSubReason.hidden = false;
				_metadata.fields.retentionSubReason.required = true;
				// _metadata.fields.retentionSubReason.query = {
				// 	"bool": {
				// 		"must": [{
				// 				"term": {
				// 					"active": true
				// 				}
				// 			}, {
				// 				"term": {
				// 					"parentTab._id": _object.reason._id
				// 				}
				// 			}
				// 		]
				// 	}
				// };

				if (_object.retentionSubReason && _object.retentionSubReason._id) {
                    _metadata.fields.retentionSubReason.readOnly = true;
                    
					var tabsToGet = [];
					var argumentationTabs = ["serasaantifraude.cancelamento.cartaodecredito.reversaocomargumentacao", "serasaantifraude.cancelamento.boleto.reversaocomargumentacao"];
					var retentionDiscountTabs = ["serasaantifraude.cancelamento.cartaodecredito.reversaocomdesconto", "serasaantifraude.cancelamento.boleto.reversaocomdesconto"];
					if (_object.isWithheld == "sim") {
						tabsToGet = tabsToGet.concat(argumentationTabs);
					} else if (_object.isWithheldDiscount == "sim") {
						tabsToGet = tabsToGet.concat(retentionDiscountTabs);
					}

				// 	_metadata.fields.retentionSubSubReason.query = {
				// 		"bool": {
				// 			"must": [{
				// 					"term": {
				// 						"active": true
				// 					}
				// 				}, {
				// 					"term": {
				// 						"parentTab._id": _object.retentionSubReason._id
				// 					}
				// 				}, {
				// 					"terms": {
				// 						"identifier.keyword": tabsToGet
				// 					}
				// 				}
				// 			]
				// 		}
				// 	};

					var sResult = crm.customerServiceTab._search({
				            // "query": _metadata.fields.retentionSubSubReason.query,
				            "query":{
        						"bool": {
        							"must": [{
        									"term": {
        										"active": true
        									}
        								}, {
        									"term": {
        										"parentTab._id": _object.retentionSubReason._id
        									}
        								}, {
        									"terms": {
        										"identifier.keyword": tabsToGet
        									}
        								}
        							]
        						}
        					},
							"size": 0
						});
					if (sResult && sResult.hits && sResult.hits.total > 0) {
						_metadata.fields.retentionSubSubReason.hidden = false;
						_metadata.fields.retentionSubSubReason.required = true;
						if (_object.retentionSubSubReason && _object.retentionSubSubReason._id){
						    _metadata.fields.retentionSubSubReason.readOnly = true;
						}
						
					}

					if (sResult && sResult.hits && sResult.hits.total > 0 && _object.retentionSubSubReason && _object.retentionSubSubReason._id) {
						getActions(_object.retentionSubSubReason);
						treatIsSolved();
					} else {
						getActions(_object.retentionSubReason);
						treatIsSolved();
					}
				}
			}
		}

		function treatIsSolved() {
			var isThereNextLevel = false;
			var tabCanFinish = false;
			var currentLevel = _object.attendanceLevel.name;
			var tabOn = _object.retentionSubSubReason ? _object.retentionSubSubReason :
				_object.retentionSubReason ? _object.retentionSubReason : _object.reason;

			if (tabOn.customerServiceData && tabOn.customerServiceData.length) {
				tabCanFinish = true;
				tabOn.customerServiceData.forEach(function (cSD) {
					if ((currentLevel === "N1" && cSD.level.name === "N2") ||
						(currentLevel === "N2" && cSD.level.name === "N3") ||
						(currentLevel === "N3" && cSD.level.name === "N4")) {
						isThereNextLevel = true;
					}
				});
			}

			if (tabCanFinish) {
			    _metadata.fields.description.required = true;
				_metadata.fields.description.hidden = false;
				treatValueOptions(isThereNextLevel);
				_metadata.fields.isSolved.required = true;
				_metadata.fields.isSolved.hidden = false;
				_metadata.fields.files.hidden = false;
			}
		}

		function treatValueOptions(isThereNextLevel) {
			//////////////////////////////
            var tabOn = _object.subSubReason ? _object.subSubReason :
            			_object.subReason ? _object.subReason :
                        _object.reason ? _object.reason : null;
            
            var canBackOffice = false;
            
            if (tabOn && tabOn._id && tabOn.customerServiceData && tabOn.customerServiceData.length){
                tabOn.customerServiceData.forEach(function (serviceData){
                    if(serviceData.level._id == _object.attendanceLevel._id){
                        canBackOffice = serviceData.canBackOffice;
                    }
                }); 
            }
            
            if (!canBackOffice || _object.backOfficeSolving) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "sim",
                    value: "Sim"
                });
            } else {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "simBackOffice",
                    value: "Sim"
                });
            }
            /////////////////////////////
			if (isThereNextLevel && !_object.backOfficeSolving && _object.cpf) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "encaminhar",
                    value: "Não"
                });
            } else if (_object.backOfficeSolving && _object.cpf) {
                _metadata.fields.isSolved.valueOptions.push({
                    identifier: "retornar",
                    value: "Não"
                });
            }

			if (_object.isSolved) { //if para garantir que na primeira execução do getMetadata não dê problema caso isSolved não exista nas opções.
				var exist = false;
				for (var i = 0; i < _metadata.fields.isSolved.valueOptions.length(); ++i) {
					if (_metadata.fields.isSolved.valueOptions[i].identifier === _object.isSolved) {
						exist = true;
					}
				}
				if (!exist) {
					_metadata.fields.isSolved.valueOptions.push({
						identifier: _object.isSolved,
						value: _object.isSolved
					});
				}
			}

		}
	}
	
	function treatFieldsEdition() {
		if (_object.attendanceLevel.name === "N3" || _object.attendanceLevel.name === "N4" || _object.backOfficeSolving) {
			_metadata.fields.retentionSubReason.readOnly = true;
			_metadata.fields.retentionSubSubReason.readOnly = true;
		}

		if (_object.backOfficeSolving) {
			_metadata.fields.isWithheld.readOnly = true;
			_metadata.fields.isWithheldDiscount.readOnly = true;
			_metadata.fields.retentionCustomerServiceAction.hidden = false;
			_metadata.fields.retentionCustomerServiceAction.readOnly = true;
		} else {
		    _metadata.fields.retentionCustomerServiceAction.readOnly = false;
		}

        var levelName = _object.attendanceLevel.name;
		if (_object.isSolved === 'encaminhar' && (levelName === "N1" || levelName === "N2")) {
			_metadata.fields.priority.required = true;
			_metadata.fields.priority.hidden = false;
		}

		if (_object.customerServiceAction && _object.isSolved && _object.isSolved === 'encaminhar') {
			throw "Caso não possa resolver o atendimento, não é possível escolher uma ação.";
		}
		
		if (_object.attendanceLevel && _pin.previousAttendanceLevel && _object.attendanceLevel._id && _pin.previousAttendanceLevel._id && _object.attendanceLevel._id == _pin.previousAttendanceLevel._id) {
		    if ((_object.retentionCustomerServiceAction && (_object.retentionCustomerServiceAction.identifier === "scheduleSuspensionService" || _object.retentionCustomerServiceAction.identifier === "suspendService")) && _object.statusAction) {
                _metadata.fields.isWithheld.readOnly = true;
                _metadata.fields.isWithheldDiscount.readOnly = true;
                _metadata.fields.retentionCustomerServiceAction.hidden = true;
            } else {
                _metadata.fields.retentionSubSubReason.readOnly = false;
            }
        }
        
        if (_object.isSolved == "sim" && _object.responsibleGroup && _object.responsibleGroup._id == "59ef9311ce02722cda742b89"){
		    _metadata.fields.resolution.hidden = false;
		    _metadata.fields.resolution.required = true;
		} else {
		    _metadata.fields.resolution.hidden = true;
		    _metadata.fields.resolution.required = false;
		}
	}
    
    function treatEditHelpProgression() {
        
        function treatNotSolved() {
            
            function formatDate(date) {
    		    return date.toISOString().slice(0, 10).split('-').reverse().join('/');
    		}
    		
            var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
            return "<html>Baseado na urgência do problema, o priorize selecionando a opção adequada no campo <b>“Prioridade”</b>.<br>" +
				"Se necessário <b>anexe</b> os arquivos com as evidências geradas para o atendimento.<br><br>" + 
				"O SLA deste atendimento é de <b>" + (slaData && slaData.sla ? slaData.sla : "(Não encontrado)") + "</b>; " + 
				"informe ao consumidor que a previsão de atendimento é até <b>" + (slaData && slaData.finalDate ? formatDate(slaData.finalDate) : "(Não encontrado)") + "</b>.<br><br>" + 
				"Termine o atendimento através do botão <b>“Concluir”</b></html>.";
        }
        
        function treatWithheldHelp() {
            _metadata.editHelp = "<html><b>Explique</b> ao consumidor as <b>vantagens</b> em manter o serviço, anotando sua <b>argumentação</b> se houve retenção.<br>" + 
                "Caso ele queira prosseguir com o cancelamento desmarque a opção <b>'Retido'</b>.</html>";
            
            if (_object.argumentation) {
                _metadata.editHelp = "<html>Selecione o <b>SubMotivo</b>, <b>SubSubMotivo</b> e <b>Ação de atendimento</b> apropriados. Em seguida confirme se o atendimento está resolvido ou não.</html>";
                if (_object.isSolved === "encaminhar") {
                    var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
                    _metadata.editHelp = treatNotSolved();
                } else if (_object.isSolved === "sim"){
                    _metadata.editHelp = "<html>Termine o atendimento através do botão <b>“Concluir”</b></html>.";
                }
            }
            
        }
        
        function treatWithheldDiscountHelp() {
            _metadata.editHelp = "<html><b>Ofereça</b> os descontos disponíveis ao cliente e reforce as vantagens em manter o serviço.<br>" + 
                "Caso ele queria prosseguir com o cancelamento desmarque a opção <b>'Retido com desconto'</b>.</html>";
            if (_object.retentionDiscount) {
                _metadata.editHelp = "<html>Selecione o <b>SubMotivo</b>, <b>SubSubMotivo</b> e <b>Ação de atendimento</b> apropriados. Em seguida confirme se o atendimento está resolvido ou não.</html>";
                if (_object.isSolved === "encaminhar") {
                    var slaData = _object.slaData && _object.slaData.length && _object.slaData[0];
                    _metadata.editHelp = treatNotSolved();
                } else if (_object.isSolved === "sim"){
                    _metadata.editHelp = "<html>Termine o atendimento através do botão <b>“Concluir”</b></html>.";
                }
            }
        }
        
        function treatCancelationHelp() {
            _metadata.editHelp = "<html>Selecione o <b>SubMotivo</b> e <b>SubSubMotivo</b> de cancelamento informado pelo cliente.</html>";
            if (_object.retentionSubSubReason) {
                if (_object.isSolved === 'encaminhar') {
                    _metadata.editHelp = treatNotSolved();
                } else {
                    if (_object.contract && _object.contract._id && _object.customerServiceAction && _object.customerServiceAction.identifier == "scheduleSuspensionService"){
                        var disableDate = billing.contract.calculateScheduleDisableDate({"_id": _object.contract._id});
                        if (disableDate && disableDate.date){
                            var date = new Date(disableDate.date);
                            var day, mounth, year;
                            day = date.getDate();
                            mounth = date.getMonth()+1;
                            year = date.getFullYear();
                            if(day<10){
                                day="0"+day;
                            }
                            if(mounth<10){
                                mounth="0"+mounth;
                            }
                            _metadata.editHelp = "<html>Suspensão será agenda para o dia <b>"+day+"/"+mounth+"/"+year+".<br></html>"
                        }
                    } else if (_object.contract && _object.contract._id && _object.customerServiceAction && _object.customerServiceAction.identifier == "suspendService"){
                        if (_object.refundAmount > 0){
                            var refund = parseFloat(_object.refundAmount.toFixed(2));
                            refund = refund.toFixed(2);
                            _metadata.editHelp = "<html>Contrato do cliente será suspenso imediatamente.<br>" + 
                                    "O valor do estorno de acordo com a política de cancelamento é: <b>R$ "+refund+".</b></html>";
                        } else {
                            _metadata.editHelp = "<html>Contrato do cliente será <b>suspenso imediatamente.</b><br>" + 
                                    "Cancelamento não prevê estorno, conforme política de cancelamento.</html>";
                        }
                    } else { 
                        _metadata.editHelp = "<html>Selecione a <b>ação</b> apropriada e marque o atendimento como resolvido.<br>" + 
                                    "Caso não seja possível realizar o cancelamento, informe ao consumidor que o atendimento será " + 
                                    "atribuído para a área responsável.</html>";
                    }
                    
                }
            }
        }
        
        if (_object.attendanceLevel.name === "N1") {
            
            if (_object.isWithheld == "sim") {
                treatWithheldHelp();
            } else {
                if (_object.isWithheldDiscount == "sim" && _metadata.fields.isWithheldDiscount.required) {
                    treatWithheldDiscountHelp();
                } else {
                    treatCancelationHelp();
                }
            }
            
        }
        
        if (_object.isSolved == "sim" && _object.responsibleGroup && _object.responsibleGroup._id == "59ef9311ce02722cda742b89"){
            _metadata.editHelp = "<html>Informe no campo resolução a resposta ao consumidor. <br><b>IMPORTANTE:</b> Toda informação contida no campo irá ser enviada por e-mail para o consumidor.</html>"
        }
    }

	var readOnlyFields = ["customer", "prodServ", "reason", "contract", "refundAmount"];
	var requiredFields = ["argumentation", "isWithheld"];
	
	
	/////
	_metadata.fields.isWithheld.valueOptions.push({
        identifier: "sim",
        value: "Sim"
    });
    _metadata.fields.isWithheld.valueOptions.push({
        identifier: "nao",
        value: "Não"
    });



	readOnlyFields.forEach(function (field) {
		_metadata.fields[field].readOnly = true;
	});
	requiredFields.forEach(function (field) {
		_metadata.fields[field].required = true;
	});

	_metadata.fields.retentionSubReason.name = "SubMotivo";
	_metadata.fields.retentionSubSubReason.name = "SubSubMotivo";
	_metadata.fields.retentionCustomerServiceAction.name = "Ação de atendimento";

	treatAttendanceEvolution();
	treatFieldsEdition();
	treatEditHelpProgression();
}

function performSaleCustomMetadata() {
	
	function antifraudContactDataDetails() {
	    _metadata.editHelp = "<html><b>Confirme</b> o e-mail, telefone e endereço de cobrança do serviço <b>AntiFraude</b>.</html>";
    	if (!_object.customer.emails || !_object.customer.emails.length) {
    		_metadata.editHelp += "<html><br>O consumidor <b>não</b> possui um e-mail cadastrado.</html>";
    	}
    	if (!_object.customer.phones || !_object.customer.phones.length) {
    		_metadata.editHelp += "<html><br>O consumidor <b>não</b> possui um telefone celular cadastrado.</html>";
    	}
    	if (!_object.customer.addresses || !_object.customer.addresses.length) {
    		_metadata.editHelp += "<html><br>O consumidor <b>não</b> possui um endereço cadastrado.</html>";
    	}
	    
	    _metadata.fields.customer.hidden = false;
	    _metadata.fields.phoneContact.hidden = false;
	    _metadata.fields.emailContact.hidden = false;
	    _metadata.fields.addressContact.hidden = false;
	    _metadata.fields.antifraudContactDataConfirmation.hidden = false;
	    
	    _metadata.fields.phoneContact.minMultiplicity = 1;
	    _metadata.fields.emailContact.minMultiplicity = 1;
	    _metadata.fields.addressContact.minMultiplicity = 1;
	}

	function saleDetails() {
	    
	    function treatCouponMetadata() {
    		if (_object.saleContract) {
    		    if (_object.saleContract.paymentMethod && _object.saleContract.paymentMethod.identifier === "COURTESY") {
    		    	_metadata.fields.coupon.required = true;
    		    } else {
    			    _metadata.fields.coupon.required = false;
    		    }
    			if (_object.saleContract.product && _object.saleContract.buyer) {
    				if (_object.applyAutomaticDiscount) {
    					_metadata.fields.coupon.readOnly = true;
    				} else {
    					_metadata.fields.coupon.readOnly = false;
    				}
    			}
    		}
	    }
	    
	    function treatNoSaleMetadata() {
    		if (_object.saleSubReason && _object.saleSubReason.identifier === 'serasaantifraude.oferta.naovenda') {
    		    var hiddenFields = ["saleContract", "applyAutomaticDiscount", "coupon", "value", "installmentsValue"];
    		    hiddenFields.forEach(function(field) {
                    _metadata.fields[field].hidden = true;
                    _metadata.fields[field].required = false;
                });
                
                _metadata.fields.saleSubSubReason.name = 'SubSubMotivo';
                _metadata.fields.saleSubSubReason.hidden = false;
                _metadata.fields.saleSubSubReason.required = true;
                _metadata.fields.saleSubSubReason.query = {
            		"bool": {
                		"must": [{
                    		"term": {"active": true }
                		    
                		}, {
                    		"term": {"parentTab._id": _object.saleSubReason._id}
                		}]
            		}
        		};
                
    		    _metadata.editHelp = '<html>Selecione o motivo da desistência da venda em <b>SubSubMotivo</b> e clique em <b>Concluir</b> para finalizar o atendimento.</html>';
    		}
		}
	    
	    function treatEditHelp() {
	        
            function checkPaymentCondition() {
                if (_object.saleContract && _object.saleContract.product && _object.saleContract.paymentMethod) {
                    var contract = _object.saleContract;
                    if (contract.product.paymentConditions && contract.product.paymentConditions.length) {
                        return contract.product.paymentConditions.some(function (paymentCondition) {
                            var paymentConditionValid = false;
                            if (paymentCondition.paymentMethodGroup && paymentCondition.paymentMethodGroup.length) {
                                paymentCondition.paymentMethodGroup.forEach(function (pcPMG) {
                                    paymentConditionValid |= contract.paymentMethod.paymentMethodGroup._id === pcPMG._id;
                                });
                            }
                            if (paymentCondition.paymentMethod && paymentCondition.paymentMethod.length) {
                                paymentCondition.paymentMethod.forEach(function (pcPMT) {
                                    paymentConditionValid |= contract.paymentMethod._id === pcPMT._id;
                                });
                            }
                            if (paymentConditionValid) {
                                return true;
                            }
                        });
                    }
                }
                
                return false;
            }	        
	        
    	    _metadata.editHelp = "<html>Selecione qual <b>Produto</b> mais se adapta às necessidades do cliente. Em seguida escolha qual a <b>Forma de pagamento</b> desejada.</html>";
    	    
    	    var hasPaymentCondition = checkPaymentCondition();
    	    if (hasPaymentCondition) {
    			_metadata.editHelp += "<html><br><br>Este produto permite a seleção da <b>Condição de pagamento</b>.</html>";
    	    }
    	    
    	    if (_object.applyAutomaticDiscount && _object.discount) {
    	        _metadata.editHelp += "<html>Essa venda está com desconto '" + (_object.discount.name ? _object.discount.name : "(Não encontrado)") + "' aplicado automaticamente.<br>"
    	         + 'Para que esse desconto não seja aplicado, desmarcar a opção <b>"Aplicar Desconto Automático"</b>.';
    	    }
    	    
    	    _metadata.editHelp += "<html><br><br>Caso o cliente desista da compra, selecione <b>'Não Venda'</b> no <b>'SubMotivo'</b>.</html>";
    	    
    	    var paymentConditionFilled = !hasPaymentCondition || (hasPaymentCondition && _object.saleContract.paymentCondition);
    	    if (paymentConditionFilled && _object.saleContract && _object.saleContract.product && _object.saleContract.paymentMethod) {
    	        if (_object.saleContract.productData && _object.saleContract.productData.length && _object.saleContract.productData[0].provisioningParameters) {
    	            _metadata.editHelp = "<html>Confirme os <b>dados da compra</b> com o cliente e clique em <b>'Concluir'</b> para finalizar a venda</html>";
    	        }
    	    }
	    }
	    
        var notHidden = ["saleContract", "applyAutomaticDiscount", "coupon", "value", "saleSubReason"];
        notHidden.forEach(function(field) {
            _metadata.fields[field].hidden = false;
        });
        
    	_metadata.fields.saleContract.required = true;
    	_metadata.fields.applyAutomaticDiscount.required = true;
    	_metadata.fields.installmentsValue.readOnly = true;
    	_metadata.fields.value.readOnly = true;	    
        _metadata.fields.saleSubReason.name = "SubMotivo";
        _metadata.fields.saleSubReason.required = true;
        _metadata.fields.saleSubReason.query = {
    		"bool": {
        		"must": [{
            		"term": {"active": true }
        		    
        		}, {
            		"term": {"parentTab._id": _object.reason._id}
        		}]
    		}
		};
	    
		if (_object.saleContract && _object.saleContract.paymentMethod && _object.saleContract.paymentCondition) {
			_metadata.fields.value.name = "Valor total";
			_metadata.fields.installmentsValue.hidden = false;
		}
		
	    treatEditHelp();
	    treatCouponMetadata();
		treatNoSaleMetadata();
	}

    ////////////////////////
    
    if (!_object.antifraudContactDataConfirmation) {
	    antifraudContactDataDetails();
	} else {
	    _metadata.fields.antifraudContactDataConfirmation.hidden = true;
	    saleDetails();
	}
	
}

function terminalStateCustomMetadata() {}
